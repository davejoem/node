'use strict'
// Load required packages
var mongoose = require('mongoose');
var jwt = require("jsonwebtoken");
var keys = require('../config/keys');

//models
var User = require('./user');

// Define a device schema
var DeviceSchema = new mongoose.Schema({
  admin : {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  name :  String,
  description : String,
  location: {
    type: String,
    required: false
  },
  lock: {
    locked: {
      type: Boolean,
      required: true,
      default: false
    },
    by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  machine: {
    type: String,
    required: false
  },
  os: {
    type: String,
    required: true
  },
  settings: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Preference'
  }],
  states: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'State'
  }],
  type: {
    type: String,
    required: false
  },
  users : {
    signedIn : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    signedOut : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
  },
});

// methods ======================
DeviceSchema.methods.signInUser = function(userid,cb) {
    if (this.users.signedIn.indexOf(userid) == -1) {
      this.users.signedIn.push(userid);
    }
    this.users.signedOut.pull(userid);
    cb(null,this);
};
DeviceSchema.methods.signOutUser = function(userid,cb) {
    if (this.users.signedOut.indexOf(userid) == -1) {
      this.users.signedOut.push(userid);
    }
    this.users.signedIn.pull(userid);
    cb(null,this);
};
DeviceSchema.methods.generateToken = function(cb) {
  var token = jwt.sign({
    _id : this._id    
  }, keys.device);
  if (cb) return cb(null, token);
  return token;
};

// create the model for devices and expose it to our app
module.exports = mongoose.model('Device', DeviceSchema);