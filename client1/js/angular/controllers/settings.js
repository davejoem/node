var settingsController = function($location, $state, $rootScope, $scope, $client) {
  $scope.settings = $client.user.details.settings
  $scope.links = $client.launcher.views.allapps.apps
  $scope.states = {
    video: {
      home: {
        name: 'Home',
        abbrev: 'home'
      },
      library: {
        name: 'Library',
        abbrev: 'library'
      },
      collection: {
        name: 'My Collection',
        abbrev: 'collection'
      },
      previous: {
        name: 'Previous state',
        abbrev: 'previous'
      }
    },
    music: {
      home: {
        name: 'Home',
        abbrev: 'home'
      },
      library: {
        name: 'Library',
        abbrev: 'library'
      },
      collection: {
        name: 'My Collection',
        abbrev: 'collection'
      },
      previous: {
        name: 'Previous state',
        abbrev: 'previous'
      }
    },
    profile: {
      general: {
        name: 'General',
        abbrev: 'general'
      },
      social: {
        name: 'Social',
        abbrev: 'social'
      },
      devices: {
        name: 'Devices',
        abbrev: 'devices'
      },
      finance: {
        name: 'Finance',
        abbrev: 'finance'
      },
      previous: {
        name: 'Previous State',
        abbrev: 'previous'
      },
    },
    settings: {
      home: {
        name: 'Home',
        abbrev: 'home'
      },
      previous: {
        name: 'Previous state',
        abbrev: 'previous'
      }
    },
    linkapp: {
      calls: {
        name: 'Calls',
        abbrev: 'calls'
      },
      conversations: {
        name: 'Conversations',
        abbrev: 'conversations'
      },
      contacts: {
        name: 'Contacts',
        abbrev: 'contacts'
      },
      previous: {
        name: 'Previous state',
        abbrev: 'previous'
      }
    }
  }
  $scope.pinify = function(action, event) {
    $scope.action = action
    if (action == 'change')
      $scope.popup({
        scope: $scope,
        event: event,
        templateUrl: "/templates/misc/pin_change.html"
      })
    if (action == 'enable')
      $scope.popup({
        scope: $scope,
        event: event,
        templateUrl: "/templates/misc/pin.html"
      })
  }
  $scope.exec = function(pin) {
    if ($scope.action == 'enable') {
      $client.user.enablePin(pin).then(function(data) {
        $scope.pin = null ;
        $scope.closeDialog();
        $rootScope.$broadcast('appsettings', data.preference);
      }, function(error) {
        $scope.pin = null ;
        $client.toast.toast(error)
      }, function(info) {
        $scope.closeDialog()
        $client.toast.toast(info)
      });
    }
    if ($scope.action == 'change') {
      $client.user.changePin(pin).then(function(data) {
        $scope.pin = null
        $rootScope.$broadcast('appsettings', data.preference)
      }, function(error) {
        $scope.pin = null
        $scope.closeDialog()
        $client.toast.toast(error)
      }, function(info) {
        $scope.closeDialog()
        $client.toast.toast(info)
      })
    }
  }
  $scope.saveSettings = function() {
    $client.user.syncSettings().then(function(data) {
      $client.toast.toast(data)
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
  }
  $scope.syncSettings = function() {
    $client.user.details.settings = $scope.settings
    $client.theme.choose()
    if ($client.user.details.settings.apps.settings.misc.autosync)
      $client.user.syncSettings().then(function(data) {
        $client.toast.toast(data)
      }, function(error) {
        $client.toast.toast(error)
      }, function(info) {
        $client.toast.toast(info)
      })
  }
  $scope.getSettings = $client.user.getSettings;
  $scope.currentState = $state.current.name;
  if ($scope.user.settings.apps.general.rememberstates) {
    if ($client.navigator.directly) {
      switch ($client.navigator.dest.split('.')[1]) {
      case 'video':
        $scope.selectedIndex = 0;
        break;
      case 'music':
        $scope.selectedIndex = 1;
        break;
      case 'gamer':
        $scope.selectedIndex = 2;
        break;
      case 'linkapp':
        $scope.selectedIndex = 3;
        break;
      case 'profile':
        $scope.selectedIndex = 4;
        break;
      case 'settings':
        $scope.selectedIndex = 5;
        break;
      case 'notifications':
        $scope.selectedIndex = 6;
        break;
      case 'ideas':
        $scope.selectedIndex = 7;
        break;
      case 'stats':
        $scope.selectedIndex = 8;
        break;
      case 'logs':
        $scope.selectedIndex = 9;
        break;
      }
    } else {
      $scope.selectedIndex = $scope.user.state.states.settings.laststate
    }
  } else {
    if ($client.navigator.directly) {
      console.log($client.navigator.dest)
      //$scope.selectedIndex = $scope.states.indexOf($client.navigator.dest.split('.')[1])      
      $state.go($client.navigator.dest)
    } else {
      switch ($scope.settings.apps.settings.display.startin) {
      case 'home':
        $scope.selectedIndex = 5;
        break;
      case 'previous':
        $scope.selectedIndex = $scope.user.state.states.settings.laststate;
        break;
      }
    }
  }
  if ($scope.user.settings.general.rememberviews) {}
  $scope.$watch('selectedIndex', function(current) {
    switch (current) {
    case 0:
      $scope.goTo("settings.video");
      $client.user.details.state.states.settings.laststate = 0;
      break;
    case 1:
      $scope.goTo("settings.music");
      $client.user.details.state.states.settings.laststate = 1;
      break;
    case 2:
      $scope.goTo("settings.gamer");
      $client.user.details.state.states.settings.laststate = 2;
      break;
    case 3:
      $scope.goTo("settings.linkapp");
      $client.user.details.state.states.settings.laststate = 3;
      break;
    case 4:
      $scope.goTo("settings.profile");
      $client.user.details.state.states.settings.laststate = 4;
      break;
    case 5:
      $scope.goTo("settings");
      $client.user.details.state.states.settings.laststate = 5;
      break;
    case 6:
      $scope.goTo("settings.notifications");
      $client.user.details.state.states.settings.laststate = 6;
      break;
    case 7:
      $scope.goTo("settings.ideas");
      $client.user.details.state.states.settings.laststate = 7;
      break;
    case 8:
      $scope.goTo("settings.stats");
      $client.user.details.state.states.settings.laststate = 8;
      break;
    case 9:
      $scope.goTo("settings.logs");
      $client.user.details.state.states.settings.laststate = 9;
      break;
    }
  })
}
settingsController.$inject = ['$location', '$state', '$rootScope', '$scope', '$client'];
app.controller('settingsController', settingsController);
