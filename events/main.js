module.exports = (agent)=>{
  var Call = agent.models.Call
  var Chat = agent.models.Chat
  var Device = agent.models.Device
  var Episode = agent.models.Episode
  var Game = agent.models.Game
  var Message = agent.models.Message
  var Movie = agent.models.Movie
  var Notification = agent.models.Notification
  var Preference = agent.models.Preference
  var Season = agent.models.Season
  var Show = agent.models.Show
  var State = agent.models.State
  var Transaction = agent.models.Transaction
  var User = agent.models.User
  var io = agent.io
  var extend = agent.extend
  var eyes = agent.eyes
  var fn = agent.fn
  var fs = agent.fs
  var jwt = agent.jwt
  var keys = agent.keys
  var passport = agent.passport
  var path = agent.path
  var Q = agent.Q
  var shortId = agent.shortId
  var url = agent.url
  io.on("connection", (socket)=>{
    console.log('device connected')
    socket.on('register device', (data,cb)=>{
      console.log('reg data',data)
      var device = new Device(data);
      device.generateToken((err,token)=>{
        if (err)
          return cb({
            err: err,
            message: {
              class: "error",
              message: "Couldn't register this device."
            }
          })
        device.save((err,device)=>{
          if (err)
            return cb({
              err: err,
              message: {
                class: "error",
                message: "Couldn't register this device."
              }
            })
          cb(null , {
            token: token,
            device: device
          }, {
            message: {
              class: "success",
              message: "Device registered successfully."
            }
          })
        }
        )
      }
      )
    }
    ).on('checkexist', (data,cb)=>{
      if (data.email) {
        var req = {}
        req.body = data
        passport.authenticate('check-email', function(err, user, info) {
          if (err) {
            return next(err);
          }
          if (user) {
            return cb({
              exist: true
            })
          } else {
            return cb(null , null , info);
          }
        })(req, cb);
      }
      if (data.username) {
        var req = {}
        req.body = data
        passport.authenticate('check-username', function(err, user, info) {
          if (err) {
            return cb(err);
          }
          if (user) {
            return cb({
              exist: true
            })
          } else {
            return cb(null , null , info);
          }
        })(req, cb);
      }
      if (data.phone) {
        var req = {}
        req.body = data
        passport.authenticate('check-phone', function(err, user, info) {
          if (err) {
            return next(err);
          }
          // will generate a 500 error
          if (user) {
            return res.json({
              exist: true
            })
          } else {
            return cb(null , null , info);
          }
        })(req, cb);
      }
    }
    ).on('signin', (data,cb)=>{
      if (!data.user.email || !data.user.password || !data.user.device) {
        return cb({
          message: {
            class: "error",
            message: 'Please fill out all fields'
          }
        })
      }
      var req = {}
      req.body = data.user;
      req.device = socket.device;
      passport.authenticate('local-signin', (err,login,info)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: err.message
            }
          })
        if (login) {
          if (login.user.devices.signedInOn.indexOf(login.device._id) < 0)
            socket.to(login.username).emit("signed in on new device", login.device)
          User.findOne({
            'accounts.local.username': login.username
          }, (err,user)=>{
            console.log(user.devices)
            if (user) {
              user.notify({
                action: 'confirm',
                concern: 'security',
                devices: 'allbutthis',
                device: socket.device._id,
                importance: 1,
                time: Date.now(),
                message: 'Someone signed in with your credentials on a new device. Was this you?'
              }, (err,data)=>{
                if (err)
                  return
                socket.to(user.accounts.local.username).emit('new notification', data)
              }
              )
            }
          }
          )
          return cb(null , {
            token: login.token
          })
        }
        if (info)
          return cb(null , null , info);
      }
      )(req, cb);
    }
    ).on('signup', (data,cb)=>{
      if (!data.user.email || !data.user.username || !data.user.password || !data.user.confirmpassword || !data.user.gender || !data.user.firstname || !data.user.middlename || !data.user.lastname || !data.user.address || !data.user.about) {
        return cb({
          message: {
            class: "error",
            message: 'Please fill out all fields!'
          }
        })
      }
      if (data.user.password !== data.user.confirmpassword) {
        return cb({
          message: {
            class: 'error',
            message: 'Passwords DO NOT match!'
          }
        })
      }
      var req = {}
      req.body = data.user;
      req.device = socket.device;
      passport.authenticate('local-signup', (err,user,info)=>{
        if (err) {
          return cb({
            message: {
              class: "error",
              message: err.message + "! Please confirm that all fields are correctly filled out."
            }
          })
        }
        // generate a 400 error
        if (user) {
          return cb(null , true);
        } else {
          return cb(info);
        }
      }
      )(req, cb);
    }
    ).on('lock device', (dev,cb)=>{
      socket.device.lock.locked = true;
      socket.device.lock.by = socket.user._id;
      socket.device.save((err,device)=>{
        if (err)
          return cb(new Error("Couldn't lock this device."));
        cb(null , {
          message: {
            class: "success",
            message: "Device locked."
          }
        })
      }
      );
    }
    ).on('unlock device', (data,cb)=>{
      var message;
      var req = {
        body: {}
      }
      req.body.email = socket.user.accounts.local.email
      req.body.key = data.key
      req.user = socket.user
      req.device = socket.device
      if (!data.key) {
        if (socket.user.settings.apps.general.lockscreen.key == 'password')
          message = "Please enter your password.";
        else
          message = "Please enter your PIN.";
        return cb(null , null , {
          message: {
            class: "warn",
            message: message
          }
        })
      }
      passport.authenticate('local-unlock', (err,lock,info)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: err.message
            }
          })
        if (lock) {
          socket.device.lock.locked = false;
          socket.device.lock.by = null ;
          socket.device.save((err,device)=>{
            if (err)
              return cb(err);
            return cb(null , true, {
              message: {
                class: "success",
                message: "Device unlocked successfully."
              }
            })
          }
          )
        }
        if (info)
          cb(info)
      }
      )(req, cb);
    }
    ).on('signout device', (data,cb)=>{
      if (data.token) {
        jwt.verify(data.token, keys.user, (err,payload)=>{
          if (err)
            cb(new Error(err.message));
          console.log(payload);
          User.findById(payload._id, (err,user)=>{
            if (err)
              cb(new Error(err.message));
            user.signOutOfDevice(data.deviceid, (err,user)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: 'Device wasn\'t successfully signed out.'
                  }
                })
              user.save((err,user)=>{
                if (err)
                  return cb({
                    message: {
                      class: "error",
                      message: 'Device wasn\'t successfully signed out.'
                    }
                  })
                socket.emit("signout device", data.deviceid)
                cb(null , {
                  message: {
                    class: "success",
                    message: 'Device successfully signed out.'
                  }
                })
              }
              );
            }
            );
          }
          )
        }
        );
      } else {
        socket.user.signOutOfDevice(data.deviceid, (err,user)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: 'Device wasn\'t successfully signed out.'
              }
            })
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: 'Device wasn\'t successfully signed out.'
                }
              })
            socket.to(socket.user.accounts.local.username).emit("signout device", data.deviceid);
            cb(null , {
              message: {
                class: "success",
                message: 'Device successfully signed out.'
              }
            })
          }
          );
        }
        );
      }
    }
    ).on('device info', (data,cb)=>{
      cb(null , socket.device, {
        message: {
          class: "success",
          message: 'Device gotten successfully.'
        }
      })
    }
    ).on('details', (data,cb)=>{
      var usr = {
        Collection: socket.user.Collection,
        details: socket.user.details,
        devices: socket.user.devices,
        email: socket.user.accounts.local.email,
        extras: socket.user.extras,
        finance: socket.user.finance,
        interactions: socket.user.interactions,
        notifications: socket.user.notifications,
        phone: socket.user.details.phone,
        settings: socket.user.settings,
        social: socket.user.social,
        state: socket.user.state,
        username: socket.user.accounts.local.username
      }
      if (socket.device.lock.locked) {
        return cb(null , {
          user: usr,
          devicelocked: true,
          message: {
            class: 'warn',
            message: "This device is locked. Please unlock it first."
          }
        })
      }
      cb(null , {
        user: usr
      })
    }
    ).on('user info', (data,cb)=>{
      var person = {}
      Person = User.findOne({
        'accounts.local.username': data.username
      })
      Person.exec((err,user)=>{
        if (err)
          return cb(err);
        if (!user) {
          return cb({
            message: {
              class: 'info',
              message: "User not found."
            }
          })
        }
        if (user) {
          if (user.settings.social.notifycheckout) {
            user.notify({
              action: 'none',
              concern: 'social',
              devices: 'all',
              device: socket.device._id,
              importance: 1,
              time: Date.now(),
              message: socket.user.accounts.local.username + ' checked you out.'
            }, (err,data)=>{
              if (err)
                return
              socket.to(user.accounts.local.username).emit('new notification', data)
            }
            )
          }
          person.Collection = user.Collection;
          person.title = user.details.title;
          person.firstname = user.details.firstname;
          person.middlename = user.details.middlename;
          person.lastname = user.details.lastname;
          person.gender = user.details.gender;
          person.profpic = user.details.profpic;
          person.about = user.details.about;
          person.phone = user.details.phone;
          person.address = user.details.address;
          person.username = user.accounts.local.username;
          person.email = user.accounts.local.email;
          person.phone = user.details.phone;
          person.befriendable = user.settings.social.allowFriendRequests;
          person.followable = user.settings.social.allowFollows;
          if (user.social.followers.indexOf(socket.user._id) == -1)
            person.followed = false;
          else
            person.followed = true;
          if (user.social.friends.indexOf(socket.user._id) == -1)
            person.isfriend = false;
          else
            person.isfriend = true;
          if (user.social.blockers.indexOf(socket.user._id) == -1)
            person.blocked = false;
          else
            person.blocked = true;
          if (user.social.requests.indexOf(socket.user._id) == -1)
            person.requested = false;
          else
            person.requested = true;
          if (user.social.requested.indexOf(socket.user._id) == -1)
            person.wantsfriendship = false;
          else
            person.wantsfriendship = true;
          user.populate({
            path: 'social.friends social.followers social.following',
            select: 'accounts.local.username',
            model: 'User'
          }, (err,user)=>{
            if (err)
              return cb(err);
            person.friends = user.social.friends;
            person.followers = user.social.followers;
            person.following = user.social.following;
            user.populate({
              path: 'social.friends',
              populate: {
                path: 'social.friends',
                select: '_id',
                model: 'User'
              }
            }, (err,user)=>{
              if (err)
                return cb(err);
              if (user.settings.social.allowProfileViewBy == 'all')
                cb(null , person);
              else if ((user.settings.social.allowProfileViewBy == 'friends') && (user.social.friends.indexOf(socket.user._id != -1)))
                cb(null , person);
              else if ((user.settings.social.allowProfileViewBy == 'friendsoffriends') && present(user.social.friends)) {
                cb(null , person);
              } else
                cb(null , null , {
                  user: user,
                  message: {
                    class: 'info',
                    message: "This user's information is only available to friends. Request him/her as a friend"
                  }
                })
              function present(friends) {
                present = false;
                friends.forEach((friend)=>{
                  if (friend.social.friends.indexOf(socket.user._id) > -1)
                    return present = true;
                }
                );
                return present;
              }
            }
            );
          }
          )
        }
      }
      )
    }
    ).on('activate user', (data,cb)=>{
      jwt.verify(data.token, keys.user, (err,payload)=>{
        if (err)
          return cb(new Error(err.message));
        User.findById(payload._id, (err,user)=>{
          if (err)
            return cb(new Error(err.message))
          socket.user = user
          //activate user if not already active
          socket.user.activate(data.code, (err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "Incorrect code. Please try again."
                }
              })
            Preference.findOne({
              'user': user._id,
              'main': true
            }, (err,pref)=>{
              user.settings.apps = pref._id;
              State.findOne({
                'user': user._id,
                'main': true
              }, (err,st)=>{
                user.state = st._id
                user.save((err,user)=>{
                  //save user
                  if (err)
                    return cb(err);
                  cb(null , {
                    message: {
                      class: "success",
                      message: "Your account was successfully activated. Please Sign In again."
                    }
                  })
                }
                );
              }
              );
            }
            );
          }
          );
        }
        )
      }
      )
    }
    ).on('subscribe', (data,cb)=>{
      var subscription = {
        type: data.subscription
      }
      if (data.subscription) {
        switch (data.subscription) {
        case 'premium':
          subscription.price = 500;
          break;
        case 'basic':
          subscription.price = 250;
          break;
        case 'free':
          subscription.price = 0;
          break;
        }
      } else {
        return cb({
          message: {
            class: "error",
            message: "No subscription selected."
          }
        })
      }
      //return message 
      if (socket.user.finance.credits - subscription.price < 0) {
        return cb({
          message: {
            class: "warn",
            message: "You have insufficient funds in your account you make this upgrade. Please top up and try again."
          }
        })
      } else {
        transaction = {
          name: "Subscription payment",
          for: null ,
          by: socket.user._id,
          For: socket.user._id,
          description: "Subscription fee for " + subscription.type + " package.",
          transactionDate: Date.now(),
          code: shortId(socket.user._id + subscription.type),
          fromDevice: socket.device._id,
          amount: subscription.price
        }
        transaction = new Transaction(transaction);
        transaction.save((err,transaction)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: "Error occured. Please try again later."
              },
              err: err
            })
          //return message
          socket.user.finance.subscription = subscription.type;
          socket.user.finance.credits -= subscription.price;
          socket.user.finance.transactions.push(transaction._id);
          socket.user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "An error occured when upgrading your subscription. Please try again."
                }
              })
            cb({
              finance: user.finance,
              message: {
                class: "success",
                message: "Transaction completed successfully."
              }
            })
            //return message
          }
          );
        }
        );
      }
    }
    ).on('enable pin', (data,cb)=>{
      if (!data.pin)
        return cb(null , null , {
          message: {
            class: "warn",
            message: 'No PIN to save!'
          }
        })
      else if (!data.pin.pinO || !data.pin.pinC) {
        return cb(null , null , {
          message: {
            class: "warn",
            message: 'Please fill in all fields!'
          }
        })
      } else if (data.pin.pinO !== data.pin.pinC) {
        return cb(null , null , {
          message: {
            class: "warn",
            message: 'PINs do not match!'
          }
        })
      } else {
        var pinO = parseInt(data.pin.pinO)
        if (typeof pinO == "number" && pinO.toString().length == 4) {
          if (socket.user.settings.general.universalpin) {
            Preference.findOne({
              'main': true,
              'user': socket.user._id
            }, (err,preference)=>{
              //find preference
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "PIN couldn't be saved"
                  }
                })
              preference.general.lockscreen.haspin = true;
              preference.general.lockscreen.pin = pinO;
              preference.save((err,preference)=>{
                if (err)
                  return cb({
                    message: {
                      class: "error",
                      message: "PIN couldn't be saved"
                    }
                  })
                delete (preference.general.lockscreen.pin);
                user.notify({
                  action: 'confirm',
                  concern: 'security',
                  devices: 'allbutthis',
                  device: socket.device._id,
                  importance: 2,
                  time: Date.now(),
                  message: 'Universal PIN was activated. Was this you?'
                }, (err,data)=>{
                  if (err)
                    return
                  socket.to(user.accounts.local.username).emit('new notification', data)
                }
                )
                cb(null , {
                  preference: preference
                }, {
                  message: {
                    class: "success",
                    message: "PIN saved for all devices."
                  }
                })
              }
              );
            }
            );
          } else {
            Preference.findOne({
              'device': socket.device._id,
              'user': socket.user._id
            }, (err,preference)=>{
              //find preference
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "PIN couldn't be saved"
                  }
                })
              preference.general.lockscreen.haspin = true;
              preference.general.lockscreen.pin = pinO;
              preference.save((err,preference)=>{
                if (err)
                  return cb({
                    message: {
                      class: "error",
                      message: "PIN couldn't be saved"
                    }
                  })
                delete (preference.general.lockscreen.pin);
                cb(null , {
                  preference: preference
                }, {
                  message: {
                    class: "success",
                    message: "PIN saved for this device."
                  }
                })
              }
              );
            }
            );
          }
        } else {
          return cb(null , null , {
            message: {
              class: "warn",
              message: 'The PIN must be a four digit number.'
            }
          })
        }
      }
    }
    ).on('change pin', (data,cb)=>{
      if (!data.pin)
        return cb(null , null , {
          message: {
            class: "warn",
            message: 'No PIN to save!'
          }
        })
      else if (!data.pin.currentpin || !data.pin.pinO || !data.pin.pinC) {
        return cb({
          data: data
        }, null , {
          message: {
            class: "warn",
            message: 'Please fill in all fields!'
          }
        })
      } else if (data.pin.pinO !== data.pin.pinC) {
        return cb(null , null , {
          message: {
            class: "error",
            message: 'PINs do not match!'
          }
        })
      } else {
        var pinO = parseInt(data.pin.pinO)
        var currentpin = parseInt(data.pin.currentpin)
        if (typeof pinO == "number" && pinO.toString().length == 4) {
          if (socket.user.settings.general.universalpin) {
            Preference.findOne({
              'main': true,
              'user': socket.user._id
            }, (err,preference)=>{
              //find preference
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "PIN couldn't be saved"
                  }
                })
              if (preference.general.lockscreen.pin !== currentpin) {
                return cb(null , null , {
                  message: {
                    class: "warn",
                    message: 'Wrong PIN entered!'
                  }
                })
              }
              preference.general.lockscreen.pin = pinO;
              preference.save((err,preference)=>{
                if (err)
                  return cb({
                    message: {
                      class: "error",
                      message: "PIN couldn't be saved"
                    }
                  })
                fn.deleteObjectKey(preference, general.lockscreen.pin, (preference)=>{
                  user.notify({
                    action: 'confirm',
                    concern: 'security',
                    devices: 'allbutthis',
                    device: socket.device._id,
                    importance: 2,
                    time: Date.now(),
                    message: 'Universal PIN was changed. Was this you?'
                  }, (err,data)=>{
                    if (err)
                      return
                    socket.to(user.accounts.local.username).emit('new notification', data)
                  }
                  )
                  cb(null , {
                    preference: preference
                  }, {
                    message: {
                      class: "success",
                      message: "PIN saved for all devices."
                    }
                  })
                }
                );
              }
              );
            }
            );
          } else {
            Preference.findOne({
              'device': socket.device._id,
              'user': socket.user._id
            }, (err,preference)=>{
              //find preference
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "PIN couldn't be saved"
                  }
                })
              if (preference.general.lockscreen.pin !== currentpin) {
                return cb(null , null , {
                  message: {
                    class: "warn",
                    message: 'Wrong PIN entered!'
                  }
                })
              }
              preference.general.lockscreen.pin = pinO
              preference.save((err,preference)=>{
                if (err)
                  return cb({
                    message: {
                      class: "error",
                      message: "PIN couldn't be saved"
                    }
                  })
                fn.deleteObjectKey(preference, "general.lockscreen.pin", (preference)=>{
                  cb(null , {
                    preference: preference
                  }, {
                    message: {
                      class: "success",
                      message: "PIN saved for this device."
                    }
                  })
                }
                )
              }
              )
            }
            )
          }
        } else {
          return cb(null , null , {
            message: {
              class: "warn",
              message: 'The PIN must be a four digit number.'
            }
          })
        }
      }
    }
    ).on('send code', (data,cb)=>{
      jwt.verify(data.token, keys.user, (err,payload)=>{
        if (err)
          return cb(new Error(err.message));
        User.findById(payload._id, (err,user)=>{
          if (err)
            return cb(new Error(err.message))
          socket.user = user
          agent.checkOnlineStatus().then(status=>{
            // if agent is online
            if (status)
              means = data.method
            else
              means = 'none'
            socket.user.sendActivationCode(means, (err,code)=>{
              if (err)
                cb({
                  message: {
                    class: "error",
                    message: "Couldn't send code."
                  },
                  phone: socket.User.details.phone,
                  email: socket.User.accounts.local.email
                })
              cb({
                message: {
                  class: "success",
                  message: "Code sent."
                },
                phone: socket.User.details.phone,
                email: socket.User.accounts.local.email
              })
            }
            )
          }
          )
        }
        )
      }
      )
    }
    ).on('resend code', (data,cb)=>{
      jwt.verify(data.token, keys.user, (err,payload)=>{
        if (err)
          return cb(new Error(err.message));
        User.findById(payload._id, (err,user)=>{
          if (err)
            return cb(new Error(err.message))
          socket.user = user
          socket.user.generateActivationCode()
          socket.user.save();
          agent.checkOnlineStatus().then((status)=>{
            // if agent is online
            if (status)
              means = data.method
            else
              means = 'none'
            socket.user.sendActivationCode(means, (err,code)=>{
              if (err)
                cb({
                  message: {
                    class: "error",
                    message: "Couldn't send code."
                  },
                  phone: socket.user.details.phone,
                  email: socket.user.accounts.local.email
                })
              cb({
                message: {
                  class: "success",
                  message: "Code sent."
                },
                phone: socket.user.details.phone,
                email: socket.user.accounts.local.email
              })
            }
            )
          }
          )
        }
        )
      }
      )
    }
    ).on('active devices', (data,cb)=>{
      jwt.verify(data.token, keys.user, (err,payload)=>{
        if (err)
          cb(new Error(err.message));
        User.findById(payload._id, (err,user)=>{
          if (err)
            cb(new Error(err.message));
          user.populate({
            path: "devices.signedInOn",
            select: "-users -settings -states -__V",
            model: "Device"
          }, (err,user)=>{
            if (err)
              cb(new Error(err.message));
            cb(null , {
              devices: user.devices.signedInOn
            })
          }
          )
        }
        )
      }
      );
    }
    ).on('inactive devices', (data,cb)=>{
      cb({
        devices: socket.user.devices.signedOutOf
      })
    }
    ).on('signout user', (data,cb)=>{
      jwt.verify(data.token, keys.user, (err,payload)=>{
        if (err)
          cb(new Error(err.message));
        User.findById(payload._id, (err,user)=>{
          if (err)
            cb(new Error(err.message));
          socket.user = user
          socket.user.signOutOfDevice(socket.device._id, (err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: 'Error occured while signing you out.'
                }
              })
            user.save((err,user)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: 'Error occured while signing you out.'
                  }
                })
              user.notify({
                action: 'none',
                concern: 'security',
                devices: 'allbutthis',
                device: socket.device._id,
                importance: 2,
                time: Date.now(),
                message: 'You signed out of a device',
                data: socket.device._id
              }, (err,data)=>{
                if (err)
                  return
                socket.to(user.accounts.local.username).emit('new notification', data)
              }
              )
              cb(null , {
                message: {
                  class: "success",
                  message: 'You have been  successfully signed out.'
                }
              })
            }
            )
          }
          )
        }
        )
      }
      )
    }
    ).on('user settings', (data,cb)=>{
      if (socket.user.settings.general.settingsperdevice) {
        socket.user.settings.apps = socket.device.settings;
        socket.user.populate('settings.apps', (err,user)=>{
          if (err)
            return cb(null , socket.user.settings, {
              message: {
                status: 'warn',
                message: 'Couldn\'t get settings for this device. Using general settings'
              }
            })
          cb(null , user.settings, {
            message: {
              status: 'success',
              message: 'Settings updated successfully.'
            }
          })
        }
        )
      } else
        cb(null , socket.user.settings, {
          message: {
            status: 'success',
            message: 'Settings updated successfully.'
          }
        })
    }
    ).on('sync user settings', (data,cb)=>{
      if (!data.settings)
        return cb(null , null , {
          message: {
            class: "warn",
            message: 'Nothing to update!'
          }
        })
      else {
        socket.user.settings.general = data.settings.general;
        socket.user.settings.social = data.settings.social;
        if (socket.user.settings.general.settingsperdevice) {
          Preference.findOne({
            'device': socket.device._id,
            'user': socket.user._id
          }, (err,preference)=>{
            if (err)
              return cb({
                err: err,
                message: {
                  class: "error",
                  message: "Settings couldn't be synced"
                }
              })
            data.settings.apps.general.lockscreen.pin = preference.general.lockscreen.pin;
            preference = extend(preference, data.settings.apps);
            preference.save((err,preference)=>{
              if (err)
                return cb({
                  err: err,
                  message: {
                    class: "error",
                    message: "Settings couldn't be synced"
                  }
                })
              socket.user.save((err,user)=>{
                if (err)
                  return cb({
                    err: err,
                    message: {
                      class: "error",
                      message: "Settings couldn't be synced"
                    }
                  })
                cb(null , {
                  message: {
                    class: "success",
                    message: "Settings saved for this device."
                  }
                })
              }
              );
            }
            );
          }
          );
        } else {
          Preference.findOne({
            'main': true,
            'user': socket.user._id
          }, (err,preference)=>{
            //find preference
            if (err)
              return cb({
                err: err,
                message: {
                  class: "error",
                  message: "Settings couldn't be synced"
                }
              })
            data.settings.apps.general.lockscreen.pin = preference.general.lockscreen.pin;
            preference = extend(preference, data.settings.apps);
            preference.save((err,preference)=>{
              if (err)
                return cb({
                  err: err,
                  message: {
                    class: "error",
                    message: "Settings couldn't be synced"
                  }
                })
              socket.user.save((err,user)=>{
                if (err)
                  return cb({
                    err: err,
                    message: {
                      class: "error",
                      message: "Settings couldn't be synced"
                    }
                  })
                user.notify({
                  action: 'clear',
                  concern: 'settings',
                  devices: 'allbutthis',
                  device: socket.device._id,
                  importance: 1,
                  time: Date.now(),
                  message: 'Settings changed for all your devices'
                }, (err,data)=>{
                  if (err)
                    return
                  socket.to(user.accounts.local.username).emit('new notification', data)
                }
                )
                cb(null , {
                  message: {
                    class: "success",
                    message: "Settings saved for all devices."
                  }
                })
              }
              );
            }
            );
          }
          );
        }
      }
    }
    ).on('sync user state', (data,cb)=>{
      if (!data.state)
        return cb({
          message: {
            class: "error",
            message: 'Nothing to update!'
          }
        })
      if (socket.user) {
        cont()
      } else {
        var int = setInterval(function() {
          if (socket.user) {
            clearInterval(int)
            cont()
          }
        }, 100)
      }
      function cont() {
        if (socket.user.settings.general.statesperdevice) {
          var st, st2;
          State.findOne({
            'device': socket.device._id,
            'user': socket.user._id
          }, (err,state)=>{
            //find preference
            if (err)
              return cb({
                message: {
                  err: err,
                  class: "error",
                  message: "State couldn't be synced"
                }
              })
            state.states = data.state.states;
            state.views = data.state.views;
            state.general = data.state.general;
            state.save((err,state2)=>{
              if (err) {
                cb({
                  message: {
                    state: state,
                    state2: state2,
                    class: "error",
                    message: "State couldn't be synced"
                  }
                })
              } else
                cb(null , {
                  message: {
                    class: "success",
                    message: "State synced successfully."
                  }
                })
            }
            );
          }
          );
        } else {
          State.findOne({
            'main': true,
            'user': socket.user._id
          }, (err,state)=>{
            //find preference
            if (err)
              return cb({
                message: {
                  err: err,
                  class: "error",
                  message: "State couldn't be synced"
                }
              })
            state.states = data.state.states;
            state.views = data.state.views;
            state.general = data.state.general;
            state.save((err,state)=>{
              if (err)
                return cb({
                  message: {
                    err: err,
                    class: "error",
                    message: "State couldn't be synced"
                  }
                })
              cb(null , {
                message: {
                  class: "success",
                  message: "State synced successfully."
                }
              })
            }
            );
          }
          );
        }
      }
    }
    )
    .on('all apps',(data,cb)=>{
      let apps = []
      let app_url = path.resolve(__dirname,`..`,`fs`,`apps`)          
      fs.readdir(app_url, (err,appFolders)=>{
        if (err) cb(err)
        appFolders.forEach(app=>{
          fs.readFile(path.resolve(app_url,app,'app.json'),'utf8',(err,json)=>{
            if (err) cb(err)
            apps.push(json)
            cb ? cb(null,apps) : null
            return apps
          })
        })
      })
    }
    )
    .on('movie collection', (data,cb)=>{
      cb({
        movies: socket.user.Collection.movies
      })
    }
    ).on('shows collection', (data,cb)=>{
      cb({
        shows: socket.user.Collection.shows
      })
    }
    ).on('transactions', (data,cb)=>{
      cb(socket.user.finance.transactions);
    }
    ).on('transaction code', (data,cb)=>{
      Transaction.findOne({
        'code': data.code
      }, (err,transaction)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Transaction not found."
            }
          })
        //return message
        if (transaction) {
          if (socket.user.populated("finance.transactions").toString().indexOf((transaction._id).toString()) == -1) {
            socket.user.finance.credits += transaction.amount;
            socket.user.save((err,user)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "An error occured. Please try again."
                  }
                })
              //return message
              User.findById(user._id).populate({
                path: 'finance.transactions',
                select: '-_id -__v',
                model: 'Transaction',
                populate: {
                  path: 'by For',
                  select: 'accounts.local.username',
                  model: 'User'
                }
              }).exec((err,user)=>{
                if (err)
                  return cb({
                    message: {
                      class: "error",
                      message: err.message
                    }
                  })
                user.notify({
                  action: 'none',
                  concern: 'finance',
                  devices: 'allbutthis',
                  device: socket.device._id,
                  importance: 1,
                  time: Date.now(),
                  message: 'Your account was topped up. Get some new stuff.'
                }, (err,data)=>{
                  if (err)
                    return
                  socket.to(user.accounts.local.username).emit('new notification', data)
                }
                )
                cb(null , {
                  finance: user.finance
                }, {
                  message: {
                    class: "success",
                    message: "Transaction completed."
                  }
                })
              }
              );
            }
            );
          } else {
            cb({
              message: {
                class: "error",
                message: "Transaction has already been posted."
              }
            })
            //return message
          }
        } else {
          transaction = {
            name: data.scheme + " transaction",
            code: data.code,
            amount: 30,
            by: socket.user._id,
            description: "Recieved via " + data.scheme,
            transactionDate: Date.now(),
          }
          transaction = new Transaction(transaction);
          transaction.save((err,transaction)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: err.message
                }
              })
            socket.user.finance.transactions.push(transaction._id);
            socket.user.finance.credits += transaction.amount;
            socket.user.save((err,user)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: err.message
                  }
                })
              User.findById(user._id).populate({
                path: 'finance.transactions',
                select: '-_id -__v',
                model: 'Transaction',
                populate: {
                  path: 'by For',
                  select: 'accounts.local.username',
                  model: 'User'
                }
              }).exec((err,user)=>{
                if (err)
                  return cb({
                    message: {
                      class: "error",
                      message: err.message
                    }
                  })
                user.notify({
                  action: 'none',
                  concern: 'finance',
                  devices: 'allbutthis',
                  device: socket.device._id,
                  importance: 1,
                  time: Date.now(),
                  message: 'Your account was topped up. Get some new stuff.'
                }, (err,data)=>{
                  if (err)
                    return
                  socket.to(user.accounts.local.username).emit('new notification', data)
                }
                )
                cb(null , {
                  finance: user.finance
                }, {
                  message: {
                    class: "success",
                    message: "Transaction completed."
                  }
                })
                //return message
              }
              );
            }
            );
          }
          );
        }
      }
      );
    }
    ).on('credit balance', (data,cb)=>{
      cb(socket.user.finance.credits);
    }
    ).on('friend', (data,cb)=>{
      User.findOne({
        'accounts.local.username': data.username
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "User not found."
            }
          })
        //return message
        if (!user) {
          return cb({
            message: {
              class: "warn",
              message: "User not found. Please Confirm you typed the username correctly."
            }
          })
          //return message
        }
        if (user.accounts.local.username == socket.user.accounts.local.username) {
          return cb({
            message: {
              class: "error",
              message: "Can't send credits to yourself :-D"
            }
          })
          //return message
        }
        cb(null , {
          username: user.accounts.local.username
        }, {
          message: {
            class: "success",
            message: "User found."
          }
        })
        //return message
      }
      );
    }
    ).on('share credit', (data,cb)=>{
      if (socket.user.finance.credits - data.amount < 0) {
        return cb({
          message: {
            class: "error",
            message: "You don't have sufficient credits to make this transaction."
          }
        })
        //return message
      } else {
        User.findOne({
          'accounts.local.username': data.username
        }, (err,user)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: "Error occured. Please try again later."
              }
            })
          //return message
          transaction = {
            name: "Credits transfer",
            for: null ,
            by: socket.user._id,
            For: user._id,
            description: "Transfer of credits by " + socket.user.accounts.local.username + " to " + user.accounts.local.username,
            transactionDate: Date.now(),
            code: shortId(socket.user._id),
            fromDevice: socket.device,
            amount: data.amount
          }
          transaction = new Transaction(transaction);
          user.finance.credits += data.amount;
          user.finance.transactions.push(transaction._id);
          transaction.save((err,transaction)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "Error occured. Please try again later."
                },
                err: err
              })
            //return message
            user.save((err,user)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "Error occured. Please try again later."
                  },
                  err: err
                })
              user.notify({
                action: 'none',
                concern: 'finance',
                devices: 'all',
                importance: 1,
                time: Date.now(),
                message: 'Your account was topped up. Get some new stuff.'
              }, (err,data)=>{
                if (err)
                  return
                socket.to(user.accounts.local.username).emit('new notification', data)
              }
              )
              socket.user.finance.credits -= data.amount;
              socket.user.finance.transactions.push(transaction._id);
              socket.user.save((err,thisuser)=>{
                if (err) {
                  user.finance.credits -= data.amount;
                  user.save((err,user)=>{
                    if (err)
                      return cb({
                        message: {
                          class: "error",
                          message: "Error occured. Please try again later."
                        }
                      })
                    //return message
                  }
                  );
                }
                thisuser.notify({
                  action: 'view',
                  concern: 'finance',
                  devices: 'allbutthis',
                  device: socket.device._id,
                  importance: 1,
                  time: Date.now(),
                  message: 'You transfered credits to ' + user.accounts.local.username,
                  data: user.accounts.local.username
                }, (err,data)=>{
                  if (err)
                    return
                  socket.to(thisuser.accounts.local.username).emit('new notification', data)
                }
                )
                cb({
                  user: socket.user,
                  message: {
                    class: "success",
                    message: "Transaction completed successfully."
                  }
                })
                //return message
              }
              );
            }
            );
          }
          );
        }
        );
      }
    }
    ).on('notifications', (data,cb)=>{
      socket.user.populate({
        path: 'notifications',
        model: 'Notification'
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "err",
              message: "Error occured while fetching your notifications. Please try again later."
            }
          })
        cb(null , socket.user.notifications)
      }
      )
    }
    ).on('notification read', (id,cb)=>{
      Notification.findById(id, (err,notification)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error marking notification as read."
            }
          })
        if (notification) {
          notification.markRead((err,notification)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "Error marking notification as read."
                }
              })
            socket.user.populate({
              path: 'notifications',
              model: 'Notification'
            }, (err,user)=>{
              if (err)
                return cb({
                  message: {
                    class: "err",
                    message: "Error marking notification as unread."
                  }
                })
              cb(null , user.notifications)
            }
            )
          }
          )
        }
      }
      )
    }
    ).on('notification unread', (id,cb)=>{
      Notification.findById(id, (err,notification)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error marking notification as unread."
            }
          })
        notification.markUnread((err,notification)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: "Error marking notification as unread."
              }
            })
          socket.user.populate({
            path: 'notifications',
            model: 'Notification'
          }, (err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "err",
                  message: "Error marking notification as unread."
                }
              })
            cb(null , socket.user.notifications)
          }
          )
        }
        )
      }
      )
    }
    ).on('request', (data,cb)=>{
      if (data.username == socket.user.accounts.local.username)
        return cb(null , null , {
          message: {
            class: "warn",
            message: "You cannot request yourself."
          }
        })
      User.findOne({
        'accounts.local.username': data.username
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error occured. Please try again later."
            }
          })
        //return message
        if (user.social.friends.indexOf(socket.user._id) < 0) {
          if (user.social.requests.indexOf(socket.user._id) < 0) {
            if (!user.settings.social.allowFriendRequests)
              return cb({
                message: {
                  class: "warn",
                  message: "Sorry user doesn't allow friend requests."
                }
              })
            //return message
            user.social.requests.push(socket.user._id);
            socket.user.social.requested.push(user._id);
            user.save((err,user)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "Error occured while placing friend request. Try again later."
                  }
                })
              user.notify({
                action: 'confirm',
                concern: 'social',
                devices: 'all',
                importance: 1,
                time: Date.now(),
                message: socket.user.accounts.local.username + ' wants to be friends.'
              }, (err,data)=>{
                if (err)
                  return
                socket.to(user.accounts.local.username).emit('new notification', data)
              }
              )
              socket.user.save((err,thisuser)=>{
                if (err)
                  return cb({
                    message: {
                      class: "error",
                      message: "Error occured while placing friend request. Try again later."
                    }
                  })
                //return message
                thisuser.populate({
                  path: 'social.requested',
                  select: 'accounts.local.username',
                  model: 'User'
                }, (err,thisuser)=>{
                  socket.user.social.requested = thisuser.social.requested,
                  cb(null , {
                    user: socket.user
                  }, {
                    message: {
                      class: "success",
                      message: "Friend request sent."
                    }
                  })
                  //return message
                }
                );
              }
              );
            }
            );
          } else
            cb(null , {
              user: socket.user
            }, {
              message: {
                class: "info",
                message: "You have already sent a request to this user."
              }
            })
          //return message
        } else {
          cb(null , {
            user: socket.user
          }, {
            message: {
              class: "info",
              message: "You are already friends with this user."
            }
          })
          //return message
        }
      }
      );
    }
    ).on('unrequest', (data,cb)=>{
      if (data.username == socket.user.accounts.local.username)
        return cb(null , {
          user: socket.user
        }, {
          message: {
            class: "warn",
            message: "You cannot use your own username."
          }
        })
      User.findOne({
        'accounts.local.username': data.username
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error occured. Please try again later."
            }
          })
        //return message
        if (user.social.requests.indexOf(socket.user._id) != -1) {
          user.social.requests.pull(socket.user._id);
          socket.user.social.requested.pull(user._id);
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "Error occured while cancelling friend request. Try again later."
                }
              })
            //return message
            socket.user.save((err,thisuser)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "Error occured while cancelling friend request. Try again later."
                  }
                })
              //return message
              thisuser.populate({
                path: 'social.requested',
                select: 'accounts.local.username',
                model: 'User'
              }, (err,thisuser)=>{
                socket.user.social.requested = thisuser.social.requested,
                cb(null , {
                  user: socket.user
                }, {
                  message: {
                    class: "success",
                    message: "Friend request cancelled."
                  }
                })
                //return message
              }
              );
            }
            );
          }
          );
        } else
          cb(null , {
            user: socket.user
          }, {
            message: {
              class: "info",
              message: "You have already sent a request to this user."
            }
          })
        //return message
      }
      );
    }
    ).on('befriend', (data,cb)=>{
      if (data.username == socket.user.accounts.local.username)
        return cb(null , {
          user: socket.user
        }, {
          message: {
            class: "warn",
            message: "You cannot use your own username."
          }
        })
      User.findOne({
        'accounts.local.username': data.username
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error occured. Please try again later."
            }
          })
        //return message
        if (user.social.friends.indexOf(socket.user._id) < 0) {
          if (user.social.requested.indexOf(socket.user._id) >= 0) {
            user.social.friends.push(socket.user._id);
            user.social.requested.pull(socket.user._id);
            User.findById(socket.user._id, (err,ruser)=>{
              ruser.social.friends.push(user._id);
              ruser.social.requests.pull(user._id);
              user.save((err,user)=>{
                if (err)
                  return cb({
                    message: {
                      class: "error",
                      message: "Error occured while confirming friend request. Try again later."
                    }
                  })
                user.notify({
                  action: 'view',
                  concern: 'social',
                  devices: 'all',
                  importance: 1,
                  time: Date.now(),
                  message: 'You and ' + socket.user.accounts.local.username + ' are now friends.',
                  data: socket.user.accounts.local.username
                }, (err,data)=>{
                  if (err)
                    return
                  socket.to(user.accounts.local.username).emit('new notification', data)
                }
                )
                ruser.save((err,thisuser)=>{
                  if (err)
                    return cb({
                      message: {
                        class: "error",
                        message: "Error occured while confirming friend request. Please try again later."
                      }
                    })
                  //return message
                  thisuser.populate({
                    path: 'social.friends',
                    select: 'accounts.local.username',
                    model: 'User'
                  }, (err,thisuser)=>{
                    thisuser.notify({
                      action: 'view',
                      concern: 'social',
                      devices: 'allbutthis',
                      device: socket.device._id,
                      importance: 1,
                      time: Date.now(),
                      message: 'You and ' + ruser.accounts.local.username + ' are now friends.',
                      data: socket.user.accounts.local.username
                    }, (err,data)=>{
                      if (err)
                        return
                      socket.to(thisuser.accounts.local.username).emit('new notification', data)
                    }
                    )
                    socket.user.social.friends = thisuser.social.friends
                    socket.user.social.requests = thisuser.social.requests
                    cb(null , {
                      user: socket.user
                    }, {
                      message: {
                        class: "success",
                        message: "Friend request confirmed."
                      }
                    })
                    //return message
                  }
                  );
                }
                );
              }
              );
            }
            );
          } else {
            cb(null , {
              user: socket.user
            }, {
              message: {
                class: "warn",
                message: "This user has not requested you as a friend."
              }
            })
            //return message
          }
        } else
          cb(null , {
            user: socket.user
          }, {
            message: {
              class: "info",
              message: "You are already friends with this user."
            }
          })
        //return message
      }
      );
    }
    ).on('unfriend', (data,cb)=>{
      if (data.username == socket.user.accounts.local.username)
        return cb(null , {
          user: socket.user
        }, {
          message: {
            class: "warn",
            message: "You cannot use your own username."
          }
        })
      User.findOne({
        'accounts.local.username': data.username
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error occured. Please try again later."
            }
          })
        //return message
        if (user.social.friends.indexOf(socket.user._id) >= 0) {
          user.social.friends.pull(socket.user._id);
          socket.user.social.friends.pull(user._id);
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "Error occured while removing friend. Try again later."
                }
              })
            socket.user.save((err,thisuser)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "Error occured while removing friend. Please try again later."
                  }
                })
              //return message
              thisuser.populate({
                path: 'social.friends',
                select: 'accounts.local.username',
                model: 'User'
              }, (err,thisuser)=>{
                socket.user.social.friends = thisuser.social.friends
                cb(null , {
                  user: socket.user
                }, {
                  message: {
                    class: "success",
                    message: "Friend removed."
                  }
                })
                //return message
              }
              );
            }
            );
          }
          );
        } else
          cb(null , {
            user: socket.user
          }, {
            message: {
              class: "info",
              message: "You are not friends with this user."
            }
          })
        //return message
      }
      );
    }
    ).on('follow', (data,cb)=>{
      if (data.username == socket.user.accounts.local.username)
        return cb(null , {
          user: socket.user
        }, {
          message: {
            class: "warn",
            message: "You cannot follow yourself."
          }
        })
      User.findOne({
        'accounts.local.username': data.username
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error occured. Please try again later."
            }
          })
        //return message
        if (user.social.followers.indexOf(socket.user._id) < 0) {
          if (!user.settings.social.allowFriendRequests)
            return cb(null , {
              user: socket.user
            }, {
              message: {
                class: "warn",
                message: "Sorry, user doesn't allow being followed."
              }
            })
          //return message
          user.social.followers.push(socket.user._id);
          socket.user.social.following.push(user._id);
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "Error occured while placing follow request. Try again later."
                }
              })
            user.notify({
              action: 'view',
              concern: 'social',
              devices: 'all',
              importance: 1,
              time: Date.now(),
              message: socket.user.accounts.local.username + ' is now following you.',
              data: user.accounts.local.username
            }, (err,data)=>{
              if (err)
                return
              socket.to(user.accounts.local.username).emit('new notification', data)
            }
            )
            socket.user.save((err,thisuser)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "Error occured while placing follow request. Try again later."
                  }
                })
              thisuser.populate({
                path: 'social.following',
                select: 'accounts.local.username',
                model: 'User'
              }, (err,thisuser)=>{
                thisuser.notify({
                  action: 'view',
                  concern: 'social',
                  devices: 'allbutthis',
                  device: socket.device._id,
                  importance: 1,
                  time: Date.now(),
                  message: 'You are now following ' + user.accounts.local.username + '.',
                  data: user.accounts.local.username
                }, (err,data)=>{
                  if (err)
                    return
                  socket.to(thisuser.accounts.local.username).emit('new notification', data)
                }
                )
                socket.user.social.following = thisuser.social.following
                cb(null , {
                  user: socket.user
                }, {
                  message: {
                    class: "success",
                    message: "You followed " + user.accounts.local.username + "."
                  }
                })
                //return message})
              }
              );
            }
            );
          }
          );
        } else
          cb(null , {
            user: socket.user
          }, {
            message: {
              class: "info",
              message: "You already follow this user."
            }
          })
        //return message
      }
      );
    }
    ).on('unfollow', (data,cb)=>{
      if (data.username == socket.user.accounts.local.username)
        return cb(null , {
          user: socket.user
        }, {
          message: {
            class: "warn",
            message: "You cannot use your own username."
          }
        })
      User.findOne({
        'accounts.local.username': data.username
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error occured. Please try again later."
            }
          })
        //return message
        if (user.social.followers.indexOf(socket.user._id) >= 0) {
          socket.user.social.following.pull(user._id);
          user.social.followers.pull(socket.user._id);
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "Error occured while placing unfollow request. Try again later."
                }
              })
            //return message
            socket.user.save((err,thisuser)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "Error occured while placing unfollow request. Try again later."
                  }
                })
              thisuser.populate({
                path: 'social.following',
                select: 'accounts.local.username',
                model: 'User'
              }, (err,thisuser)=>{
                socket.user.social.following = thisuser.social.following
                cb(null , {
                  user: socket.user
                }, {
                  message: {
                    class: "success",
                    message: "You unfollowed " + user.accounts.local.username + "."
                  }
                })
                //return message})
              }
              );
            }
            );
          }
          );
        } else
          cb(null , {
            user: socket.user
          }, {
            message: {
              class: "info",
              message: "You haven't followed this user."
            }
          })
        //return message
      }
      );
    }
    ).on('block', (data,cb)=>{
      if (data.username == socket.user.accounts.local.username)
        return cb(null , {
          user: socket.user
        }, {
          message: {
            class: "warn",
            message: "You cannot use your own username."
          }
        })
      User.findOne({
        'accounts.local.username': data.username
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error occured. Please try again later."
            }
          })
        //return message
        if (user.social.blockers.indexOf(socket.user._id) < 0) {
          socket.user.social.blocked.push(user._id);
          user.social.blockers.push(socket.user._id);
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "Error occured while placing block request. Try again later."
                }
              })
            //return message
            socket.user.save((err,thisuser)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "Error occured while placing block request. Try again later."
                  }
                })
              thisuser.populate({
                path: 'social.blocked',
                select: 'accounts.local.username',
                model: 'User'
              }, (err,thisuser)=>{
                socket.user.social.blocked = thisuser.social.blocked
                cb(null , {
                  user: socket.user
                }, {
                  message: {
                    class: "success",
                    message: "You blocked " + user.accounts.local.username + "."
                  }
                })
                //return message})
              }
              );
            }
            );
          }
          );
        } else
          cb({
            message: {
              class: "info",
              message: "You have already blocked this user."
            }
          })
        //return message
      }
      );
    }
    ).on('unblock', (data,cb)=>{
      if (data.username == socket.user.accounts.local.username)
        return cb(null , {
          user: socket.user
        }, {
          message: {
            class: "warn",
            message: "You cannot use your own username."
          }
        })
      User.findOne({
        'accounts.local.username': data.username
      }, (err,user)=>{
        if (err)
          return cb({
            message: {
              class: "error",
              message: "Error occured. Please try again later."
            }
          })
        //return message
        if (user.social.blockers.indexOf(socket.user._id) >= 0) {
          socket.user.social.blocked.pull(user._id);
          user.social.blockers.pull(socket.user._id);
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: "Error occured while placing unblock request. Try again later."
                }
              })
            //return message
            socket.user.save((err,thisuser)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: "Error occured while placing unblock request. Try again later."
                  }
                })
              thisuser.populate({
                path: 'social.blocked',
                select: 'accounts.local.username',
                model: 'User'
              }, (err,thisuser)=>{
                socket.user.social.blocked = thisuser.social.blocked
                cb(null , {
                  user: socket.user
                }, {
                  message: {
                    class: "success",
                    message: "You unblocked " + user.accounts.local.username + "."
                  }
                })
                //return message})
              }
              );
            }
            );
          }
          );
        } else
          cb({
            message: {
              class: "info",
              message: "You haven't blocked this user."
            }
          })
        //return message
      }
      );
    }
    ).on('all movies', (data,cb)=>{
      Movie.find((err,movies)=>{
        if (err)
          cb({
            message: {
              class: "error",
              message: "Couldn\'t get movies."
            }
          })
        if (movies) {
          cb(null , movies);
        }
      }
      );
    }
    ).on('buy movie', (data,cb)=>{
      Movie.findById(data.movieid, (err,movie)=>{
        var req = {}
        movie = movie;
        var newcredits = socket.user.finance.credits - movie.price;
        if (newcredits < 0) {
          return cb({
            message: {
              class: "warn",
              message: "You don\'t have sufficient credits to make this purchase."
            }
          })
        }
        if (socket.user.Collection.movies.indexOf(movie._id) != -1) {
          return cb({
            message: {
              class: "info",
              message: "You already have this movie in your library."
            }
          })
        }
        transaction = {
          name: "Movie purchase",
          code: shortId(movie.imdbID + socket.user._id),
          for: movie._id,
          by: socket.user._id,
          For: socket.user._id,
          description: "Payment for " + movie.title + ".",
          transactionDate: Date.now(),
          fromDevice: req.device,
          amount: movie.price
        }
        transaction = new Transaction(transaction);
        transaction.setRef("Movie");
        socket.user.finance.credits -= movie.price;
        socket.user.finance.transactions.push(transaction._id);
        socket.user.Collection.movies.push(movie._id);
        transaction.save((err,transaction)=>{
          if (err)
            cb(err);
          else {
            movie.owners.push(socket.user._id);
            movie.save((err,movie)=>{
              if (err) {
                cb({
                  message: {
                    class: "error",
                    message: err.message
                  }
                })
              } else {
                socket.user.save((err,user)=>{
                  if (err)
                    cb(err);
                  else
                    socket.user.populate('Collection.movies', (err,user)=>{
                      if (err) {
                        return cb(err);
                      } else {
                        user.notify({
                          action: 'view',
                          concern: 'video',
                          devices: 'allbutthis',
                          device: socket.device._id,
                          importance: 1,
                          time: Date.now(),
                          message: 'New movie added to your library.',
                          data: movie._id
                        }, (err,data)=>{
                          if (err)
                            return
                          socket.to(user.accounts.local.username).emit('new notification', data)
                        }
                        )
                        cb(null , {
                          message: {
                            class: "success",
                            message: "Movie added to your library."
                          },
                          movies: socket.user.Collection.movies
                        })
                      }
                    }
                    );
                }
                );
              }
            }
            );
          }
        }
        );
      }
      );
    }
    ).on('movie download link', (data,cb)=>{
      Movie.findById(data.movieid, (err,movie)=>{
        if (err)
          cb(err);
        var format = data.format;
        // Get the download format
        if (movie.owners.indexOf(socket.user._id) != -1) {
          fn.link(movie.path, format, 'download', function(err, sid) {
            if (err)
              return cb(err)
            cb(null , {
              sid: sid
            })
          })
        } else {
          cb({
            message: {
              class: "warn",
              message: "You don't own this movie.  Please buy it first."
            }
          })
        }
      }
      )
    }
    ).on('movie play link', (data,cb)=>{
      Movie.findById(data.movieid, (err,movie)=>{
        if (err)
          cb(err);
        if (movie.owners.indexOf(socket.user._id) != -1) {
          fn.link(movie.path, "null", 'play', (err,sid)=>{
            if (err)
              return cb(err)
            cb(null , {
              sid: sid
            })
          }
          );
        } else {
          cb({
            message: {
              class: "warn",
              message: "You don't own this movie. Please buy it first."
            }
          })
        }
      }
      )
    }
    ).on('download movie', (data,cb)=>{
      var sid = data.sid
        , format = data.format;
      fn.linkUp(sid, (err,filePath)=>{
        // Get the download file path
        if (err)
          return cb({
            message: {
              class: "error",
              message: err.message + ". Movie temporarily unavailable."
            }
          })
        movie.downloads++;
        movie.save();
        fp = filePath;
        filePath += '.' + format;
        fs.stat(filePath, (err,stats)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: err.message
              }
            })
          var total = stats.size;
          var error = false;
          // Set a flag to check for errors in downloading the file
          if (req.headers['range']) {
            var range = req.headers.range;
            var parts = range.replace(/bytes=/, "").split("-");
            var partialstart = parts[0];
            var partialend = parts[1];
            var start = parseInt(partialstart, 10);
            var end = partialend ? parseInt(partialend, 10) : total - 1;
            var chunksize = (end - start) + 1;
            res.writeHead(206, {
              'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
              'Accept-Ranges': 'bytes',
              'Content-Length': chunksize,
              'Content-Disposition': 'attachment; filename=' + fp,
              'Content-Type': 'video' + format
            })
          } else {
            res.writeHead(200, {
              'Content-Length': total,
              'Content-Disposition': 'attachment; filename=' + fp,
              'Content-Type': "video/" + format
            })
          }
          fs.createReadStream(filePath, {
            start: start ? start : 0,
            end: end ? end : total
          }).pipe(res).on('error', (err)=>{
            // Error when downloading...
            error = true;
            //Set error flag to true
          }
          );
        }
        );
      }
      );
    }
    ).on('play movie', (data,cb)=>{
      Movie.findById(data.movieid, (err,movie)=>{
        if (err)
          cb(err);
        var sid = data.sid
          , format = data.format;
        fn.linkUp(sid, (err,filePath)=>{
          // Get the download file path
          if (err)
            return cb({
              message: {
                class: "error",
                message: "Movie temporarily unavailable."
              }
            })
          fs.stat(filePath + '.' + format, (err,stats)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: err.message
                }
              })
            movie.plays++;
            movie.save();
            var total = stats.size;
            var error = false;
            if (req.headers['range']) {
              var range = req.headers.range;
              var parts = range.replace(/bytes=/, "").split("-");
              var partialstart = parts[0];
              var partialend = parts[1];
              var start = parseInt(partialstart, 10);
              var end = partialend ? parseInt(partialend, 10) : total - 1;
              var chunksize = (end - start) + 1;
              res.writeHead(206, {
                'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
                'Accept-Ranges': 'bytes',
                'Content-Length': chunksize,
                'Content-Type': 'video/' + format
              })
            } else {
              res.writeHead(200, {
                'Content-Length': total,
                'Content-Type': 'video/' + format
              })
            }
            fs.createReadStream(filePath + '.' + format, {
              start: start ? start : 0,
              end: end ? end : total
            }).pipe(res);
          }
          );
        }
        );
      }
      );
    }
    ).on('all movie comments', (data,cb)=>{
      cb(null , movie.comments);
    }
    ).on('add movie comment', (data,cb)=>{
      movie.addComment({
        author: {
          id: socket.user._id,
          username: socket.user.accounts.local.username
        },
        comment: req.body.comment,
        rating: req.body.rating,
        date: Date.now()
      }, (err,movie)=>{
        movie.calcAverageRating((err,movie)=>{
          movie.save(function(err, movie) {
            if (err)
              cb(err);
            else
              cb(null , movie);
          })
        }
        );
      }
      );
    }
    ).on('like movie comment', (data,cb)=>{
      movie.like(req.body.index);
      movie.save(function(err, movie) {
        if (err)
          return cb(err);
        cb(null , movie);
      })
    }
    ).on('dislike movie comment', (data,cb)=>{
      movie.dislike(req.body.index);
      movie.save(function(err, movie) {
        if (err)
          return cb(err)
        cb(null , movie)
      })
    }
    ).on('movie info', (data,cb)=>{
      cb(null , movie);
    }
    ).on('reload movies', (data,cb)=>{
      var newMoviesCount = 0
        , errorCount = 0
        , Err = function() {
        if (newMoviesCount > 0)
          message = ". " + newMoviesCount + " new movies were added.";
        else
          message = "No new movies were added."
        if (errorCount == 1)
          message += " One error occured. Please check the logs.";
        if (errorCount > 1)
          message += " " + errorCount + " errors occured. Please check the logs.";
        return message;
      }
      var loadMovies = Q.Promise((resolve,reject,notify)=>{
        var count = 1
        Q.nfcall(fs.readdir, consts.moviesFolder).done((movies)=>{
          for (var i = 0; i < movies.length; i++) {
            ((i)=>{
              Q.ninvoke(Movie, "find", {
                "path": './fs/movies/files/' + movies[i].split('.')[0].toString()
              }).done((movie)=>{
                if (movie.length > 0) {
                  ext = movies[i].split('.')[movies[i].split('.').length - 1].toString();
                  if (movie[0].availableextensions.indexOf(ext) == -1) {
                    movie[0].availableextensions.push(ext);
                    movie[0].save();
                  }
                  Poster(movie[0]).done((poster)=>{
                    log({
                      log: movie[0].title + "'s poster was updated.",
                      info: movie[0].title + "'s poster was updated."
                    })
                    log({
                      log: movie[0].title + " already exists in the database.",
                      info: movie[0].title + " already exists in the database."
                    })
                    if (count == movies.length)
                      resolve({
                        status: true,
                        message: Err()
                      })
                    count++;
                    return;
                  }
                  , (err)=>{
                    log({
                      log: movie[0].title + "'s poster has issues.",
                      err: err
                    })
                    errorCount++;
                    if (count == movies.length)
                      resolve({
                        status: true,
                        message: Err()
                      })
                    count++;
                    return;
                  }
                  );
                } else {
                  title = movies[i].split('.')[0].toString();
                  title = title.split(' ').join('+');
                  title = title.split('(')[0].toString();
                  query = {
                    scheme: "movie",
                    search: title
                  }
                  if (title.indexOf('(') > -1)
                    year = title.split('(')[1].toString();
                  else
                    year = '';
                  if (year.indexOf(')') > -1)
                    query.year = year.split(')')[0];
                  tmdb(query).then((mov)=>{
                    mov.Poster = mov.poster;
                    Poster(mov).then((poster)=>{
                      mov.poster = poster;
                      mov.Poster = null ;
                      return mov;
                    }
                    , (err)=>{
                      errorCount++;
                      mov.poster = null ;
                      return mov;
                    }
                    ).done((mov)=>{
                      movie = new Movie(mov);
                      movie.path = './fs/movies/files/' + movies[i].split('.')[0].toString();
                      ext = movies[i].split('.')[movies[i].split('.').length - 1].toString();
                      movie.availableextensions.push(ext);
                      movie.save((err,movie)=>{
                        if (err) {
                          errorCount++;
                          log({
                            log: mov.Title + " couldn't be saved!!",
                            err: err
                          })
                          if (count == movies.length)
                            resolve({
                              status: true,
                              message: Err()
                            })
                          count++;
                        } else {
                          newMoviesCount++;
                          if (count == movies.length)
                            resolve({
                              status: true,
                              message: Err()
                            })
                          count++;
                        }
                      }
                      );
                    }
                    );
                  }
                  , (err)=>{
                    errorCount++;
                    log({
                      log: query.search + " couldn't be saved!!",
                      err: err
                    })
                    if (count == movies.length)
                      resolve({
                        status: true,
                        message: Err()
                      })
                    count++;
                  }
                  );
                }
              }
              , (err)=>{
                errorCount++;
                log({
                  log: "Error ",
                  err: err
                })
                if (count == movies.length)
                  resolve({
                    status: true,
                    message: Err()
                  })
                count++;
              }
              );
            }
            )(i);
          }
        }
        , (err)=>reject(err));
      }
      );
      loadMovies.done((complete)=>cb(null , {
        message: {
          class: "success",
          message: 'Database updated successfully. ' + complete.message
        }
      }), (err)=>cb({
        message: {
          class: "error",
          message: Err()
        }
      }));
    }
    ).on('new movie', (data,cb)=>{
      movie = new Movie(req.body);
      movie.save((err,movie)=>{
        //save movie
        if (err)
          return done(err);
        fs.writeFile('./fs/movies/dbfiles/' + movie._id, movie, (err)=>{
          if (err) {
            cb({
              message: {
                class: "error",
                message: err.message
              }
            })
          }
          cb(null , {
            message: {
              class: "success",
              message: "Movie added successfully."
            }
          })
        }
        );
      }
      );
    }
    ).on('all shows', (data,cb)=>{
      show = Show.find();
      season = Season.find();
      episode = Episode.find();
      //show.populate({path: 'seasons', select: '-_id -__v', model: 'Season'})
      //show.populate({path: 'episodes', select: '-_id -__v', model: 'Episode'})
      show.exec((err,shows)=>{
        if (err) {
          log({
            log: "Shows find error.",
            err: err
          })
          return cb(new Error("Shows find error."));
        }
        season.exec((err,seasons)=>{
          if (err) {
            log({
              log: "Seasons find error.",
              err: err
            })
            return cb(new Error("Seasons find error."));
          }
          episode.exec((err,episodes)=>{
            if (err) {
              log({
                log: "Episodes find error.",
                err: err
              })
              return cb(new Error("Episodes find error."));
            }
            cb(null , {
              shows: shows,
              seasons: seasons,
              episodes: episodes
            })
          }
          );
        }
        );
      }
      );
    }
    ).on('buy show', (data,cb)=>{
      req.show.calcPrice();
      newcredits = req.user.finance.credits - req.show.price;
      if (newcredits < 0) {
        return res.status(402).json({
          message: {
            class: "error",
            message: "You don\'t have sufficient credits to make this purchase."
          }
        })
      }
      if (req.user.extras.shows.myshows.indexOf(req.show._id) != -1) {
        return res.status(400).json({
          message: {
            class: "error",
            message: "You already have this show in your library."
          }
        })
      }
      transaction = new Transaction({
        name: "Show purchase",
        for: req.show._id,
        by: req.user._id,
        For: req.user._id,
        description: "Payment for " + req.show.name + " (tv show) by " + req.user.accounts.local.username + ".",
        transactionDate: Date.now(),
        amount: req.show.price
      })
      transaction.setRef("Show");
      req.user.finance.credits -= req.show.price;
      req.user.finance.transactions.push(transaction);
      req.user.extras.shows.myshows.push(req.show._id);
      req.user.extras.show.count++;
      req.user.save((err,user)=>{
        if (err) {
          log({
            log: err.message,
            err: err
          })
          res.status(500).send(err)
        } else {
          transaction.save((err,transaction)=>{
            if (err) {
              log({
                log: err.message,
                err: err
              })
              res.status(500).send(err)
            } else {
              req.show.owners.push(req.user._id);
              req.show.save((err,show)=>{
                if (err) {
                  res.send(err)
                } else
                  cb(null , {
                    message: {
                      class: "success",
                      message: "Show added to your library."
                    },
                    myshows: user.extras.shows
                  })
              }
              );
            }
          }
          );
        }
      }
      );
    }
    ).on('buy season', (data,cb)=>{
      req.season.calcPrice();
      newcredits = req.user.finance.credits - req.season.price;
      if (newcredits < 0) {
        return res.status(402).json({
          message: {
            class: "error",
            message: "You don\'t have sufficient credits to make this purchase."
          }
        })
      }
      if (req.user.extras.shows.myseasons.indexOf(req.season._id) != -1) {
        return res.status(400).json({
          message: {
            class: "error",
            message: "You already have this season in your library."
          }
        })
      }
      transaction = new Transaction({
        name: "Season purchase",
        for: req.season._id,
        by: req.user._id,
        For: req.user._id,
        description: "Payment for " + req.show.name + " (tv season) by " + req.user.accounts.local.username + ".",
        transactionDate: Date.now(),
        amount: req.show.price
      })
      transaction.setRef("Show");
      req.user.finance.credits -= req.show.price;
      req.user.finance.transactions.push(transaction._id);
      req.user.extras.shows.myseasons.push(req.season._id);
      req.user.save(function(err, user) {
        if (err) {
          log({
            log: err.message,
            err: err
          })
          res.status(500).send(err)
        } else {
          transaction.save(function(err, transaction) {
            if (err) {
              log({
                log: err.message,
                err: err
              })
              res.status(500).send(err)
            } else {
              req.show.owners.push(req.user._id);
              req.show.save(function(err, show) {
                if (err) {
                  log({
                    log: err.message,
                    err: err
                  })
                  res.status(500).send(err)
                } else
                  cb(null , {
                    message: {
                      class: "success",
                      message: "Season added to your library."
                    },
                    myshows: user.extras.shows
                  })
              })
            }
          })
        }
      })
    }
    ).on('buy episode', (data,cb)=>{
      if ((req.user.finance.credits - req.episode.price) < 0) {
        return res.status(402).json({
          message: {
            class: "error",
            message: "You don\'t have sufficient credits to make this purchase."
          }
        })
      }
      if (req.user.extras.shows.myepisodes.indexOf(req.episode._id) != -1) {
        return res.status(400).json({
          message: {
            class: "error",
            message: "You already have this episode in your library."
          }
        })
      }
      transaction = new Transaction({
        name: "Episode purchase",
        for: req.episode._id,
        by: req.user._id,
        For: req.user._id,
        description: "Payment for " + req.episode.name + " (tv episode) by " + req.user.accounts.local.username + ".",
        transactionDate: Date.now(),
        amount: req.episode.price
      })
      transaction.setRef("Episode");
      req.user.finance.credits -= req.episode.price;
      req.user.finance.transactions.push(transaction._id);
      req.user.extras.shows.myepisodes.push(req.episode._id);
      req.user.save(function(err, user) {
        if (err) {
          log({
            log: err.message,
            err: err
          })
          res.status(500).send(err)
        } else {
          transaction.save(function(err, transaction) {
            if (err) {
              log({
                log: err.message,
                err: err
              })
              res.status(500).send(err)
            } else {
              req.episode.owners.push(req.user._id);
              req.show.save(function(err, show) {
                if (err) {
                  log({
                    log: err.message,
                    err: err
                  })
                  res.status(500).send(err)
                } else
                  cb(null , {
                    message: {
                      class: "success",
                      message: "Episode added to your library."
                    },
                    myshows: user.extras.shows
                  })
              })
            }
          })
        }
      })
    }
    ).on('show downloadlink', (data,cb)=>{
      if (req.show.owners.indexOf(req.user._id) != -1) {
        fn.link(req.show.path, (err,sid)=>{
          if (err) {
            log({
              log: err.message,
              err: err
            })
            cb(err)
          }
          cb(null , {
            sid: sid
          })
        }
        );
      } else {
        cb({
          message: {
            class: "error",
            message: "You don't own this show. Please buy it first."
          }
        })
      }
    }
    ).on('episode playlink', (data,cb)=>{
      if (req.show.owners.indexOf(req.user._id) != -1) {
        fn.link(req.show.path, (err,sid)=>{
          if (err) {
            log({
              log: err.message,
              err: err
            })
            cb(err)
          }
          cb(null , {
            sid: sid
          })
        }
        );
      } else {
        cb({
          message: {
            class: "error",
            message: "You don't own this tv show. Please buy it first."
          }
        })
      }
    }
    ).on('download show', (data,cb)=>{
      if (req.show.owners.indexOf(req.user._id) != -1) {
        var sid = req.query.sid;
        // Get the download sid
        fn.linkUp(sid, (err,path)=>{
          // Get the download file path
          if (err) {
            log({
              log: err.message,
              err: err
            })
            return res.status(404).json({
              message: {
                class: "error",
                message: "TV Show temporarily unavailable."
              }
            })
          }
          res.writeHead(200, {
            'Content-Length': fs.statSync(path).size,
            'Content-Type': 'video/mp4'
          })
          fs.createReadStream(path).pipe(res).on('end', fn.unlink(sid, (err)=>{}
          ));
          // Finally, delete the link to invalidate it
        }
        );
      } else {
        cb({
          user: req.user,
          show: req.show,
          message: {
            class: "error",
            message: "You don't own this tv show. Please buy it first."
          }
        })
      }
    }
    ).on('play show', (data,cb)=>{
      if (req.user.finance.subscription == "premium") {
        if (req.show.owners.indexOf(req.user._id) != -1) {
          var sid = req.query.sid;
          // Get the download sid
          fn.linkUp(sid, function(err, path) {
            // Get the download file path
            if (err) {
              log({
                log: err.message,
                err: err
              })
              return res.status(404).json({
                message: {
                  class: "error",
                  message: "TV Show temporarily unavailable."
                }
              })
            }
            fs.stat(path, (err,stat)=>{
              var total = stat.size;
              if (req.headers['range']) {
                var range = req.headers.range;
                var parts = range.replace(/bytes=/, "").split("-");
                var partialstart = parts[0];
                var partialend = parts[1];
                var start = parseInt(partialstart, 10);
                var end = partialend ? parseInt(partialend, 10) : total - 1;
                var chunksize = (end - start) + 1;
                res.writeHead(206, {
                  'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
                  'Accept-Ranges': 'bytes',
                  'Content-Length': chunksize,
                  'Content-Type': 'video/mp4'
                })
              } else {
                res.writeHead(200, {
                  'Content-Length': total,
                  'Content-Type': 'video/mp4'
                })
              }
              fs.createReadStream(path, {
                start: start ? start : null ,
                end: end ? end : null
              }).on('end', fn.unlink(sid, (err)=>{}
              )).pipe(res);
            }
            );
          })
        } else {
          cb({
            user: req.user,
            show: req.show,
            message: {
              class: "error",
              message: "You don't own this show. Please buy it first."
            }
          })
        }
      } else {
        cb({
          user: req.user,
          show: req.show,
          message: {
            class: "error",
            message: "This feature is only avilable to premium subscribers."
          }
        })
      }
    }
    ).on('show comment add', (data,cb)=>{
      req.show.addComment({
        author: {
          id: req.user._id,
          username: req.user.accounts.local.username
        },
        comment: req.body.comment,
        rating: req.body.rating,
      })
      req.show.calcAverageRating();
      req.show.save(function(err, show) {
        if (err) {
          return res.send(err)
        } else
          res.json(show);
      })
    }
    ).on('show comment edit', (data,cb)=>{
      req.show.addComment({
        author: {
          id: req.user._id,
          username: req.user.accounts.local.username
        },
        comment: req.body.comment,
        rating: req.body.rating,
      })
      req.show.calcAverageRating();
      req.show.save(function(err, show) {
        if (err) {
          return res.send(err)
        } else
          res.json(show);
      })
    }
    ).on('/shows/season/:seasonid/comments', (data,cb)=>{
      req.season.addComment({
        author: {
          id: req.user._id,
          username: req.user.accounts.local.username
        },
        comment: req.body.comment,
        rating: req.body.rating,
      })
      req.season.calcAverageRating();
      req.season.save(function(err, season) {
        if (err) {
          return res.send(err)
        } else
          res.json(season);
      })
    }
    ).on('/shows/episode/:episodeid/comments', (data,cb)=>{
      req.episode.addComment({
        author: {
          id: req.user._id,
          username: req.user.accounts.local.username
        },
        comment: req.body.comment,
        rating: req.body.rating,
      })
      req.episode.calcAverageRating();
      req.episode.save(function(err, episode) {
        if (err) {
          return res.send(err)
        } else
          res.json(episode);
      })
    }
    ).on('/shows/:showid/comments', (data,cb)=>{
      res.status(200).json(req.show.comments);
    }
    ).on('/shows/season/:seasonid/comments', (data,cb)=>{
      res.status(200).json(req.season.comments);
    }
    ).on('/shows/episode/:episodeid/comments', (data,cb)=>{
      res.status(200).json(req.episode.comments);
    }
    ).on('reload shows', (data,cb)=>{
      loadShows = (dir,done)=>{
        fs.readdir(dir, (err,shows)=>{
          if (err)
            return log({
              log: "Shows folder read error.",
              err: err
            }, ()=>{
              done(this.err);
            }
            );
          var Scount = shows.length;
          if (!Scount)
            return done(null , results);
          for (sh of shows) {
            ((sh)=>{
              query = {
                tag: "tv",
                type: "series",
                search: sh.split(' ').join('+'),
                plot: "full",
                response: "json"
              }
              tmdb(query, (shw)=>{
                if (shw) {
                  Show.find({
                    imdbID: shw.imdbID
                  }, (err,show)=>{
                    if (err)
                      return log({
                        log: "Show find error.",
                        err: err
                      })
                    if (!show.length) {
                      parts = shw.Poster.split('.');
                      ext = parts[parts.length - 1];
                      options = {
                        host: url.parse(shw.Poster).host,
                        port: 80,
                        path: url.parse(shw.Poster).pathname,
                        file: shw.imdbID + '.' + ext,
                        item: shw.Title
                      }
                      Poster(options, (poster)=>{
                        if (poster) {
                          show = new Show();
                          show.actors = shw.Actors;
                          show.awards = shw.Awards;
                          show.country = shw.Country;
                          show.director = shw.Director;
                          show.genre = shw.Genre;
                          show.imdbRating = shw.imdbRating;
                          show.imdbID = shw.imdbID;
                          show.language = shw.Language;
                          show.plot = shw.Plot;
                          show.poster = poster;
                          show.rated = shw.Rated;
                          show.released = shw.Released;
                          show.runtime = shw.Runtime;
                          show.title = shw.Title;
                          show.writer = shw.Writer;
                          show.year = shw.Year;
                          show.path = path.resolve(dir, sh);
                          show.save((err,Sh)=>{
                            if (err)
                              return log({
                                log: shw.name + " save error.",
                                err: err
                              })
                            Scount--;
                            if (Scount === 0) {
                              log({
                                log: Sh.title + " save success.",
                                success: ""
                              })
                              return done(null , shows);
                            }
                          }
                          );
                        }
                      }
                      );
                    }
                  }
                  );
                }
              }
              );
            }
            )(sh);
          }
        }
        );
      }
      loadSeasons = (done)=>{
        Show.find((err,shows)=>{
          for (sh of shows) {
            ((sh)=>{
              fs.readdir(sh.path, (err,seasons)=>{
                if (err)
                  return log({
                    log: "seasons read error.",
                    err: err
                  })
                scount = seasons.length;
                if (!scount)
                  return log({
                    log: "Empty folder",
                    err: show.path
                  })
                for (ssn of seasons) {
                  ((ssn)=>{
                    query = {
                      tag: "t=",
                      type: "series",
                      search: sh.title.split(' ').join('+'),
                      plot: "full",
                      response: "json",
                      season: ssn.split(' ')[1]
                    }
                    tmdb(query, (sn)=>{
                      if (sn) {
                        season = new Season();
                        season.actors = sh.actors;
                        season.genre = sh.Genre;
                        season.name = sh.title + ': Season ' + sn.Season;
                        season.number = sn.Season;
                        season.path = path.resolve(sh.path, ssn);
                        season.poster = sh.poster;
                        season.show = sh._id;
                        season.eps = sn.Episodes;
                        season.save((err,Sn)=>{
                          if (err)
                            return log({
                              log: season.name + " save error.",
                              err: err
                            })
                          scount--;
                          if (scount === 0) {
                            log({
                              log: Sn.title + " save success.",
                              success: ""
                            })
                            return done(null , seasons);
                          }
                        }
                        );
                      }
                    }
                    );
                  }
                  )(ssn);
                }
              }
              );
            }
            )(sh);
          }
        }
        );
      }
      loadEpisodes = (done)=>{
        Season.find((err,seasons)=>{
          for (sn of seasons) {
            ((sn)=>{
              fs.readdir(sn.path, (err,episodes)=>{
                if (err)
                  return log({
                    log: "episodes read error.",
                    err: err
                  })
                ecount = episodes.length;
                if (!ecount)
                  return log({
                    log: "Empty folder",
                    err: season.path
                  })
                if (episodes.length < sn.eps.length)
                  log({
                    log: sn.name + " has missing episodes.",
                    warn: ""
                  })
                if (episodes.length > sn.eps.length)
                  log({
                    log: sn.name + " has excess files (episodes).",
                    warn: ""
                  })
                for (epsd of episodes) {
                  ((epsd)=>{
                    query = {
                      tag: "t=",
                      type: "series",
                      search: sn.name.split(':')[0].split(' ').join('+'),
                      plot: "full",
                      response: "json",
                      season: sn.number,
                      episode: epsd.split(' ')[1].split('.')[0]
                    }
                    tmdb(query, (ep)=>{
                      if (ep) {
                        Episode.find({
                          imdbID: ep.imdbID
                        }, (err,episode)=>{
                          if (err)
                            return log({
                              log: "episode find error",
                              err: err
                            })
                          if (!episode.length) {
                            parts = ep.Poster.split('.');
                            ext = parts[parts.length - 1];
                            options = {
                              host: url.parse(ep.Poster).host,
                              port: 80,
                              path: url.parse(ep.Poster).pathname,
                              file: ep.imdbID + '.' + ext,
                              item: ep.Title
                            }
                            Poster(options, (poster)=>{
                              if (poster) {
                                episode = new Episode();
                                episode.actors = ep.Actors;
                                episode.awards = ep.Awards;
                                episode.country = ep.Country;
                                episode.director = ep.Director;
                                episode.episodeNumber = ep.Episode;
                                episode.genre = ep.Genre;
                                episode.imdbID = ep.imdbID;
                                episode.imdbRating = ep.imdbRating;
                                episode.language = ep.Language;
                                episode.name = sn.name + ' - ' + epsd.split('.')[0];
                                episode.number = ep.Episode;
                                episode.path = path.resolve(sn.path, epsd);
                                episode.plot = ep.Plot;
                                episode.poster = poster;
                                episode.show = sn.show;
                                episode.season = sn._id;
                                episode.seasonNumber = ep.Season;
                                episode.seriesID = ep.seriesID;
                                episode.rated = ep.Rated;
                                episode.released = ep.Released;
                                episode.runtime = ep.Runtime;
                                episode.title = ep.Title;
                                episode.writer = ep.Writer;
                                episode.year = ep.Year;
                                episode.save((err,Ep)=>{
                                  if (err)
                                    return log({
                                      log: episode.name + " save error",
                                      err: err
                                    })
                                  ecount++;
                                  if (ecount == episodes.length - 1) {
                                    log({
                                      log: Ep.title + " save success",
                                      success: ""
                                    })
                                    return done(null , episodes);
                                  }
                                }
                                );
                              }
                            }
                            );
                          }
                        }
                        );
                      }
                    }
                    );
                  }
                  )(epsd)
                }
              }
              );
            }
            )(sn);
          }
        }
        );
      }
      affialiate = (done)=>{
        var SHOWS = null
          , SEASONS = null
          , EPISODES = null ;
        Show.find((err,shows)=>{
          if (err) {
            log({
              log: "Shows find error: affialiate.",
              err: err
            })
            return done(err);
          }
          SHOWS = shows;
          SHOWS.find().sort({
            "name": -1
          }).toArray((err,shows)=>{
            Scount = shows.length;
            for (var i = 0; i < SHOWS.length; ++i) {
              Season.find((err,seasons)=>{
                if (err) {
                  log({
                    log: "Seasons find error: affialiate.",
                    err: err
                  })
                  return done(err);
                }
                SEASONS = seasons;
                SEASONS.find({
                  "show": shows[i]['_id']
                }).sort({
                  "name": -1
                }).toArray((err,seasons)=>{
                  scount = seasons.length;
                  for (var j = 0; j < seasons.length; ++j) {
                    if (shows[i]['seasons'].indexOf(seasons[j]['_id'] == -1))
                      shows[i]['seasons'].push(seasons[j]['_id']);
                    Episode.find((err,episodes)=>{
                      if (err) {
                        log({
                          log: "Shows find error: affialiate.",
                          err: err
                        })
                        return done(err);
                      }
                      EPISODES = episodes;
                      EPISODES.find({
                        "show": shows[i]['_id']
                      }).sort({
                        "name": -1
                      }).toArray((err,episodes)=>{
                        ecount = episodes.length;
                        for (var k = 0; k < episodes.length; ++k) {
                          if (shows[i]['episodes'].indexOf(episodes[k]['_id'] == -1))
                            shows[i]['episodes'].push(episodes[k]['_id']);
                          if (episodes[k]['season'] == seasons[j]['_id'])
                            seasons[j]['episodes'].push(episodes[k]['_id']);
                          ecount--;
                          if (ecount === 0) {
                            seasons[j].save((err,show)=>{
                              if (err) {
                                log({
                                  log: "season save error: affialiate.",
                                  err: err
                                })
                                return done(err);
                              }
                              scount--;
                              if (scount === 0) {
                                shows[i].save((err,show)=>{
                                  if (err) {
                                    log({
                                      log: "Show save error: affialiate.",
                                      err: err
                                    })
                                    return done(err);
                                  }
                                  Scount--;
                                  if (Scount === 0)
                                    done(null , shows);
                                }
                                );
                              }
                            }
                            );
                          }
                        }
                      }
                      );
                    }
                    );
                  }
                }
                );
              }
              );
            }
          }
          );
        }
        );
      }
      loadShows(consts.showsFolder, (err,shows)=>{
        if (err) {
          socket.emit("error", {
            status: 500,
            message: err.message
          })
        }
        loadSeasons((err,seasons)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: err.message
              }
            })
          loadEpisodes((err,episodes)=>{
            affialiate((err,shows)=>{
              if (err)
                return cb({
                  message: {
                    class: "error",
                    message: err.message
                  }
                })
              if (shows)
                cb(null , {
                  message: {
                    class: "success",
                    message: 'Database updated successfully'
                  }
                })
            }
            )
          }
          );
        }
        );
      }
      );
    }
    ).on('new show', (data,cb)=>{
      show = new Show(data,cb);
      show.save(function(err, show) {
        //save movie
        if (err)
          return cb(err);
        fs.writeFile('./fs/shows/dbfiles/' + show._id, show, (err)=>{
          if (err) {
            socket.emit("error", {
              status: 500,
              message: err.message
            })
          }
          cb(null , {
            message: "Show added successfully."
          })
        }
        );
      })
    }
    ).on('start chat', (data,cb)=>{
      if (data.name) {
        if (data.name == 'public') {
          socket.join('public')
          cb(null , null , {
            message: {
              class: 'success',
              message: 'chat away'
            }
          })
        } else {
          User.findOne({
            'accounts.local.username': data.name
          }, (err,user)=>{
            if (err)
              return cb(err)
            Chat.findOne({
              'user': socket.user._id,
              'with': user._id
            }).populate({
              path: 'messages',
              populate: {
                path: 'sender.user',
                select: 'accounts.local.username'
              }
            }).populate({
              path: 'messages',
              populate: {
                path: 'sender.device'
              }
            }).populate({
              path: 'with',
              select: 'accounts.local.username'
            }).exec((err,chat)=>{
              if (err)
                return cb(err)
              if (chat) {
                cb(null , {
                  chat: chat,
                  user: socket.user
                }, {
                  message: {
                    class: 'success',
                    message: 'chat away'
                  }
                })
              } else {
                var newChat = new Chat({
                  user: socket.user._id,
                  with: user._id,
                  participants: [socket.user._id, user._id]
                })
                var newChat2 = new Chat({
                  user: user._id,
                  with: socket.user._id,
                  counterpart: newChat._id,
                  participants: [user._id, socket.user._id]
                })
                newChat.counterpart = newChat2._id
                newChat2.counterpart = newChat._id
                newChat.save((err,chat)=>{
                  if (err)
                    return cb(err)
                  newChat2.save((err,chat2)=>{
                    if (err)
                      return cb(err)
                    socket.user.interactions.chats.push(newChat._id)
                    socket.user.save((err,suser)=>{
                      user.interactions.chats.push(newChat2._id)
                      user.save((err,suser)=>{
                        chat.populate({
                          path: 'with',
                          select: 'accounts.local.username',
                          model: 'User'
                        }, (err,chat)=>{
                          cb(null , {
                            chat: chat,
                            user: socket.user
                          }, {
                            message: {
                              class: 'success',
                              message: 'chat away'
                            }
                          })
                        }
                        )
                        for (var i = 0; i < chat.participants; i++) {
                          User.findById(chat.participants[i], (err,user)=>{
                            if (user.accounts.local.username != socket.user.accounts.local.username)
                              socket.to(user.accounts.local.username).emit('user chat active', socket.user.accounts.local.username)
                          }
                          )
                        }
                      }
                      )
                    }
                    )
                  }
                  )
                }
                )
              }
            }
            )
          }
          )
        }
      } else {
        Chat.findById(data.id).populate({
          path: 'messages',
          populate: {
            path: 'sender.user',
            select: 'accounts.local.username'
          }
        }).populate({
          path: 'messages',
          populate: {
            path: 'sender.device'
          }
        }).populate({
          path: 'with',
          select: 'accounts.local.username'
        }).exec((err,chat)=>{
          if (err)
            return cb(err)
          for (var i = 0; i < chat.participants; i++) {
            User.findById(chat.participants[i], (err,user)=>{
              if (user.accounts.local.username != socket.user.accounts.local.username)
                socket.to(user.accounts.local.username).emit('user chat active', socket.user.accounts.local.username)
            }
            )
          }
          cb(null , {
            chat: chat,
            user: socket.user
          }, {
            message: {
              class: 'success',
              message: 'chat away'
            }
          })
        }
        )
      }
    }
    ).on('chat message', (data,cb)=>{
      Chat.findById(data.chat, (err,chat)=>{
        if (err)
          return cb(err)
        Chat.findById(chat.counterpart, (err,chat2)=>{
          if (err)
            return cb(err)
          var msg = new Message({
            text: data.text,
            time: Date.now(),
            sender: {
              user: data.sender,
              device: data.device
            }
          })
          msg.save((err,msg)=>{
            if (err)
              return cb(err)
            chat.messages.push(msg._id)
            chat2.messages.push(msg._id)
            chat.save((err,chat)=>{
              if (err)
                return cb(err)
              chat2.save((err,chat)=>{
                if (err)
                  return cb(err)
                Message.findById(msg._id).populate({
                  path: 'sender.user',
                  select: 'accounts.local.username',
                  model: 'User'
                }).populate({
                  path: 'sender.device',
                  model: 'Device'
                }).exec((err,msg)=>{
                  for (var i = 0; i < chat.participants; i++) {
                    User.findById(chat.participants[i], (err,user)=>{
                      if (user.accounts.local.username != socket.user.accounts.local.username)
                        socket.to(user.accounts.local.username).emit('chat message', {
                          msg: msg,
                          username: socket.user.accounts.local.username
                        })
                    }
                    )
                  }
                  cb(null , {
                    msg: msg
                  })
                }
                )
              }
              )
            }
            )
          }
          )
        }
        )
      }
      )
    }
    ).on('online status', (status,cb)=>{
      console.log("user's online status changed to ", status);
    }
    ).on('mpesa transaction', (req,res)=>{
      User.find({
        'details.phone': req.body.phone
      }, (err,user)=>{
        transaction = {
          name: "M-PESA transaction",
          code: req.body.code,
          text: req.body.text,
          amount: req.body.amount,
          by: user._id,
          description: "Sent " + req.body.amount + " via M-PESA.",
          transactionDate: Date.now(),
        }
        transaction = new Transaction(transaction);
        transaction.save((err,transaction)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: err.message
              }
            })
          user.finance.transactions.push(transaction._id);
          user.finance.credits += transaction.amount;
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: err.message
                }
              })
            cb(null , {
              message: {
                class: "success",
                message: "Transaction added."
              }
            })
          }
          );
        }
        );
      }
      );
    }
    ).on('airtelmoney transaction', (req,res)=>{
      User.find({
        'details.phone': req.body.phone
      }, (err,user)=>{
        transaction = {
          name: "Airtel Money transaction",
          code: req.body.code,
          text: req.body.text,
          amount: req.body.amount,
          by: user._id,
          description: "Recieved " + req.body.amount + " via Airtel Money.",
          transactionDate: Date.now(),
        }
        transaction = new Transaction(transaction);
        transaction.save((err,transaction)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: err.message
              }
            })
          user.finance.transactions.push(transaction._id);
          user.finance.credits += transaction.amount;
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: err.message
                }
              })
            cb(null , {
              message: {
                class: "success",
                message: "Transaction added."
              }
            })
          }
          );
        }
        )
      }
      );
    }
    ).on('equitel transaction', (req,res)=>{
      User.find({
        'details.phone': req.body.phone
      }, (err,user)=>{
        transaction = {
          name: "Equitel transaction",
          code: req.body.code,
          text: req.body.text,
          amount: req.body.amount,
          by: user._id,
          description: "Recieved " + req.body.amount + " via Equitel.",
          transactionDate: Date.now(),
        }
        transaction = new Transaction(transaction);
        transaction.save((err,transaction)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: err.message
              }
            })
          user.finance.transactions.push(transaction._id);
          user.finance.credits += transaction.amount;
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: err.message
                }
              })
            cb(null , {
              message: {
                class: "success",
                message: "Transaction added."
              }
            })
          }
          );
        }
        );
      }
      );
    }
    ).on('mobikash transaction', (req,res)=>{
      User.find({
        'details.phone': req.body.phone
      }, (err,user)=>{
        transaction = {
          name: "Mobikash transaction",
          code: req.body.code,
          text: req.body.text,
          amount: req.body.amount,
          by: user._id,
          description: "Recieved " + req.body.amount + " via Mobikash.",
          transactionDate: Date.now(),
        }
        transaction = new Transaction(transaction);
        transaction.save((err,transaction)=>{
          if (err)
            return cb({
              message: {
                class: "error",
                message: err.message
              }
            })
          user.finance.transactions.push(transaction._id);
          user.finance.credits += transaction.amount;
          user.save((err,user)=>{
            if (err)
              return cb({
                message: {
                  class: "error",
                  message: err.message
                }
              })
            cb(null , {
              message: {
                class: "success",
                message: "Transaction added."
              }
            })
          }
          );
        }
        );
      }
      );
    }
    ).on('error', (err)=>{
      console.log(err)
    }
    ).on('disconnect', ()=>{
      if (socket.user)
        socket.to(socket.user.accounts.local.username).emit("device is now offline", {
          device: socket.device
        })
    }
    )
  }
  )
}
