'use strict'
// Load required packages
var mongoose = require('mongoose')
// Define a call schema
var CallSchema = new mongoose.Schema({
  class: {
    type: String,
    required: true,
    default: 'OneOnOne'
  },
  caller: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  device: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Device"
  },
  duration: {
    type: Number,
    required: true,
    default: 0
  },
  recipient: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  partcipants: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }],
  time: {
    type: Date,
    required: true,
    default: Date(Date.now())
  }
})
// methods ======================
CallSchema.methods.addPartcipant = function(userid, cb) {
  this.partcipants.push(userid)
  this.class = "Conference"
  cb(null , this)
}
CallSchema.methods.call = function(event,cb) {
  var count
  if (event = 'dial') {
    count = setInterval(function(){
      this.duration++
      cb(null,this.duration)
    },1000)
  }
  if (event = 'end') {    
    clearInterval(count)
    cb(null,this.duration)
  }
}
// create the model for calls and expose it to our app
module.exports = mongoose.model('Call', CallSchema)
