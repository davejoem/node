var lockController = function($scope, $interval, $client) {
  $scope.slides = [{
    id: "image00",
    src: "/images/img00.jpg"
  }, {
    id: "image01",
    src: "/images/img01.jpg"
  }, {
    id: "image02",
    src: "/images/img02.jpg"
  }, {
    id: "image03",
    src: "/images/img03.jpg"
  }, {
    id: "image04",
    src: "/images/img04.jpg"
  }]
  $scope.direction = 'left'
  $scope.start = function(event) {
    $client.ripple.attach($scope, angular.element(event.target), {
      center: true
    })
    $state.go('apps')
  }
  $scope.currentIndex = 0
  $scope.setCurrentSlideIndex = function(index) {
    $scope.direction = (index > $scope.currentIndex) ? 'left' : 'right'
    $scope.currentIndex = index
  }
  $scope.isCurrentSlideIndex = function(index) {
    return $scope.currentIndex === index
  }
  $scope.nextSlide = function() {
    $scope.direction = 'left'
    $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0
  }
  $scope.prevSlide = function() {
    $scope.direction = 'right'
    $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1
  }
  $scope.unlock = function() {
    $client.unlockSession($scope.key).then(function(success) {
      $scope.unlocked = true
    }, function(error) {
      $scope.error = error
      $client.toast.toast(error)
    }, function(info) {
      console.log(info)
      $client.toast.toast({
        theme: info.message.class,
        textContent: info.message.message,
        hideDelay: 1000,
      }).then(function() {
        $client.navigator.resume()
      })
    })
  }
  function loadSlides() {
    $interval(function() {
      $scope.nextSlide()
    }, 5000)
  }
  loadSlides()
}
lockController.$inject = ['$scope', '$interval', '$client']
app.controller('lockController', lockController)
