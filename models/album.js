'use strict'
// Load required packages
var mongoose = require('mongoose')
//models
var Playlist = require('./playlist')
// Define a call schema
var AlbumSchema = new mongoose.Schema({
  albumart: String,
  artist: {
    type: mongoose.Schem.Type.ObjectId,
    ref: 'Artist'
  },
  awards: [{
    type: String
  }],
  averagerating: {
    type: Number,
    default: 0,
    required: true
  },
  averagebitrate: String,
  contibutingartists: [{
    type: mongoose.Schem.Type.ObjectId,
    ref: 'Artist'
  }],
  comments: [{
    author: {
      id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
      },
      username: {
        type: String,
        required: true
      },
    },
    comment: {
      type: String,
      required: true
    },
    rating: {
      type: Number,
      default: 0,
      required: true
    },
    likes: {
      type: Number,
      default: 0,
      required: true
    },
    dislikes: {
      type: Number,
      default: 0,
      required: true
    },
    date: {
      type: Date,
      required: true,
      default: Date.now()
    }
  }],
  id: Number,
  country: String,
  genre: String,
  subgenres: [{
    id: Number,
    name: String
  }],
  homepage: String,
  language: String,
  length: {
    type: String,
    required: true
  },
  metascore: String,
  mood: String,
  overview: String,
  owners: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    unique: true
  }],
  path: {
    type: String,
    required: true,
    unique: true
  },
  plays: {
    type: Number,
    default: 0,
    required: true
  },
  producer: String,
  rated: {
    type: String,
    required: true
  },
  release_date: String,
  revenue: Number,
  sharers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    unique: true
  }],
  sharedwiths: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    unique: true
  }],
  skips: {
    type: Number,
    default: 0,
    required: true
  },
  songs: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Song'
  }],
  studios: [{
    type: String
  }],
  title: {
    type: String,
    required: true
  },
  track_no: Number,
  type: {
    type: String,
    required: false
  },
  videos: [{
    type: String
  }],
  writers: [{
    type: String
  }],
  year: String
})
// methods ======================
AlbumSchema.methods.addToPlaylist = function(playlistid, cb) {
  Playlist.findById(playlistid, (err,playlist)=>{
    if (err)
      return cb(err)
    for (var song of this.songs) {
      playlist.songs.push(song)
    }
    playlist.save()
    cb(null , this)
  }
  )
}
AlbumSchema.methods.calcAverageRating = function(cb) {
  var total = 0
    , averagerating = 0;
  for (i = 0; i < this.comments.length; i++) {
    total = this.comments[i].rating + total;
    averagerating = total / this.comments.length;
  }
  Math.round(averagerating);
  this.averagerating = averagerating;
  if (cb)
    cb(null , this);
  else
    return this;
}
// create the model for songs and expose it to our app
module.exports = mongoose.model('Album', AlbumSchema);
