function ideasController($scope, $client){
    $scope.settings = $client.user.details.settings.apps.ideas;
}
ideasController.$inject = ['$scope','$client'];
app.controller('ideasController', ideasController);