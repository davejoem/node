'use strict'
// Load required packages
var mongoose = require('mongoose')
//models
var Playlist = require('./playlist')
// Define a call schema
var ArtistSchema = new mongoose.Schema({
  albums: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Album'
  }],
  awards: [{
    type: String
  }],
  averagerating: {
    type: Number,
    default: 0,
    required: true
  },
  bio: String,
  comments: [{
    author: {
      id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
      },
      username: {
        type: String,
        required: true
      },
    },
    comment: {
      type: String,
      required: true
    },
    rating: {
      type: Number,
      default: 0,
      required: true
    },
    likes: {
      type: Number,
      default: 0,
      required: true
    },
    dislikes: {
      type: Number,
      default: 0,
      required: true
    },
    date: {
      type: Date,
      required: true,
      default: Date.now()
    }
  }],
  id: Number,
  country: String,
  genre: String,
  subgenres: [{
    id: Number,
    name: String
  }],
  website: String,
  language: String,
  overview: String,
  picture: String,
  songs: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Song'
  }],
  videos: [{
    type: String
  }]  
})
// methods ======================
ArtistSchema.methods.addToPlaylist = function(playlistid, cb) {
  Playlist.findById(playlistid, (err,playlist)=>{
    if (err)
      return cb(err)
    for (var song of this.songs) {
      playlist.songs.push(song)
    }
    playlist.save()
    cb(null , this)
  }
  )
}
ArtistSchema.methods.calcAverageRating = function(cb) {
  var total = 0
    , averagerating = 0;
  for (i = 0; i < this.comments.length; i++) {
    total = this.comments[i].rating + total;
    averagerating = total / this.comments.length;
  }
  Math.round(averagerating);
  this.averagerating = averagerating;
  if (cb)
    cb(null , this);
  else
    return this;
}
// create the model for songs and expose it to our app
module.exports = mongoose.model('Album', AlbumSchema);
