'use strict'
// Load required packages
var mongoose = require('mongoose')
//models
var User = require('./user')
var Song = require('./song')
var Album = require('./album')
// Define a call schema
var PlaylistSchema = new mongoose.Schema({
  private: {
    type: Boolean,
    required: true,
    default: false
  },
  songs: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Song"
  }],
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  followers: [{
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    device: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Device"
    }
  }]
})
// methods ======================
PlaylistSchema.methods.addSong = function(songid, cb) {
  this.songs.push(userid);
  cb(null , this);
}
// create the model for playlists and expose it to our app
module.exports = mongoose.model('Playlist', PlaylistSchema);
