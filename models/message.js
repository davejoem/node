'use strict'
// Load required packages
var mongoose = require('mongoose');
// Define a message schema
var MessageSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true
  },
  sender: {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    device: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Device",
      required: true
    }
  },
  time: {
    type: Date,
    required: true,
    default: Date.now()
  },
  recieved: {
    type: Boolean,
    required: true,
    default: false
  },
  read: {
    type: Boolean,
    required: true,
    default: false
  }
});
// methods ======================
MessageSchema.methods.markRead = function(userid, cb) {
  this.read = true;
  cb(null , this);
}
MessageSchema.methods.markRecieved = function(index, cb) {
  this.recieved = true;
  cb(null, this);
}
// create the model for devices and expose it to our app
module.exports = mongoose.model('Message', MessageSchema);