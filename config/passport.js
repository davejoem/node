module.exports = (agent)=>{
  // config/passport.js
  // load all the things we need
  var BasicStrategy = require('passport-http').BasicStrategy
  var LocalStrategy = require('passport-local').Strategy
  var FacebookStrategy = require('passport-facebook').Strategy
  var TwitterStrategy = require('passport-twitter').Strategy
  var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
  var User = agent.models.User
  var Device = agent.models.Device
  var Preference = agent.models.Preference
  var State = agent.models.State
  var passport = agent.pass
  var io = agent.io
  // load the auth variables
  var keys = require('./keys')
  // import settings for social networks
  var allIsWell = function(user, req, done) {
    // all is well
    token = user.generateToken()
    if (user.devices.signedInOn.indexOf(req.device._id) == -1) {
      user.signInOnDevice(req.device._id, (err,user)=>{
        //add device to signin list
        user.save(function(err, user) {
          //save user
          if (err) {
            return done(err)
          }
          Preference.findOne({
            'device': req.device._id,
            'user': user._id
          }, (err,preference)=>{
            if (err) {
              return done(err)
            }
            if (!preference) {
              var settings = new Preference()
              //create settings for the user for this devices
              settings.user = user._id
              //associate user with preference
              settings.device = req.device._id
              //associate preference with device
              req.device.settings.push(settings._id)
              //associate device with preference
              req.device.save((err,device)=>{
                //save device
                if (err)
                  return done(err)
                settings.save((err,preference)=>{
                  //save preference
                  State.findOne({
                    'device': req.device._id,
                    'user': user._id
                  }, (err,state)=>{
                    if (err) {
                      return done(err)
                    }
                    if (!state) {
                      var state = new State()
                      //create state for the user for this devices
                      state.user = user._id
                      //associate user with state
                      state.device = req.device._id
                      //associate state with device
                      req.device.states.push(state._id)
                      //associate device with state
                      req.device.save((err,device)=>{
                        //save device
                        if (err)
                          return done(err)
                        state.save((err,preference)=>{
                          //save state
                          if (err)
                            return done(err)
                          return done(null , {
                            token: token,
                            user: user,
                            username: user.accounts.local.username,
                            device: req.device
                          })
                          //return token and username
                        }
                        )
                      }
                      )
                    } else
                      return done(null , {
                        token: token,
                        user: user,
                        username: user.accounts.local.username,
                        device: req.device
                      })
                    //return token and username
                  }
                  )
                }
                )
              }
              )
            } else {
              State.findOne({
                'device': req.device._id,
                'user': user._id
              }, (err,state)=>{
                if (err) {
                  return done(err)
                }
                if (!state) {
                  var state = new State()
                  //create state for the user for this devices
                  state.user = user._id
                  //associate user with state
                  state.device = req.device._id
                  //associate state with device
                  req.device.states.push(state._id)
                  //associate device with state
                  req.device.save((err,device)=>{
                    //save device
                    if (err)
                      return done(err)
                    state.save((err,preference)=>{
                      //save state
                      if (err)
                        return done(err)
                      return done(null , {
                        token: token,
                        user: user,
                        username: user.accounts.local.username,
                        device: req.device
                      })
                      //return token and username
                    }
                    )
                  }
                  )
                } else
                  return done(null , {
                    token: token,
                    user: user,
                    username: user.accounts.local.username,
                    device: req.device
                  })
                //return token and username
              }
              )
            }
          }
          )
        })
      }
      )
    } else
      return done(null , {
        token: token,
        user: user,
        username: user.accounts.local.username,
        device: req.device
      })
    //return token and username
  }
  // =========================================================================
  // passport session setup ==================================================
  // =========================================================================
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users out of session
  // used to serialize the user for the session
  passport.serializeUser(function(user, done) {
    done(null , user.id)
  })
  // used to deserialize the user
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user)
    })
  })
  // =========================================================================
  // LOCAL UNLOCK ============================================================
  // =========================================================================
  passport.use('local-unlock', new LocalStrategy({
    usernameField: 'email',
    // by default, local strategy uses username and password, we will override with email
    passwordField: 'key',
    passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },function(req, email, key, done) {
    process.nextTick(function() {
      // asynchronous. User.findOne() doesnt fire again unless data is returned
      if (req.user.settings.apps.general.lockscreen.key == 'password') {
        if (!req.user.validPassword(key)) {
          return done(null , false, {
            message: {
              class: 'error',
              message: 'Oops! Wrong password.'
            },
            password: {
              glyphicon: 'glyphicon-remove',
              class: 'has-error'
            }
          })
        } else {
          return done(null , true)
        }
      } else {
        if (req.user.settings.general.universalpin) {
          Preference.findOne({
            'main': true,
            'user': req.user._id
          }, (err,preference)=>{
            if (err)
              return done(err)
            if (preference.general.lockscreen.pin === parseInt(key)) {
              return done(null , true)
            } else {
              return done(null , false, {
                message: {
                  class: 'error',
                  message: 'Oops! Wrong PIN.'
                },
                PIN: {
                  glyphicon: 'glyphicon-remove',
                  class: 'has-error',
                  value: parseInt(key)
                }
              })
            }
          }
          )
        } else {
          Preference.findOne({
            'device': req.device._id,
            'user': req.user._id
          }, (err,preference)=>{
            if (err)
              return done(err)
            if (preference.general.lockscreen.pin === parseInt(key)) {
              return done(null , true)
            } else {
              return done(null , false, {
                message: {
                  class: 'error',
                  message: 'Oops! Wrong PIN.'
                },
                PIN: {
                  glyphicon: 'glyphicon-remove',
                  class: 'has-error',
                  value: parseInt(key)
                }
              })
            }
          }
          )
        }
      }
    })
  }
  ))
  // =========================================================================
  // LOCAL SIGNIN ============================================================
  // =========================================================================
  passport.use('local-signin', new LocalStrategy({
    usernameField: 'email',
    // by default, local strategy uses username and password, we will override with email
    passwordField: 'password',
    passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },function(req, email, password, done) {
    if (email) {
      email = email.toLowerCase()
    }
    // Use lower-case e-mails to avoid case-sensitive e-mail matching
    process.nextTick(function() {
      // asynchronous. User.findOne() doesnt fire again unless data is returned
      User.findOne({
        'accounts.local.email': email
      }, function(err, user) {
        if (err)
          return done(err)
          // if there are any errors, return the error
        else if (!user) {
          User.findOne({
            'accounts.local.username': email
          }, function(err, user) {
            if (err)
              return done(err)
              // if there are any errors, return the error
            else if (!user) {
              User.findOne({
                'details.phone': email
              }, function(err, user) {
                if (err)
                  return done(err)
                  // if there are any errors, return the error
                else if (!user) {
                  // if no user is found, return the message
                  return done(null , false, {
                    message: {
                      class: 'error',
                      message: 'Oops! Incorrect email, username or phone number.'
                    },
                    emailusernamephone: {
                      glyphicon: 'glyphicon-remove',
                      class: 'has-error'
                    }
                  })
                } else if (!user.validPassword(password)) {
                  // if user is found but passwords doesnt match, return the message
                  return done(null , false, {
                    message: {
                      class: 'error',
                      message: 'Oops! Wrong password.'
                    },
                    password: {
                      glyphicon: 'glyphicon-remove',
                      class: 'has-error'
                    }
                  })
                } else if (!user.isActivated()) {
                  // if user is found but is not yet activated, return the message
                  return done(null , false, {
                    message: {
                      class: 'warning',
                      message: "You haven't activated your account yet. Please activate to proceed."
                    },
                    inactive: true,
                    token: user.getTempToken()
                  })
                } else if (!user.canSignin().canSignin) {
                  // if user is found but is logged in in more devices than allowed, return the message
                  return done(null , false, {
                    message: {
                      class: 'warning',
                      message: 'Sorry! You are already signed in on ' + user.canSignin().signedInOnLength + ' devices which is your current limit. Please sign out off any of these devices. \n Upgrading your account will relinquish this limitation.'
                    },
                    devices: true,
                    token: user.getTempToken()
                  })
                } else {
                  allIsWell(user, req, done)
                }
              })
            } else if (!user.validPassword(password)) {
              // if user is found but password doesn't match, return the message
              return done(null , false, {
                message: {
                  class: 'error',
                  message: 'Oops! Wrong password.'
                },
                password: {
                  glyphicon: 'glyphicon-remove',
                  class: 'has-error'
                }
              })
            } else if (!user.isActivated()) {
              // if user is found but is not yet activated, return the message
              return done(null , false, {
                message: {
                  class: 'warning',
                  message: "You haven't activated your account yet. Please activate to proceed."
                },
                inactive: true,
                token: user.getTempToken()
              })
            } else if (!user.canSignin().canSignin) {
              // if user is found but is logged in in more devices than allowed, return the message
              return done(null , false, {
                message: {
                  class: 'warning',
                  message: 'Sorry! You are already signed in on ' + user.canSignin().signedInOnLength + ' devices which is your current limit. Please sign out off any of these devices. \n Upgrading your account will relinquish this limitation.'
                },
                devices: true,
                token: user.getTempToken()
              })
            } else {
              allIsWell(user, req, done)
            }
          })
        } else if (!user.validPassword(password)) {
          // if user is found but passwords doesnt match, return the message
          return done(null , false, {
            message: {
              class: 'error',
              message: 'Oops! Wrong password.'
            },
            password: {
              glyphicon: 'glyphicon-remove',
              class: 'has-error'
            }
          })
        } else if (!user.isActivated()) {
          // if user is found but is not yet activated, return the message
          return done(null , false, {
            message: {
              class: 'warning',
              message: "You haven't activated your account yet. Please activate to proceed."
            },
            inactive: true,
            token: user.getTempToken()
          })
        } else if (!user.canSignin().canSignin) {
          // if user is found but is logged in in more devices than allowed, return the message
          return done(null , false, {
            message: {
              class: 'warning',
              message: 'Sorry! You are already signed in on ' + user.canSignin().signedInOnLength + ' devices which is your current limit. Please sign out off any of these devices. \n Upgrading your account will relinquish this limitation.'
            },
            devices: true,
            token: user.getTempToken()
          })
        } else {
          allIsWell(user, req, done)
        }
      })
    })
  }
  ))
  // =========================================================================
  // LOCAL SIGNUP ============================================================
  // =========================================================================
  passport.use('local-signup', new LocalStrategy({
    usernameField: 'email',
    // by default, local strategy uses username and password, we will override with email
    passwordField: 'password',
    passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },(req,email,password,done)=>{
    if (email)
      email = email.toLowerCase()
    process.nextTick(function() {
      if (!req.user) {
        User.findOne({
          'accounts.local.email': email
        }).exec((err,user)=>{
          if (err)
            return done(err)
          else if (user) {
            return done({
              message: {
                class: 'error',
                message: 'That email is already taken.'
              },
              //return message
              signupemail: {
                glyphicon: 'glyphicon-remove',
                class: 'has-error'
              }
            })
          } else if (!user) {
            User.findOne({
              'accounts.local.username': req.body.username
            }).exec((err,user)=>{
              if (err)
                return done(err)
                // if there are any errors, return the error
              else if (user) {
                // check to see if theres already a user with that username
                return done(//if a user with that username exists,
                {
                  message: {
                    class: 'error',
                    message: 'That username is already taken.'
                  },
                  //return message
                  signupusername: {
                    glyphicon: 'glyphicon-remove',
                    class: 'has-error'
                  }
                })
                //and error
              } else if (!user) {
                User.findOne({
                  'details.phone': req.body.phone
                }).exec((err,user)=>{
                  if (err)
                    return done(err)
                    // if there are any errors, return the error
                  else if (user) {
                    // check to see if there's already a user with that phone
                    return done(//if a user with that phone number exists,
                    {
                      message: {
                        class: 'error',
                        message: 'That phone number is already in use.'
                      },
                      //return message
                      signupphone: {
                        glyphicon: 'glyphicon-remove',
                        class: 'has-error'
                      }
                    })
                    //and error
                  } else if (!user) {
                    //if user with that email,username and phone doesnt exist
                    var newUser = new User()
                    // create the user
                    var settings = new Preference()
                    //create settings for the user
                    var state = new State()
                    //create state for the user
                    settings.user = newUser._id
                    //associate user with preference
                    settings.main = true
                    // make this the default settings for the user
                    state.user = newUser._id
                    //associate user with state
                    state.main = true
                    // make this the default state for the user
                    newUser.accounts.local.email = email
                    newUser.accounts.local.password = newUser.generateHash(password)
                    newUser.accounts.local.username = req.body.username
                    newUser.details.title = req.body.title
                    newUser.details.firstname = req.body.firstname
                    newUser.details.middlename = req.body.middlename
                    newUser.details.lastname = req.body.lastname
                    newUser.details.gender = req.body.gender
                    newUser.details.phone = req.body.phone
                    newUser.details.address = req.body.address
                    newUser.details.about = req.body.about
                    newUser.settings.apps = settings._id
                    newUser.state = state._id
                    newUser.generateUserSpace(newUser.accounts.local.username, (err,space)=>{
                      if (err)
                        return done(err)
                      let means = ""
                      newUser.generateActivationCode()
                      agent.checkOnlineStatus().then((status)=>{
                        // if agent is online
                        if (status)
                          means = 'email'
                        else
                          means = 'none'
                        newUser.sendActivationCode(means, newUser, (err,user)=>{
                          newUser.save(function(err, newUser) {
                            //save user
                            if (err)
                              return done(err)
                            settings.save((err,preference)=>{
                              //save user's apps' settings
                              if (err) {
                                return done(err)
                              }
                              state.save((err,state)=>{
                                //save user's apps' settings
                                if (err) {
                                  return done(err)
                                }
                                return done(null , true)
                              }
                              )
                            }
                            )
                          })
                        }
                        )
                      }
                      )
                    }
                    )
                  }
                }
                )
              }
            }
            )
          }
        }
        )
      } else {
        // user is logged in and already has a local account. Ignore signup. (You should log out before trying to create a new account, user!)
        return done(null , req.user)
      }
    })
  }
  ))
  // =========================================================================
  // FACEBOOK ================================================================
  // =========================================================================
  passport.use(new FacebookStrategy({
    clientID: keys.facebookAuth.clientID,
    clientSecret: keys.facebookAuth.clientSecret,
    callbackURL: keys.facebookAuth.callbackURL,
    profileFields: ['id', 'name', 'email'],
    passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },function(req, token, refreshToken, profile, done) {
    // asynchronous
    process.nextTick(function() {
      if (!req.user) {
        // check if the user is already logged in
        User.findOne({
          'accounts.facebook.id': profile.id
        }, function(err, user) {
          if (err)
            return done(err)
          if (user) {
            if (!user.accounts.facebook.token) {
              // if there is a user id already but no token (user was linked at one point and then removed)
              user.accounts.facebook.token = token
              user.accounts.facebook.name = profile.name.givenName + ' ' + profile.name.familyName
              user.accounts.facebook.email = (profile.emails[0].value || '').toLowerCase()
              user.save(function(err) {
                if (err)
                  return done(err)
                return done(null , user)
              })
            }
            return done(null , user)
            // user found, return that user
          } else {
            // if there is no user, create them
            var newUser = new User()
            newUser.accounts.facebook.id = profile.id
            newUser.accounts.facebook.token = token
            newUser.accounts.facebook.name = profile.name.givenName + ' ' + profile.name.familyName
            newUser.accounts.facebook.email = (profile.emails[0].value || '').toLowerCase()
            newUser.save(function(err) {
              if (err)
                return done(err)
              return done(null , newUser)
            })
          }
        })
      } else {
        // user already exists and is logged in, we have to link accounts
        var user = req.user
        // pull the user out of the session
        user.accounts.facebook.id = profile.id
        user.accounts.facebook.token = token
        user.accounts.facebook.name = profile.name.givenName + ' ' + profile.name.familyName
        user.accounts.facebook.email = (profile.emails[0].value || '').toLowerCase()
        user.save(function(err) {
          if (err)
            return done(err)
          return done(null , user)
        })
      }
    })
  }
  ))
  // =========================================================================
  // TWITTER =================================================================
  // =========================================================================
  passport.use(new TwitterStrategy({
    consumerKey: keys.twitterAuth.consumerKey,
    consumerSecret: keys.twitterAuth.consumerSecret,
    callbackURL: keys.twitterAuth.callbackURL,
    passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },function(req, token, tokenSecret, profile, done) {
    // asynchronous
    process.nextTick(function() {
      // check if the user is already logged in
      if (!req.user) {
        User.findOne({
          'accounts.twitter.id': profile.id
        }, function(err, user) {
          if (err)
            return done(err)
          if (user) {
            // if there is a user id already but no token (user was linked at one point and then removed)
            if (!user.accounts.twitter.token) {
              user.accounts.twitter.token = token
              user.accounts.twitter.username = profile.username
              user.accounts.twitter.displayName = profile.displayName
              user.save(function(err) {
                if (err)
                  return done(err)
                return done(null , user)
              })
            }
            return done(null , user)
            // user found, return that user
          } else {
            // if there is no user, create them
            var newUser = new User()
            newUser.accounts.twitter.id = profile.id
            newUser.accounts.twitter.token = token
            newUser.accounts.twitter.username = profile.username
            newUser.accounts.twitter.displayName = profile.displayName
            newUser.save(function(err) {
              if (err)
                return done(err)
              return done(null , newUser)
            })
          }
        })
      } else {
        // user already exists and is logged in, we have to link accounts
        var user = req.user
        // pull the user out of the session
        user.accounts.twitter.id = profile.id
        user.accounts.twitter.token = token
        user.accounts.twitter.username = profile.username
        user.accounts.twitter.displayName = profile.displayName
        user.save(function(err) {
          if (err)
            return done(err)
          return done(null , user)
        })
      }
    })
  }
  ))
  // =========================================================================
  // GOOGLE ==================================================================
  // =========================================================================
  passport.use(new GoogleStrategy({
    clientID: keys.googleAuth.clientID,
    clientSecret: keys.googleAuth.clientSecret,
    callbackURL: keys.googleAuth.callbackURL,
    passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },function(req, token, refreshToken, profile, done) {
    // asynchronous
    process.nextTick(function() {
      if (!req.user) {
        // check if the user is already logged in
        User.findOne({
          'accounts.google.id': profile.id
        }, function(err, user) {
          if (err)
            return done(err)
          if (user) {
            // if there is a user id already but no token (user was linked at one point and then removed)
            if (!user.accounts.google.token) {
              user.accounts.google.token = token
              user.accounts.google.name = profile.displayName
              user.accounts.google.email = (profile.emails[0].value || '').toLowerCase()
              // pull the first email
              user.save(function(err) {
                if (err)
                  return done(err)
                return done(null , user)
              })
            }
            return done(null , user)
          } else {
            var newUser = new User()
            newUser.accounts.google.id = profile.id
            newUser.accounts.google.token = token
            newUser.accounts.google.name = profile.displayName
            newUser.accounts.google.email = (profile.emails[0].value || '').toLowerCase()
            // pull the first email
            newUser.save(function(err) {
              if (err)
                return done(err)
              return done(null , newUser)
            })
          }
        })
      } else {
        // user already exists and is logged in, we have to link accounts
        var user = req.user
        // pull the user out of the session
        user.accounts.google.id = profile.id
        user.accounts.google.token = token
        user.accounts.google.name = profile.displayName
        user.accounts.google.email = (profile.emails[0].value || '').toLowerCase()
        // pull the first email
        user.save(function(err) {
          if (err)
            return done(err)
          return done(null , user)
        })
      }
    })
  }
  ))
  return passport
}
