'use strict'
// Load required packages
var mongoose = require('mongoose');
//models
var User = require('./user');
// Define a chat schema
var ChatSchema = new mongoose.Schema({
  class: {
    type: String,
    required: true,
    default: "OneOnOne"
  },
  messages: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Message",
  }],
  counterpart: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Chat",
    required: true
  },
  participants: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }],
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  },
  with: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  }
})
// methods ======================
ChatSchema.methods.addParticipant = function(userid, cb) {
  this.partcipants.push(userid);
  cb(null , this);
}
ChatSchema.methods.deleteMessage = function(index, cb) {
  this.messages.splice(index, 1)
  cb(null , this);
}
// create the model for devices and expose it to our app
module.exports = mongoose.model('Chat', ChatSchema)
