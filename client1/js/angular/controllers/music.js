function musicController($scope, $mdSidenav, $mdBottomSheet, $client) {
  $scope.settings = $client.user.details.settings.apps.music
  $scope.view = $client.user.details.settings.apps.music.display.starton
  $scope.getMusic = $client.music.getMusic
  $scope.sidenav = false
  $scope.toggleSideNav = function() {
    $mdSidenav('nav').toggle().then(function() {
      $scope.sidenav = !$scope.sidenav
    })
  }
  $scope.showTasks = function(event) {
    $mdBottomSheet.show({
      scope: $scope,
      event: event,
      templateUrl: '/templates/music/tasks.html'
    })
  }
  $scope.playing = $client.music.playing
  $scope.repeat = $client.music.repeat
  $scope.shuffle = $client.music.shuffle
  $scope.play = $client.music.play
  $scope.pause = $client.music.pause
  $scope.next = $client.music.next
  $scope.previous = $client.music.previous
  $scope.changeStamp = $client.music.changeStamp
  $scope.openBottomSheet = function() {
    $mdBottomSheet.show({
      templateUrl: '/templates/music/options.html'
    })
  }
  $scope.searchbar = $scope.settings.display.showsearchbar
  $scope.playpos = 20
  $scope.volume = $client.music.volume
  $scope.timestamp = $client.music.timestamp
  $scope.filter = {
    scheme: "all",
    searchText: ''
  }
  $scope.orderBy = ''
  function watch() {
    $scope.$watch('filter.searchText', function(text) {
      var expression;
      switch ($scope.filter.scheme) {
      case 'all':
        expression = text;
        break;
      case 'album':
        expression = {
          album: text
        };
        break;
      case 'title':
        expression = {
          title: text
        };
        break;
      case 'artist':
        expression = {
          artist: text
        };
        break;
      case 'genre':
        expression = {
          genre: text
        };
        break;
      case 'year':
        expression = {
          year: text
        };
        break;
      case 'studio':
        expression = {
          studio: text
        };
        break;
      }
      ($state.is('music.library') && $scope.view == 'albums') ? (obj = {
        array: $scope.allalbums,
        expression: expression
      },
      $scope.albums = $client.search.filter(obj)) : ($state.is('music.mylibrary') && $scope.view == 'albums') ? (obj = {
        array: $scope.allmyalbums,
        expression: expression
      },
      $scope.myalbums = $client.search.filter(obj)) : ($state.is('music.library') && $scope.view == 'songs') ? (obj = {
        array: $scope.allsongs,
        expression: expression
      },
      $scope.songs = $client.search.filter(obj)) : ($state.is('music.mylibrary') && $scope.view == 'songs') ? (obj = {
        array: $scope.allmysongs,
        expression: expression
      },
      $scope.mysongs = $client.search.filter(obj)) : null ;
    });
    $scope.$watch('orderBy', function(text) {
      ($scope.filter == 'all') ? expression = ["title", "year", "released"] : expression = [$scope.filter, "year", "released"];
      (text == 'asc') ? reverse = false : reverse = true;
      ($state.is("music.library") && $scope.view == "albums") ? (obj = {
        array: $scope.allalbums,
        expression: expression,
        reverse: reverse
      },
      $scope.albums = $client.search.orderBy(obj)) : ($state.is('music.mylibrary') && $scope.view == "albums") ? (obj = {
        array: $scope.allmyalbums,
        expression: expression,
        reverse: reverse
      },
      $scope.myalbums = $client.search.orderBy(obj)) : ($state.is('music.library') && $scope.view == "songs") ? (obj = {
        array: $scope.allsongs,
        expression: expression,
        reverse: reverse
      },
      $scope.songs = $client.search.orderBy(obj)) : ($state.is('music.mylibrary') && $scope.view == "songs") ? (obj = {
        array: $scope.allmysongs,
        expression: expression,
        reverse: reverse
      },
      $scope.mysongs = $client.search.orderBy(obj)) : null ;
    });
  }
  $scope.switchTo = function(index) {
    $scope.selectedIndex = index
  }
  if ($scope.user.settings.apps.general.rememberstates) {
    $scope.selectedIndex = $scope.user.state.states.music.laststate;
  } else {
    switch ($scope.settings.display.startin) {
    case 'home':
      $scope.selectedIndex = 0;
      break;
    case 'library':
      $scope.selectedIndex = 1;
      break;
    case 'collection':
      $scope.selectedIndex = 2;
      break;
    case 'previous':
      $scope.selectedIndex = $scope.user.state.states.music.laststate;
      break;
    }
  }
  $scope.$watch('selectedIndex', function(index) {
    switch (index) {
    case 0:
      $scope.goTo("music.home")
      $client.user.details.state.states.music.laststate = 0
      break;
    case 1:
      $scope.goTo("music.nowplaying")
      $client.user.details.state.states.music.laststate = 1
      break;
    case 2:
      $scope.goTo("music.library")
      $client.user.details.state.states.music.laststate = 2
      break;
    case 3:
      $scope.goTo("music.collection")
      $client.user.details.state.states.video.laststate = 3
      break;
    }
  })
  $scope.inform = function(item, event, index) {
    $scope.item = item;
    $scope.event = event;
    $scope.index = index;
    $scope.popup({
      scope: $scope,
      templateUrl: '/templates/music/info.html'
    });
  }
}
musicController.$inject = ['$scope', '$mdSidenav', '$mdBottomSheet', '$client']
app.controller('musicController', musicController)
