'use strict'
// Load required packages
const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const shortId = require('shortid')
const jwt = require('jsonwebtoken')
const fs = require('fs-extra')
const path = require("path")
const request = require('request')
const nodemailer = require('nodemailer')
const keys = require('../config/keys')
// load models
const Device = require('./device')
const Episode = require('./episode')
const Game = require('./game')
const Movie = require('./movie')
const Notification = require('./notification')
const Preference = require('./preference')
const Season = require('./season')
const Show = require('./show')
const State = require('./state')
const Transaction = require('./transaction')
// Define our user schema
const UserSchema = new mongoose.Schema({
  accounts: {
    local: {
      email: {
        type: String,
        unique: true,
        required: true
      },
      otheremails: [],
      username: {
        type: String,
        unique: true,
        required: true
      },
      password: {
        type: String,
        unique: false,
        required: true
      },
      secretword: {
        type: String,
        required: false
      },
      activation: {
        activated: {
          type: Boolean,
          default: false,
          required: true
        },
        activationcode: {
          type: String,
          default: shortId.generate,
          required: true
        }
      }
    },
    facebook: {
      id: String,
      token: String,
      email: String,
      name: String
    },
    twitter: {
      id: String,
      token: String,
      displayName: String,
      username: String
    },
    google: {
      id: String,
      token: String,
      email: String,
      name: String
    },
    instagram: {},
    linkedin: {}
  },
  Collection: {
    episodes: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "Episode"
    }],
    games: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "Game"
    }],
    movies: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "Movie"
    }],
    seasons: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "Season"
    }],
    shows: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "Show"
    }]
  },
  details: {
    title: {
      type: String,
      required: true
    },
    firstname: {
      type: String,
      minLength: 1,
      maxLength: 20,
      unique: false,
      required: true
    },
    middlename: {
      type: String,
      minLength: 0,
      maxLength: 20,
      unique: false,
      required: true
    },
    lastname: {
      type: String,
      minLength: 1,
      maxLength: 20,
      unique: false,
      required: true
    },
    gender: {
      type: String,
      required: true
    },
    profpic: {
      src: String,
      required: false
    },
    about: {
      type: String,
      required: true
    },
    accounttype: {
      type: String,
      required: true,
      default: "user"
    },
    phone: {
      type: String,
      required: true,
      carrier: {
        type: String,
        prefix: Number,
        required: false
      }
    },
    address: {
      type: String,
      required: true
    },
    tutored: {
      type: Boolean,
      default: false
    },
  },
  devices: {
    signedInOn: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Device"
    }],
    signedOutOf: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Device"
    }],
    maxDevices: {
      type: Number,
      default: 3
    }
  },
  extras: {
    ip: {}
  },
  finance: {
    subscription: {
      type: String,
      default: "free",
      required: true
    },
    transactions: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Transaction"
    }],
    credits: {
      type: Number,
      default: 1000,
      required: true
    }
  },
  interactions: {
    chats: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Chat',
      unique: true
    }],
    call: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Call',
      unique: true
    }]
  },
  notifications: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Notification',
    unique: true
  }],
  settings: {
    apps: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Preference',
      required: true
    },
    general: {
      settingsperdevice: {
        type: Boolean,
        required: true,
        default: true
      },
      statesperdevice: {
        type: Boolean,
        required: true,
        default: true
      },
      universalpin: {
        type: Boolean,
        required: true,
        default: false
      }
    },
    social: {
      allowFriendRequests: {
        type: Boolean,
        required: true,
        default: true
      },
      allowFollows: {
        type: Boolean,
        required: true,
        default: true
      },
      allowProfileViewBy: {
        type: String,
        required: true,
        default: "all"
      },
      allowMessagesFrom: {
        type: String,
        required: true,
        default: "all"
      },
      notifycheckout: {
        type: Boolean,
        required: true,
        default: false
      },
      sharewith: {
        type: String,
        required: true,
        default: 'friends'
      },
      shareemail: {
        type: Boolean,
        required: true,
        default: true
      },
      shareimage: {
        type: Boolean,
        required: true,
        default: true
      },
      sharephone: {
        type: Boolean,
        required: true,
        default: false
      },
      sharename: {
        type: Boolean,
        required: true,
        default: false
      },
      shareaddress: {
        type: Boolean,
        required: true,
        default: false
      },
      shareabout: {
        type: Boolean,
        required: true,
        default: false
      }
    }
  },
  social: {
    friends: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "User"
    }],
    requests: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "User"
    }],
    requested: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "User"
    }],
    followers: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "User"
    }],
    following: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "User"
    }],
    blocked: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "User"
    }],
    blockers: [{
      type: mongoose.Schema.Types.ObjectId,
      unique: true,
      ref: "User"
    }]
  },
  state: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'State',
    required: true
  }
})
// methods ======================
//authentication================================================================================
// generating a hash
UserSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null )
}
// checking if password is valid
UserSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.accounts.local.password)
}
UserSchema.methods.getId = function() {
  return this._id
}
const cert = fs.readFileSync(path.resolve(__dirname, "../certs/cert.pem"))
UserSchema.methods.generateToken = function() {
  let today = new Date()
  let exp = new Date(today)
  exp.setDate(today.getDate() + 30)
  // set expiration to 30 days
  return jwt.sign({
    _id: this._id,
    exp: parseInt(exp.getTime() / 1000),
  }, keys.user)
}
UserSchema.methods.getTempToken = function() {
  let today = new Date()
  let exp = new Date(today)
  exp.setDate(today.getDate())
  // set expiration to today
  return jwt.sign({
    _id: this._id,
    exp: parseInt(((exp.getTime() / 1000 / 60) + 5) * 60),
    // set expiration to today plus one minutes
  }, keys.user)
}
UserSchema.methods.isActivated = function() {
  return this.accounts.local.activation.activated
}
UserSchema.methods.activate = function(code, cb) {
  if (this.accounts.local.activation.activationcode == code) {
    this.accounts.local.activation.activated = true
    cb(null , this)
  } else
    cb(true)
}
UserSchema.methods.generateActivationCode = function() {
  this.accounts.local.activation.activationcode = shortId.generate(this.accounts.local.username)
}
UserSchema.methods.sendActivationCode = function(method, user, cb) {
  let self = this
  typeof(user) == 'function' ? (cb = user, user = 'undefined') :  null
  let sendByPhone = function() {
    console.log(self.accounts.local.activation.activationcode)
    let countryCode = '+1'
      , mobileNumber = '4155550000'
      , message = `Welcome to Floatr. Your activation code is ${self.accounts.activation.activationcode}`
    request.post({
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'Accepts': 'application/json'
      },
      url: (process.env.BLOWERIO_URL || 'http://localhost:8443') + '/messages',
      form: {
        to: countryCode + mobileNumber,
        message: message
      }
    }, function(error, response, body) {
      console.log(error, response, body)
      if (!error && response.statusCode == 201) {
        console.log('Message sent!')
      } else {
        let apiResult = JSON.parse(body)
        console.log('Error was: ' + apiResult.message)
      }
    })
    console.log(self.accounts.local.activation.activationcode)
    return cb(null , self)
  }
  let sendByEmail = function() {
    console.log(self.accounts.local.activation.activationcode)
    let transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'davejoeem@gmail.com',
        // Your email id
        pass: '!Daunty@26'// Your password
      }
    });
    let mailOptions = {
      from: '"Floatr" <no-reply@floatr.herokuapp.com>',
      // sender address
      to: self.accounts.local.email,
      // list of receivers format 'bar@blurdybloop.com, baz@blurdybloop.com'
      subject: 'Floatr Activation Code',
      // Subject line
      text: 'Your activation code is: ${self.accounts.local.activation.activationcode}',
      // plaintext body
      html: `
        <h3>Welcome to the Floatr network</h3>
        <br />
        <p>This is your activation code</p>
        <br />
        <pre>${self.accounts.local.activation.activationcode}</pre>
        <br />
        <p>Copy it to the activation page or 
          <a href="https://floatr.herokuapp.com/activate?code=${self.accounts.local.activation.activationcode}&username=${self.accounts.local.username}">click here</a>
          to activate.
        </p>
        <p>You can also copy and paste this link onto your browser's address bar</p>
        <a href="https://floatr.herokuapp.com/activate?code=${self.accounts.local.activation.activationcode}&username=${self.accounts.local.username}">
          https://floatr.herokuapp.com/activate?code=${self.accounts.local.activation.activationcode}&username=${self.accounts.local.username}
        </a>
        <p>If you recieved this email by mistake, please ignore and delete it</p>
        <br/>
        <p>Floatr - Farther your reach.</p>
      ` // html body
    }
    transporter.verify(function(success) {
      console.log('Server is ready to take our messages');
      transporter.sendMail(mailOptions, function(err, info) {
        if (err) {
          console.log(error);
          cb(err)
        } else {
          console.log('Message sent: ' + info.response);
          cb(null , self)
        }
      })
    }, function(err) {
      console.error('Server not ready. Reason: ', err);
      cb(err)
    })
  }
  let dontSend = function() {
    console.log(self.accounts.local.activation.activationcode)
    cb(null , self)
  }
  switch (method) {
  case 'phone':
    sendByPhone()
    break
  case 'email':
    sendByEmail()
    break
  case 'both':
    sendByPhone()
    sendByEmail()
    break
  case 'none':
    dontSend()
    break
  default:
    sendByEmail()
  }
}
UserSchema.methods.generateUserSpace = function(username, cb) {
  fs.ensureFile("./fs/users/" + username + "/logs/log.txt", (err)=>{
    if (err)
      return cb(err)
    cb(null , true)
  }
  )
}
UserSchema.methods.deleteUser = function(username, cb) {
  fs.stat('./fs/users/' + username, (err,stats)=>{
    if (err)
      return cb(err)
    if (stats) {
      fs.unlink("./fs/users/" + username, (err)=>{
        if (err)
          return cb(err)
        cb(null , true)
      }
      )
    }
    arr = this.social.friends.concat(this.social.followers).concat(this.social.following).concat(this.social.requested).concat(this.social.requests).concat(this.social.blocked)
    arr.forEach((member)=>{
      User.findById(member, (err,user)=>{
        if (err)
          return cb(err)
        if (user.social.friends.indexOf(this._id))
          user.social.friends.pull(this._id)
        if (user.social.followers.indexOf(this._id))
          user.social.followers.pull(this._id)
        if (user.social.following.indexOf(this._id))
          user.social.following.pull(this._id)
        if (user.social.requested.indexOf(this._id))
          user.social.requested.pull(this._id)
        if (user.social.requests.indexOf(this._id))
          user.social.requests.pull(this._id)
        if (user.social.blockers.indexOf(this._id))
          user.social.blockers.pull(this._id)
        user.save()
      }
      )
    }
    )
  }
  )
}
//runtime====================================================================================
UserSchema.methods.canSignin = function() {
  return {
    canSignin: this.details.accounttype == 'admin' || this.finance.subscription != 'free' ? true : this.devices.signedInOn.length < this.devices.maxDevices,
    signedInOn: this.devices.signedInOn,
    signedInOnLength: this.devices.signedInOn.length,
    maxDevices: this.devices.maxDevices,
  }
}
UserSchema.methods.signInOnDevice = function(deviceid, cb) {
  this.devices.signedOutOf.pull(deviceid)
  if (this.devices.signedInOn.indexOf(deviceid) == -1) {
    this.devices.signedInOn.push(deviceid)
  }
  Device.findById(deviceid, (err,device)=>{
    if (err)
      return cb(err)
    if (device) {
      device.signInUser(this._id, (err,device)=>{
        if (err)
          return cb(err)
        device.save((err,device)=>{
          if (err)
            cb(err)
          return cb(null , this)
        }
        )
      }
      )
    }
  }
  )
}
UserSchema.methods.signOutOfDevice = function(deviceid, cb) {
  this.devices.signedInOn.pull(deviceid)
  if (this.devices.signedOutOf.indexOf(deviceid) == -1)
    this.devices.signedOutOf.push(deviceid)
  Device.findById(deviceid, (err,device)=>{
    if (err)
      cb(err)
    if (device) {
      device.signOutUser(this._id, (err,device)=>{
        if (err)
          return cb(err)
        device.save((err,device)=>{
          if (err)
            cb(err)
          return cb(null , this)
        }
        )
      }
      )
    }
  }
  )
}
UserSchema.methods.notify = function(config, cb) {
  if (config.devices == 'all') {
    config.devices = this.devices.signedInOn
  }
  if (config.devices == 'allbutthis') {
    config.devices = this.devices.signedInOn
    config.devices.splice(this.devices.signedInOn.indexOf(config.device), 1)
  }
  var note = new Notification(config)
  note.user = this._id
  note.save((err,note)=>{
    if (err && cb)
      return cb(err)
    this.notifications.push(note._id)
    this.save((err,user)=>{
      if (err)
        return cb(err)
      if (cb)
        cb(null , {
          user: user,
          notification: note
        })
    }
    )
  }
  )
}
UserSchema.methods.renotify = function(notification, cb) {
  notification.read = false
  notification.save((err,note)=>{
    if (err && cb)
      return cb(err)
    if (this.notifications.unread.indexOf(note._id) < 0) {
      this.notifications.unread.push(note._id)
    }
    this.save((err,user)=>{
      if (err)
        return cb(err)
      if (cb)
        cb(null , {
          user: user,
          notification: note
        })
    }
    )
  }
  )
}
// create the model for users and expose it to our
module.exports = mongoose.model("User", UserSchema)
