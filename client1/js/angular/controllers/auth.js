function authController($window, $timeout, $scope, $state, $client) {
  $scope.$on('signoutdevicesTimedOut', function(event, data) {
    $scope.user = {}
    $scope.clearError()
    $scope.error.message = {
      class: 'warning',
      message: "Timed out! Please sign in again."
    }
    $state.go('start')
  })
  $scope.$on('entered activation', function(event, data) {
    if (!$scope.error) {
      console.log('no errors going to signin')
      $state.go('signin')
    }
  })
  $scope.$on('entereddevicesignout', function(event, data) {
    if (!$scope.error) {
      $state.go('signin')
    }
  })
  $scope.titles = [{
    title: 'Mister',
    abbrev: 'Mr.',
    gender: 'Male'
  }, {
    title: 'Miss',
    abbrev: 'Ms.',
    gender: 'Female'
  }, {
    title: 'Misses',
    abbrev: 'Mrs.',
    gender: 'Female'
  }, {
    title: 'Professor',
    abbrev: 'Prof.',
    gender: null
  }, {
    title: 'Doctor',
    abbrev: 'Dr.',
    gender: null
  }, {
    title: 'Honourable',
    abbrev: 'Hon.',
    gender: null
  }, ]
  if ($client.identified) {
    $scope.device = $client.this
  } else {
    $client.identify().then(function(dev) {
      $scope.device = dev
    }, function(dev) {
      $scope.device = dev
    })
  }
  $scope.user = {}
  $scope.signUp = function() {
    $scope.clearError()
    $scope.signing = true
    $client.user.signUp($scope.user).then(function(success) {
      console.log('success',success)
      $state.go('signin')
    }, function(error) {
      console.log('error',error)
      $scope.signing = false
      $scope.error = error
    })
  }
  $scope.signIn = function() {
    $scope.clearError()
    $scope.signing = true
    $scope.user.device = $scope.device
    $client.user.signIn($scope.user).then(function(data) {
      $scope.signing = false
      $client.user.saveToken(data.token)
      $client.user.token = data.token
      $client.authenticate().then(function(success) {
        $state.go('apps')
      }, function(error) {
        console.log(error)
      })
    }, function(error) {
      $scope.error = error
    }, function(info) {
      $scope.signing = false
      $scope.error = info
      if (info.inactive) {
        $state.go('signin.activateaccount')
      }
      if (info.devices) {
        $scope.freeslot = 0
        $client.getActiveDevices(info.token).then(function(data) {
          $scope.devices = data.devices
        }, function(error) {
          $scope.error.message = error.message
        })
        $state.go('signin.signoutdevices')
      }
    })
  }
  $scope.activateAccount = function() {
    $scope.signing = true
    $client.user.activateAccount($scope.user.code.code, $scope.error.token).then(function(data) {
      $scope.signing = false
      $state.go('signin')
      $scope.error.message = data.message
    }, function(error) {
      $client.toast.toast(error)
      console.log(error)
      $scope.signing = false
    })
  }
  $scope.clearError = function() {
    $scope.error = {}
  }
  $scope.errorTypeOf = function(type) {
    if ($scope.error.message)
      return $scope.error.message.class === type
  }
  $scope.signOutDevice = function(index) {
    $scope.error.message = null
    if ($scope.devices.length > 0) {
      $scope.signIndex = index
      $client.signOut($scope.devices[index]._id, $scope.error.token).then(function(data) {
        $scope.devices[index].message = data.message
        $scope.signIndex = null
        $scope.freeSlot(1)
        $scope.clearSignOutDevice(index, 3)
      }, function(error) {
        console.log(error)
        $scope.signing = {
          index: index,
          state: false
        }
        $scope.devices[index].message = error
      })
    }
  }
  $scope.currentState = function(state) {
    if (state) {
      return $state.current.name == state
    } else
      return $state.current.name
  }
  $scope.sendCode = function(method, repeat) {
    $scope.error.message = null
    $client.user.sendCode(method, repeat, $scope.error.token).then(function(data) {
      if (method == 'email') {
        $scope.user.email = data.email
        $client.toast.toast(data.message.message)
      } else if (method == 'phone') {
        $scope.user.phone = data.phone
        $client.toast.toast(data.message.message)
      }
    }, function(error) {
      if (error.message) {
        $scope.error.message = error.message
        $client.toast.toast(error.message.message)
      }
    })
  }
  $scope.activationScheme = function(scheme) {
    $scope.currentScheme = scheme
  }
  $scope.scheme = function(scheme) {
    return $scope.currentScheme == scheme
  }
  $scope.freeSlot = function(slot) {
    if (slot) {
      $scope.freeslot = $scope.freeslot + slot
    } else
      return $scope.freeslot < 1
  }
  $scope.isSignedOut = function(index) {
    if ($scope.devices[index].message)
      return true
    else
      return false
  }
  $scope.isSignedIn = function(index) {
    if (!$scope.devices[index].message)
      return true
    else
      return false
  }
  $scope.clearSignOutDevice = function(index, wait) {
    $timeout(function() {
      $scope.devices.splice(index, 1)
      if ($scope.devices)
        if ($scope.devices.length == 0) {
          $state.go("signin")
          $scope.devices = null
        }
    }, wait * 1000)
  }
}
authController.$inject = ['$window', '$timeout', '$scope', '$state', '$client']
app.controller('authController', authController)
