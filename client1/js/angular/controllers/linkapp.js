function linkAppController($rootScope, $scope, $mdDialog, $client) {
  $scope.messages = []
  $scope.socket = $client.socket
  $scope.addContact = function(event) {
    $scope.action = "add contact"
    $scope.popup({
      scope: $scope,
      event: event,
      templateUrl: '/templates/misc/chooseuser.html'
    })
  }
  $scope.changeStatus = function(status) {
    $scope.onlineStatus = status
    $client.say({
      event: 'online status',
      data: status
    })
  }
  $scope.currentStatus = function(status) {
    return $scope.onlineStatus === status
  }
  $scope.call = function(recipient, direct) {
    if (direct)
      $scope.direct = direct
    $scope.switchTo(0)
    $scope.calling = true
    $scope.chatting = false
    $scope.phonecall = {
      recipient: recipient,
      caller: $client.user.details.username
    }
    $client.say({
      event: 'start call',
      data: $scope.phonecall
    });
  }
  $scope.redial = function(recipient, direct) {
    $scope.calling = true
    $scope.chatting = false
    $scope.phonecall = {
      recipient: recipient,
      caller: $client.user.details.username
    }
    $client.say({
      event: 'redial',
      data: $scope.phonecall
    })
  }
  $scope.holdCall = function(recipient, direct) {
    if (direct)
      $scope.direct = direct
    $scope.switchTo(0)
    $scope.calling = true
    $scope.chatting = false
    $scope.phonecall = {
      recipient: recipient,
      caller: $client.user.details.username
    }
    $client.say({
      event: 'hold call',
      data: $scope.phonecall
    });
  }
  $scope.endCall = function(recipient, direct) {
    if (direct)
      $scope.direct = direct
    $scope.switchTo(0)
    $scope.calling = true
    $scope.chatting = false
    $scope.phonecall = {
      recipient: recipient,
      caller: $client.user.details.username
    }
    $client.say({
      event: 'end call',
      data: $scope.phonecall
    })
  }
  $scope.sendMessage = function(text) {
    if ($scope.text == "")
      return
    $scope.message = {
      text: text,
      sender: $scope.user._id,
      device: $client.this.id,
      chat: $scope.chat._id,
      time: Date(Date.now())
    }
    $client.say({
      event: 'chat message',
      data: $scope.message
    }).then(function(data){
      console.log(data)
      $scope.messages.push(data.msg)    
    },function(error){},function(info){})
    text = "";
  }
  $scope.sendTo = function(recipient, direct) {
    if (direct)
      $scope.direct = direct
    $scope.switchTo(1)
    $scope.chatting = true
    $scope.with = true;
    $scope.recipient = recipient
    $client.say({
      event: 'start chat',
      data: $scope.recipient
    }).then(function(data) {
      $rootScope.$broadcast('user', data.user)
      $scope.chat = data.chat
      $scope.messages = data.chat.messages
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      //$client.toast.toast(info)
    })
  }
  $scope.exec = function(event, username) {
    $client.user.getUserInfo(username).then(function(data) {
      if ($client.user.details.social.friends.indexOf(data) < 0)
        $scope.request(data.username)
      else
        $scope.sendTo(data.username)
      $mdDialog.cancel()
    }, function(error) {
      $mdDialog.cancel()
      $client.toast.toast(error).then(function() {
        $scope.popup({
          scope: $scope,
          event: event,
          templateUrl: '/templates/misc/chooseuser.html'
        })
      }, function(info) {
        $client.toast.toast(info).then(function() {
          $scope.popup({
            scope: $scope,
            event: event,
            templateUrl: '/templates/misc/chooseuser.html'
          })
        })
      })
    });
  }
  $scope.sentMessage = function(message) {
    return message.sender.user.accounts.local.username == $scope.user.accounts.local.username
  }
  $scope.clearSendTo = function() {
    $scope.with = false
    $scope.chatting = false
    $scope.recipient = null
  }
  $scope.switchTo = function(index) {
    $scope.with = false
    if (index > -1 || index < 3) {
      $scope.selectedIndex = index
    }
  }
  if ($scope.user.settings.apps.general.rememberstates) {
    $scope.selectedIndex = $scope.user.state.states.linkapp.laststate
  } else {
    switch ($scope.settings.apps.linkapp.display.startin) {
    case 'calls':
      $scope.selectedIndex = 0;
      break
    case 'conversations':
      $scope.selectedIndex = 1;
      break
    case 'contacts':
      $scope.selectedIndex = 2;
      break
    case 'previous':
      $scope.selectedIndex = $scope.user.state.states.linkapp.laststate;
      break
    }
  }
  $scope.$watch('selectedIndex', function(current) {
    switch (current) {
    case 0:
      $scope.goTo("linkapp.calls")
      if (!$scope.direct)
        $scope.with = false
      $client.user.details.state.states.linkapp.laststate = 0
      break
    case 1:
      $scope.goTo("linkapp.conversations")
      if (!$scope.direct)
        $scope.with = false
      $client.user.details.state.states.linkapp.laststate = 1
      break
    case 2:
      $scope.goTo("linkapp.contacts")
      $scope.with = false
      $client.user.details.state.states.linkapp.laststate = 2
      break
    }
  })
}
linkAppController.$inject = ['$rootScope', '$scope', '$mdDialog', '$client']
app.controller('linkAppController', linkAppController)
