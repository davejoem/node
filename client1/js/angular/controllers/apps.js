var appsController = function($scope, $client) {
  $scope.views = $client.launcher.views;
  $scope.apps = $client.launcher.apps;
  $scope.currentIndex = $client.launcher.currentIndex;
  $scope.selectView = $client.launcher.selectView;
  $scope.setCurrentViewIndex = $client.launcher.setCurrentViewIndex;
  $scope.isCurrentViewIndex = $client.launcher.isCurrentViewIndex;
  $scope.currentIndex = $client.launcher.currentIndex;
  $scope.open = true
  $scope.prevView = function(index) {
    if (!index) {
      var index = $scope.currentIndex;
    }
    $client.launcher.prevView(index);
    $scope.currentIndex = $client.launcher.currentIndex;
  }
  $scope.nextView = function(index) {
    if (!index) {
      var index = $scope.currentIndex
    }
    $client.launcher.nextView(index)
    $scope.currentIndex = $client.launcher.currentIndex
  }
  $scope.count = function(appname) {
    for (app of $client.launcher.views.allapps.apps) {
      if (app.url == appname) {
        if ($client.launcher.views.recentapps.apps.indexOf(app) == -1) {
          $client.launcher.views.recentapps.apps.reverse()
          $client.launcher.views.recentapps.apps.push(app)
          $client.launcher.views.recentapps.apps.reverse()
        } else {
          $client.launcher.views.recentapps.apps.splice($client.launcher.views.recentapps.apps.indexOf(app),1)
          $client.launcher.views.recentapps.apps.reverse()
          $client.launcher.views.recentapps.apps.push(app)
          $client.launcher.views.recentapps.apps.reverse()
        }
        return  
      }
    }
  }
}
appsController.$inject = ['$scope', '$client'];
app.controller('appsController', appsController);
