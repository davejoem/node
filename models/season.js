'use strict'
// Load required packages
var mongoose = require('mongoose');
var Episode = require('./episode');
var log = require('../utils/log')(require('fs'),require('path'),require('../utils/consts'));

// Define a season schema
var SeasonSchema = new mongoose.Schema({
  actors: {type: String},
  averagerating: {type: Number, default: 0, required: true}, 
  comments: {
    comments: [{      
      author: {
        id: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
        username: {type: String, required: true},
      },
      comment: {type: String, required: true}, 
      rating: {type: Number, default: 0, required: true},
      likes: {type: Number, default: 0, required: true},
      dislikes: {type: Number, default: 0, required: true}
    }],
    count: {type: Number, default: 0, required: true}
  },  
  poster: {type: String, required: false},
  downloads: {type: Number, default: 0, required: true}, 
  episodes: [{type:mongoose.Schema.Types.ObjectId, ref: 'Episode', unique: true}],
  eps: [],
  genres: {type: String},
  name:  {type:String, required: true, unique: true},
  number: {type:String, required: true, unique: false},
  owners: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],
  path: {type: String, default: "", required: true, unique: true},
  price: {type: Number, default: 0, required: true}, 
  sharedwiths: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],
  sharers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],
  show: {type:mongoose.Schema.Types.ObjectId, ref: 'Show', required: true},
  type: {type: String, required: true, default:"season"},
  year: {type: String}
});

// methods ======================
// calculate price
SeasonSchema.methods.calcPrice = function(cb) {    
    var price=0; 
    Episode.find({"season": this._id}, (err,episodes)=>{
      if (err) {
        log({log: this.name + ": Error calculating price", err: err});
        return cb();
      }
      for(i=0;i<episodes.length;i++){
        price+=episodes[i].price;
      }
      return cb(price);
    });
};
// adding a review
SeasonSchema.methods.addComment = (comment)=> {
  this.comments.count++;
  this.comments.comments.push(comment);
  return this;
};
// calculate new rating
SeasonSchema.methods.calcAverageRating = ()=> {
  var total=0, averagerating=0;
  for(i=0;i<this.comments.comments.length;i++){
    total = this.comments.comments[i].rating+total;
    averagerating = total/this.comments.comments.length;
  }
  this.averagerating = averagerating;
  return averagerating;
};
// create the model for season and expose it to our app
module.exports = mongoose.model('Season', SeasonSchema);