var startController = function($state, $scope, $interval, $client) {
  $scope.slides = [{
    id: "image00",
    src: "/images/img00.jpg"
  }, {
    id: "image01",
    src: "/images/img01.jpg"
  }, {
    id: "image02",
    src: "/images/img02.jpg"
  }, {
    id: "image03",
    src: "/images/img03.jpg"
  }, {
    id: "image04",
    src: "/images/img04.jpg"
  }];
  $scope.direction = 'left';
  $scope.start = function(event) {
    $client.ripple.attach($scope, angular.element(event.target), {
      center: true
    });
    $state.go('apps');
  }
  $scope.currentIndex = 0;
  $scope.setCurrentSlideIndex = function(index) {
    $scope.direction = (index > $scope.currentIndex) ? 'left' : 'right';
    $scope.currentIndex = index;
  }
  ;
  $scope.isCurrentSlideIndex = function(index) {
    return $scope.currentIndex === index;
  }
  ;
  $scope.nextSlide = function() {
    $scope.direction = 'left';
    $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
  }
  ;
  $scope.prevSlide = function() {
    $scope.direction = 'right';
    $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
  }
  ;
  function loadSlides() {
    $interval(function() {
      $scope.nextSlide()
    }, 5000);
  }
  loadSlides();
}
startController.$inject = ['$state', '$scope', '$interval', '$client']
app.controller('startController', startController)
