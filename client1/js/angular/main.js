var app = angular.module('app', ['ngAnimate', 'ngMdIcons', 'ui.router', 'ngMessages', 'ngMaterial', 'ngFileSaver'])
//app is our main module
app.constant('FREE_ZONES', {
  names: ['start', 'locked', 'signin', 'signup', 'signin.activateaccount', 'signin.signoutdevices', 'signin.recoverpassword', 'aboutus', 'tncs', 'contactus'],
  urls: ['/', '/locked', '/signin', '/signup', '/activateaccount', '/signoutdevices', '/recoverpassword', '/aboutus', '/tncs', '/contactus']
})
app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $location) {
  $stateProvider.state('start', {
    url: '/',
    templateUrl: '/templates/start.html',
    controller: 'startController'
  }).state('locked', {
    url: '/locked',
    templateUrl: '/templates/auth/locked.html',
    controller: 'lockController',
    resolve: {
      cancelTimeout: function($client) {
        $client.cancelTimeout()
      },
      locked: function($client, $state) {
        if (!$client.locked) {
          $state.go('apps')
        }
      }
    }
  }).state('signup', {
    url: '/signup',
    templateUrl: '/templates/auth/signup.html',
    controller: 'authController',
    data: {},
    resolve: {
      signin: function($state, $client) {
        if ($client.user.isLoggedIn()) {
          $state.go('apps')
        }
      }
    }
  }).state('signin', {
    url: '/signin',
    templateUrl: '/templates/auth/signin.html',
    controller: 'authController',
    onEnter: ["$timeout", "$state", function($timeout, $state) {
      $timeout(function() {
        if ($state.next.name == "signin")
          $state.go("start")
      }, 300000)
    }
    ],
    resolve: {
      signin: function($state, $client) {
        if ($client.user.isLoggedIn()) {
          $state.go('apps')
        }
      }
    }
  }).state('signin.activateaccount', {
    url: '/activateaccount',
    templateUrl: '/activateaccount.html',
    onEnter: ['$rootScope', function($rootScope) {
      $rootScope.$broadcast('entered activation')
    }
    ]
  }).state('signin.signoutdevices', {
    url: '/signoutdevices',
    templateUrl: '/signoutdevices.html',
    onEnter: ['$rootScope', '$timeout', '$client', '$state', function($rootScope, $timeout, $client, $state) {
      $rootScope.$broadcast('entereddevicesignout')
      if ($client.user.isLoggedIn())
        $state.go('apps')
      else {
        $timeout(function() {
          $rootScope.$broadcast('signoutdevicesTimedOut')
        }, 60000)
      }
    }
    ]
  }).state('signin.recoverpassword', {
    url: '/recoverpassword',
    templateUrl: '/recoverpassword.html'
  }).state('apps', {
    url: '/apps',
    templateUrl: '/templates/apps/apps.html',
    controller: 'appsController',
    resolve: {
      signin: function($state, $client) {
        if (!$client.user.isLoggedIn()) {
          $state.go('signin')
        }
      }
    }
  }).state('video', {
    url: '/video',
    templateUrl: '/templates/video/video.html',
    controller: 'videoController',
    onEnter: ['$client', function($client) {
      $client.video.getMovies()
      $client.video.getShows()
    }
    ]
  }).state('video.home', {
    url: '/home',
    templateUrl: '/templates/video/home.html'
  }).state('video.library', {
    url: '/library',
    templateUrl: '/templates/video/library.html'
  }).state('video.collection', {
    url: '/collection',
    templateUrl: '/templates/video/collection.html'
  }).state('video.play', {
    url: '/play',
    templateUrl: '/templates/video/nowplaying.html',
    resolve: {
      cancelTimeout: function($client) {
        $client.cancelTimeout()
      }
    }
  }).state('video.add', {
    url: '/add',
    templateUrl: '/templates/video/add.html'
  }).state('music', {
    url: '/music',
    templateUrl: '/templates/music/music.html',
    controller: 'musicController'
  }).state('music.home', {
    url: '/home',
    templateUrl: '/templates/music/home.html'
  }).state('music.library', {
    url: '/library',
    templateUrl: '/templates/music/library.html'
  }).state('music.collection', {
    url: '/collection',
    templateUrl: '/templates/music/collection.html'
  }).state('music.nowplaying', {
    url: '/nowplaying',
    templateUrl: '/templates/music/nowplaying.html'
  }).state('linkapp', {
    url: '/linkapp',
    templateUrl: '/templates/linkapp/linkapp.html',
    controller: 'linkAppController'
  }).state('linkapp.calls', {
    url: '/calls',
    templateUrl: '/templates/linkapp/calls.html'
  }).state('linkapp.contacts', {
    url: '/contacts',
    templateUrl: '/templates/linkapp/contacts.html'
  }).state('linkapp.conversations', {
    url: '/conversations',
    templateUrl: '/templates/linkapp/conversations.html'
  }).state('gamer', {
    url: '/gamer',
    templateUrl: '/templates/gamer/gamer.html',
    controller: 'gamerController'
  }).state('gamer.home', {
    url: '/home',
    templateUrl: '/templates/gamer/home.html'
  }).state('gamer.library', {
    url: '/library',
    templateUrl: '/templates/gamer/library.html'
  }).state('gamer.collection', {
    url: '/collection',
    templateUrl: '/templates/gamer/collection.html'
  }).state('gamer.play', {
    url: '/play',
    templateUrl: '/templates/gamer/play.html'
  }).state('settings', {
    url: '/settings',
    templateUrl: '/templates/settings/settings.html',
    controller: 'settingsController',
    resolve: {
      user: function($client) {
        if ($client.user.details)
          return true
        else
          return false
      }
    }
  }).state('settings.video', {
    url: '/video',
    templateUrl: '/templates/video/settings.html'
  }).state('settings.music', {
    url: '/music',
    templateUrl: '/templates/music/settings.html'
  }).state('settings.linkapp', {
    url: '/linkapp',
    templateUrl: '/templates/linkapp/settings.html'
  }).state('settings.gamer', {
    url: '/gamer',
    templateUrl: '/templates/gamer/settings.html'
  }).state('settings.notifications', {
    url: '/notifications',
    templateUrl: '/templates/notifications/settings.html'
  }).state('settings.ideas', {
    url: '/ideas',
    templateUrl: '/templates/ideas/settings.html'
  }).state('settings.stats', {
    url: '/stats',
    templateUrl: '/templates/stats/settings.html'
  }).state('settings.logs', {
    url: '/logs',
    templateUrl: '/templates/logs/settings.html'
  }).state('settings.profile', {
    url: '/profile',
    templateUrl: '/templates/profile/settings.html'
  }).state('profile', {
    url: '/profile',
    templateUrl: '/templates/profile/profile.html',
    controller: 'profileController'
  }).state('profile.details', {
    url: '/details',
    templateUrl: '/templates/profile/details.html',
    controller: 'profileController'
  }).state('profile.social', {
    url: '/social',
    templateUrl: '/templates/profile/social.html'
  }).state('profile.devices', {
    url: '/devices',
    templateUrl: '/templates/profile/devices.html'
  }).state('profile.finance', {
    url: '/finance',
    templateUrl: '/templates/profile/finance.html'
  }).state('notifications', {
    url: '/notifications',
    templateUrl: '/templates/notifications/notifications.html',
    controller: 'mainController'
  }).state('notifications.settings', {
    url: '/settings',
    templateUrl: '/templates/notifications/settings.html',
    controller: 'settingsController'
  }).state('signout', {
    url: '/signout',
    templateUrl: '/templates/signout/signout.html',
    controller: 'authCtrl',
    resolve: {
      signout: function($client) {
        $client.user.signOut()
      }
    }
  }).state('logs', {
    url: '/logs',
    templateUrl: '/templates/logs/logs.html',
    controller: 'logsController',
    data: {}
  }).state('logs.settings', {
    url: '/settings',
    templateUrl: '/templates/logs/settings.html',
    controller: 'settingsController',
    data: {}
  }).state('stats', {
    url: '/stats',
    templateUrl: '/templates/stats/stats.html',
    controller: 'statsController',
    data: {}
  }).state('stats.settings', {
    url: '/settings',
    templateUrl: '/templates/stats/settings.html',
    controller: 'settingsController',
    data: {}
  }).state('ideas', {
    url: '/ideas',
    templateUrl: '/templates/ideas/ideas.html',
    controller: 'ideasController',
    data: {}
  }).state('ideas.settings', {
    url: '/settings',
    templateUrl: '/templates/ideas/settings.html',
    controller: 'ideasController',
    data: {}
  }).state('aboutus', {
    url: '/aboutus',
    templateUrl: '/templates/misc/aboutus.html',
    controller: 'aboutusController',
    data: {}
  }).state('contactus', {
    url: '/contactus',
    templateUrl: '/templates/misc/contactus.html',
    controller: 'contactusController',
    data: {}
  }).state('tncs', {
    url: '/tncs',
    templateUrl: '/templates/misc/tncs.html',
    controller: 'tncsController',
    data: {}
  })
  $urlRouterProvider.otherwise('start')
  if (window.history && window.history.pushState) {
    $locationProvider.html5Mode(true)
    //$locationProvider.hashPrefix('')
  }
}
])
app.config(function($mdThemingProvider) {
  //palettes
  var neonRed = $mdThemingProvider.extendPalette('red', {
    '500': 'ff0000'
  })
  var neonGreen = $mdThemingProvider.extendPalette('green', {
    '500': '00ff00'
  })
  var neonBlue = $mdThemingProvider.extendPalette('blue', {
    '500': '0000ff'
  })
  $mdThemingProvider.definePalette('neonRed', neonRed)
  $mdThemingProvider.definePalette('neonGreen', neonGreen)
  $mdThemingProvider.definePalette('neonBlue', neonBlue)
  //themes
  $mdThemingProvider.theme('brightsky').primaryPalette('blue').accentPalette('green').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('codergreen').primaryPalette('green').accentPalette('neonGreen').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('crispyclean').primaryPalette('neonGreen').accentPalette('green').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('darkknight').primaryPalette('grey').accentPalette('green').warnPalette('red').backgroundPalette('grey').dark
  $mdThemingProvider.setDefaultTheme('default')
  $mdThemingProvider.theme('girly').primaryPalette('pink').accentPalette('green').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('loveray').primaryPalette('purple').accentPalette('blue').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('monkeyparadise').primaryPalette('yellow').accentPalette('green').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('orangecounty').primaryPalette('orange').accentPalette('green').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('outdoor').primaryPalette('green').accentPalette('grey').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('romance').primaryPalette('red').accentPalette('purple').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('skyblue').primaryPalette('blue').accentPalette('neonBlue').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('slate').primaryPalette('grey').accentPalette('brown').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('error').primaryPalette('red').accentPalette('grey').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('info').primaryPalette('blue').accentPalette('grey').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('success').primaryPalette('green').accentPalette('grey').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.theme('warn').primaryPalette('brown').accentPalette('grey').warnPalette('red').backgroundPalette('grey')
  $mdThemingProvider.setDefaultTheme('default')
  $mdThemingProvider.alwaysWatchTheme(true)
})
app.run(function($http, $rootScope, $state, $timeout, $client, FREE_ZONES) {
  $rootScope.$on('$stateChangeStart', function(event, next) {
    $rootScope.$broadcast('load client')
    $rootScope.$broadcast('next state', next)
    $rootScope.$broadcast('previous state', $state.current)
    if ($client.socket && $client.socket.connected) {
      if (FREE_ZONES.names.indexOf(next.name) < 0) {
        if ($client.user.isLoggedIn()) {
          if ($client.user.details) {
            if (!$state.includes(next.name.split('.')[0]))
              $client.theme.choose(next)
          } else {
            event.preventDefault()
            $client.user.getUserInfo().then(function(data) {
              if (data.devicelocked) {
                $client.user.user = data.user
                $rootScope.$broadcast('user', data.user)
                $rootScope.$broadcast('locked')
                if ($client.identified) {
                  $client.user.details.devices.currentDevice = $client.this
                } else {
                  $client.identify().then(function(dev) {
                    $client.user.details.devices.currentDevice = dev
                  }, function(dev) {
                    $client.user.details.devices.currentDevice = dev
                  })
                }
                return
              }
              $rootScope.$broadcast('user', data.user)
              $client.user.details = data.user
              $client.user.details.settings = data.user.settings
              $client.startTimeout()
              if ($client.identified) {
                $client.user.details.devices.currentDevice = $client.this
              } else {
                $client.identify().then(function(dev) {
                  $client.user.details.devices.currentDevice = dev
                }, function(dev) {
                  $client.user.details.devices.currentDevice = dev
                })
              }
              if (!$state.includes(next.name.split('.')[0]))
                $client.theme.choose(next)
              if (data.user.settings.apps.general.resumetopreviousapp) {
                $client.navigator.goTo(data.user.state.general.lastapp)
              }
            }, function(error) {})
          }
        } else {
          event.preventDefault()
          $state.go('signin')
        }
      }
    } else {
      event.preventDefault()
      $client.connect().then(function(success) {
        $client.listen()
        $state.go(next.name)
      }, function(error) {
        window.localStorage.removeItem('device')
        $client.register().then(function(success) {
          $client.authenticate().then(function(success) {
            $client.listen()
            $state.go(next.name)
          }, function(error) {
            $client.toast.toast(error)
          }, function(info) {
            $client.toast.toast(info)
          })
        }, function(error) {
          $client.toast.toast(error).finally(function() {
            $window.location.reload()
          })
        })
      })  
      
      
    }
  })
  $rootScope.$on('$stateChange', function(event, next) {
    console.log(event, next)
  })
  $rootScope.$on('$stateChangeError', console.log.bind(console))
})
