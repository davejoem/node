'use strict'
// Load required packages
var mongoose = require('mongoose')
// Define a Notification schema
var NotificationSchema = new mongoose.Schema({
  action:{
    type: String,
    required: true
  },
  concern: {
    type: String,
    required: true
  },
  data:{
    required: false
  },
  devices: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Device"
  }],
  importance: {
    type: Number,
    required: true
  },
  message: {
    type: String,
    required: true
  },
  read: {
    type: Boolean,
    required: true,
    default: false
  },
  time: {
    type: Date,
    required: true,
    default: Date.now()
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }
})
// methods ======================
NotificationSchema.methods.markRead = function(cb) {
  this.read = true
  this.save((err,note)=>{
    if (err) return cb(err)
    cb(null , this)  
  })  
}
NotificationSchema.methods.markUnread = function(cb) {
  this.read = false
  this.save((err,note)=>{
    if (err) return cb(err)
    cb(null , this)  
  })
}
// create the model for Notifications and expose it to our app
module.exports = mongoose.model('Notification', NotificationSchema)
