var mainController = function($window, $location, $mdDialog, $mdSidenav, $timeout, $interval, $state, $rootScope, $scope, $mdBottomSheet, $mdMedia, $client) {
  $scope.socket = $client.socket;
  $scope.client = $client
  $scope.$watch(function() {
    return $mdMedia('gt-lg')
  }, function(lg) {
    $scope.gt_lg = lg;
  })
  $scope.$watch(function() {
    return $mdMedia('gt-md')
  }, function(md) {
    $scope.gt_md = md;
  })
  $scope.$watch(function() {
    return $mdMedia('gt-sm')
  }, function(sm) {
    $scope.gt_sm = sm;
  })
  $scope.$watch(function() {
    return $mdMedia('gt-xs')
  }, function(xs) {
    $scope.gt_xs = xs;
  })
  $scope.is = function(checker, value) {
    switch (value){
      case 'date': value = parseInt(new Date(Date.now()).toDateString().split(' ')[2])
      break
    }
    return is[checker](value)
  }
  $scope.deviceType = function(dev, message) {
    return dev == message.sender.device.machine
  }
  $scope.themes = $client.theme.themes;
  $scope.isLoggedIn = $client.user.isLoggedIn;
  $scope.locked = $client.locked;
  $scope.lockSession = $client.lockSession;
  $scope.signOut = function(deviceid, token) {
    $client.signOut(deviceid, token).then(function(success) {
      $client.toast.toast(success)
    }, function(error) {
      $client.toast.toast(error)
    })
  }
  $scope.signOutOfDevice = function() {
    $client.user.signOut().then(function(success) {
      $window.localStorage.removeItem('user')
      $state.go('start')
      $rootScope.$broadcast('reset')
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
  }
  $scope.isLoginPage = function() {
    return $state.is('signin')
  }
  $scope.getNotifications = function() {
    $client.note.get().then(function(data) {
      $rootScope.$broadcast('notifications', data)
    }, function(error) {
      $client.toast.toast(error)
    })
  }
  $scope.clearNotification = function(index) {
    $client.note.clear(index).then(function(data) {
      $rootScope.$broadcast('notifications', data)
    }, function(error) {
      $client.toast.toast(error)
    })
  }
  $scope.clearAllNotifications = function() {
    $client.note.clearAll().then(function(data) {
      $rootScope.$broadcast('notifications', data)
    }, function(error) {
      $client.toast.toast(error)
    })
  }
  $scope.concernIs = function(concern, notification) {
    return notification.concern == concern
  }
  $scope.toggleReadStatus = function(notification) {
    if (notification.read) {
      $client.say({
        event: 'notification unread',
        data: notification._id
      }).then(function(data) {
        $rootScope.$broadcast('notifications', data)
      }, function(error) {
        $client.toast.toast(error)
      }, function(info) {})
    } else {
      $client.say({
        event: 'notification read',
        data: notification._id
      }).then(function(data) {
        $rootScope.$broadcast('notifications', data)
      }, function(error) {
        $client.toast.toast(error)
      }, function(info) {})
    }
  }
  $scope.checkOutNotification = function(notification) {}
  $scope.notificationRead = function(notification) {
    return notification.read
  }
  $scope.isActiveState = function(state, suffix) {
    if (suffix)
      state += '.' + suffix;
    return $state.is(state)
  }
  $scope.urlIs = function(url) {
    return $location.pathname === url;
  }
  $scope.goto = $state.go;
  $scope.goTo = $client.navigator.goTo;
  $scope.goBack = $client.navigator.goBack;
  $scope.popup = function(options) {
    var dialog = {
      autoWrap: true,
      clickOutsideToClose: options.clickOutsideToClose || true,
      closeTo: options.event || options.scope.event || $scope.event || null ,
      fullscreen: options.fullscreen || false,
      openFrom: options.event || options.scope.event || $scope.event || null ,
      parent: options.parent || angular.element(document.body),
      preserveScope: options.preserveScope || true,
      scope: options.scope || $scope,
      targetEvent: options.event || null ,
      templateUrl: options.templateUrl || null
    }
    return $mdDialog.show(dialog)
  }
  $scope.sheetUp = function(options) {
    var sheet = {
      clickOutsideToClose: options.clickOutsideToClose || true,
      controller: options.controller || mainController,
      controllerAs: options.controllerAs || null ,
      disableParentScroll: options.disableParentScroll || true,
      escapeToClose: options.escapeToClose || true,
      locals: options.locals || null ,
      parent: options.parent || angular.element(document.body),
      preserveScope: options.preserveScope || false,
      resolve: options.resolve || null ,
      scope: options.scope || $scope,
      targetEvent: options.event || null ,
      template: options.template || null ,
      templateUrl: options.templateUrl || null
    }
    return $mdBottomSheet.show(sheet)
  }
  $scope.closeDialog = function(cancel) {
    if (cancel)
      $mdDialog.hide()
    else
      $mdDialog.cancel()
  }
  $scope.ripple = function(event) {
    $client.ripple.attach($scope, angular.element(event.target), {
      center: false
    })
  }
  $scope.findUser = function(username) {}
  ,
  $scope.showUser = function(event, username) {
    $client.user.getUserInfo(username).then(function(data) {
      $scope.person = data
      $scope.popup({
        scope: $scope,
        event: event,
        templateUrl: '/templates/misc/user.html'
      })
    }, function(error) {})
  }
  $scope.request = function(username) {
    var promise = $client.user.request(username)
    promise.then(function(data) {
      $scope.user = $client.user.details = data.user
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
    return promise
  }
  ,
  $scope.unrequest = function(username) {
    $client.user.unrequest(username).then(function(data) {
      $scope.user = $client.user.details = data.user
      $client.toast.toast(data)
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
  }
  ,
  $scope.befriend = function(username) {
    $client.user.befriend(username).then(function(data) {
      $scope.user = $client.user.details = data.user
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
  }
  ,
  $scope.unfriend = function(username) {
    $client.user.unfriend(username).then(function(data) {
      $scope.user = $client.user.details = data.user
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
  }
  ,
  $scope.block = function(username) {
    $client.user.block(username).then(function(data) {
      $scope.user = $client.user.details = data.user
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
  }
  ,
  $scope.unblock = function(username) {
    $client.user.unblock(username).then(function(data) {
      $scope.user = $client.user.details = data.user
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
  }
  ,
  $scope.follow = function(username) {
    $client.user.follow(username).then(function(data) {
      $scope.user = $client.user.details = data.user
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
  }
  ,
  $scope.unfollow = function(username) {
    $client.user.unfollow(username).then(function(data) {
      $scope.user = $client.user.details = data.user
    }, function(error) {
      $client.toast.toast(error)
    }, function(info) {
      $client.toast.toast(info)
    })
  }
  ,
  $scope.$on('user', function(event, user) {
    $client.$scope = $scope
    $client.user.details = user
    $scope.user = user;
    $scope.settings = user.settings;
    $client.note.notifications = user.notifications;
    $scope.notifications = user.notifications;
    $scope.state = user.state;
    if (user.settings.general.rememberstates) {
      $scope.$watch('state.states', function() {
        console.log("state change")
        // $client.user.details.state.states = $scope.state.states;
      })
    }
    if (user.settings.general.rememberviews) {
      $scope.$watch('state.views', function() {
        console.log("view change")
        //$client.user.details.state.views = $scope.state.views;
      })
    }
    for (app of $client.launcher.views.allapps.apps) {
      app.settings = user.settings.apps[app.id]
    }
    $scope.$emit('assigned')
  })
  $scope.$on('status', function(event, status) {
    $scope.sitestatus = status
    switch (status) {
    case 'registered':
      $scope.registered = true
      break
    case 'active':
      $timeout(function() {
        $rootScope.$broadcast('activity', 'Waiting to be ready')
      },2000)
      $timeout(function() {
        $scope.active = true  
        $timeout(function() {
          $rootScope.$broadcast('status', 'ready')
          $scope.$emit('ready')
        }, 1000)
      }, 1000)
      break
    case 'loaded':
      $scope.loaded = true
      break
    case 'ready':
      $scope.ready = true
      break
    case 'connected':
      $scope.online = true
      $scope.offline = false
      break
    case 'disconnected':
      $scope.active = false
      $scope.online = false
      $scope.offline = true
      break
    }
  })
  $scope.$on('activity', function(event, activity) {
    $interval.cancel($scope.interval)
    show(activity)
    function show(act) {
      var i = 0
        , dot = '.'
        , a = act
      $scope.interval = $interval(function() {
        i == 3 ? (act = a,
        i = 0) : (act += dot,
        $scope.activity = act,
        i++)
      }, 1000)
    }
  })
  $scope.$on('appsettings', function(event, settings) {
    $scope.user.settings.apps = settings;
    $scope.settings.apps = settings;
    $client.user.details.settings.apps = settings;
  })
  $scope.$on('reset', function() {
    $client.user.token = null ;
    $client.user.details = null ;
    $client.note.notifications = null ;
    $client.video.mymovies = null ;
    $client.video.myshows = null ;
    $client.gamer.mygames = null ;
  })
  $scope.$on('theme', function(event, data) {
    $scope.siteTheme = data.siteTheme;
    $scope.appTheme = data.appTheme;
  })
  $scope.$on('locked', function() {
    $mdDialog.cancel()
    $state.go('locked')
    $client.locked = true;
    $scope.lockreason = 'device'
    $client.cancelTimeout()
  })
  $scope.$on('signed out', function() {
    $window.localStorage.removeItem('user')
    $state.go('signin')
    $client.cancelTimeout()
  })
  $scope.$on('lock', function() {
    $mdDialog.cancel()
    $client.lockSession()
    $scope.lockreason = 'timeout'
    $client.cancelTimeout()
  })
  $scope.$on('next state', function(event, data) {
    $state.next = data
    $client.startTimeout()
    $scope.location = location.pathname
  })
  $scope.$on('previous state', function(event, data) {
    $state.previous = data;
  })
  $scope.$on('cancelTimeout', function() {
    $client.cancelTimeout()
  })
  $scope.$on('new notification', function(event, data) {
    $client.note.notifications.push(data)
    $client.user.details.notifications.push(data)
    $scope.notifications.push(data)
  })
  $scope.$on('notifications', function(event, data) {
    $scope.user.notifications = data
    $scope.notifications = data
    $client.note.notifications = data
    $client.user.details.notifications = data
  })
  var beforeUnload = function() {
    $client.user.syncUser().finally(function() {
      $rootScope.$broadcast('reset')
    })
  }
  $window.onbeforeunload = beforeUnload()
  $window.onmousemove = function() {
    $client.startTimeout()
  }
}
mainController.$inject = ['$window', '$location', '$mdDialog', '$mdSidenav', '$timeout', '$interval', '$state', '$rootScope', '$scope', '$mdBottomSheet', '$mdMedia', '$client'];
app.controller('mainController', mainController)
