app.animation('.fadeout', function () {
    return {
        addClass: function (element, className, done) {
            if (className == 'ng-hide') {
                TweenMax.to(element, 0.5, {opacity: 0, onComplete: done });
            }
            else {
                done();
            }
        },
        removeClass: function (element, className, done) {
            if (className == 'ng-hide') {
                element.removeClass('ng-hide');
                TweenMax.to(element, 0.5, {opacity: 1, onComplete: done });
            }
            else {
                done();
            }
        }
    };
});