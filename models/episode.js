'use strict'
// Load required packages
var mongoose = require('mongoose');

// Define an episode schema
var EpisodeSchema = new mongoose.Schema({
  actors: {type: String},
  awards: {type: String},
  averagerating: {type: Number, default: 0, required: true}, 
  comments: {
    comments: [{      
      author: {
        id: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
        username: {type: String, required: true},
      },
      comment: {type: String, required: true}, 
      dislikes: {type: Number, default: 0, required: true},
      likes: {type: Number, default: 0, required: true},
      rating: {type: Number, default: 0, required: true}
    }],
    count: {type: Number, default: 0, required: true}
  },
  country: {type: String, required: true},
  director: {type: String, required: true}, 
  downloads: {type: Number, default: 0, required: true},
  episodeNumber: {type: String, required: true},
  genre: {type: String, required: true},
  imdbRating: {type: String, required: true},
  imdbID: {type: String, required: true, unique: true},
  language: {type: String, required: true},
  name:  {type:String, required: true, unique: true},
  number: {type:String, required: true, unique: false},
  owners: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],
  path: {type: String, required: true, unique: true},
  plays: {type: Number, default: 0, required: true}, 
  plot: {type: String, required: true},
  poster:{type: String, required: true},
  price: {type: Number, default: 5, required: true}, 
  rated: {type: String, required: true},
  released: {type: String, required: true},
  runtime: {type: String, required: true},
  season: {type:mongoose.Schema.Types.ObjectId, ref: 'Season', required: true},
  seasonNumber: {type: String, required: true},
  seriesID: {type: String, required: true},
  sharedwiths: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],
  sharers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],
  show: {type:mongoose.Schema.Types.ObjectId, ref: 'Show', required: true},
  title: {type: String, required: true},
  type: {type: String, required: true, default:"episode"},
  writer: {type: String, required: true},  
  year: {type: String, required: true}  
});

// methods ======================
// adding a review
EpisodeSchema.methods.addComment = function(comment) {    
    this.comments.count++;
    this.comments.comments.push(comment);
    return this;
};
// calculate new rating
EpisodeSchema.methods.calcAverageRating = function() {
  console.log("calculating");
    var total=0, averagerating=0;
    for(i=0;i<this.comments.comments.length;i++){
        total = this.comments.comments[i].rating+total;
        averagerating = total/this.comments.comments.length;
    }

    this.averagerating = averagerating;
    return averagerating;
};
// create the model for episode and expose it to our app
module.exports = mongoose.model('Episode', EpisodeSchema);