app.filter('time', function() {
  return function(raw, array) {
    var output
    var date = new Date(raw)
    var rawdate = new Date(raw).getTime()
    var second = 1000
    var minute = second * 60
    var hour = minute * 60
    var day = hour * 24
    var week = day * 7
    var year = day * 365
    var now = new Date(Date.now())
    function Day(d) {
      var self = {
        start: new Date(new Date(d - new Date(d).getHours() * hour).toDateString()).getTime(),
        end: new Date(new Date(d - new Date(d).getHours() * hour).toDateString()).getTime() + day - second,
      }
      return {
        start: self.start,
        end: self.end,
        daybefore: {
          start: self.start - day - day,
          end: self.end - day - day
        },
        yesterday: {
          start: self.start - day,
          end: self.end - day
        },
        today: {
          start: new Date(new Date(Date.now()).toDateString()),
          end: new Date(new Date(new Date(Date.now() + day).toDateString()).getTime() - second)
        },
        tomorrow: {
          start: self.start + day,
          end: self.end + day
        },
        dayafter: {
          start: self.start + day + day,
          end: self.end + day + day
        }
      }
    }
    function Week(d) {
      var self = {
        start: new Date(new Day(d - (new Date(d).getDay() * day)).start).getTime(),
        end: new Date(new Day(d - (new Date(d).getDay() * day)).start).getTime() + week - second,
      }
      return {
        start: self.start,
        end: self.end,
        last: {
          start: self.start - week,
          end: self.start - second
        },
        next: {
          start: self.end + second,
          end: self.end + week
        }
      }
    }
    function Month(d) {
      var self = {
        start: new Date(new Date(d).toDateString().split(' ')[1] + ' ' + new Date(d).getFullYear()).getTime(),
        end: new Date(d).getMonth() == 11 ? new Date((new Date(d).getFullYear() + 1).toString()).getTime() - (3 * hour) - second : new Date(d).getMonth() == 10 ? new Date(new Date('12 1 ' + new Date(d).getFullYear()).getTime() - second).getTime() : new Date(d).getMonth() + 2 <= 11 ? new Date(new Date(d).getMonth() + 2 + ' 1 ' + new Date(d).getFullYear()).getTime() - second : null
      }
      return {
        start: self.start,
        end: self.end,
        last: {
          start: new Date(self.start).getMonth() == 0 ? new Date('12 1 ' + (new Date(self.start).getFullYear() - 1)).getTime() : new Date(self.start).getMonth() == 1 ? new Date('1 1 ' + new Date(self.start).getFullYear()).getTime() : new Date(self.start).getMonth() > 1 ? new Date(new Date(self.start).getMonth() + ' 1 ' + new Date(self.start).getFullYear()).getTime() : null ,
          end: self.start - second,
        },
        next: {
          start: self.end + second,
          end: new Date(self.start).getMonth() == 11 ? new Date(new Date('2 1 ' + (new Date(self.start).getFullYear() + 1)).getTime() - second).getTime() : new Date(self.start).getMonth() == 10 ? new Date(new Date('1 1 ' + (new Date(self.start).getFullYear() + 1)).getTime() - second).getTime() : new Date(self.start).getMonth() + 2 <= 11 ? new Date(new Date(self.start).getMonth() + 2 + ' 1 ' + new Date(self.start).getFullYear()).getTime() - second : new Date(new Date(self.start).getMonth() + 2 + ' 1 ' + new Date(self.start).getFullYear()).getTime() - second
        }
      }
    }
    function Year(d) {
      var self = {
        start: new Date(new Date(d).toDateString().split(' ')[3]).getTime() - (3 * hour),
        end: new Date((parseInt(new Date(d).toDateString().split(' ')[3]) + 1).toString()).getTime() - (3 * hour) - second,
      }
      return {
        start: self.start,
        end: self.end,
        last: {
          start: new Date((parseInt(new Date(self.start).toDateString().split(' ')[3]) - 1).toString()).getTime() - (3 * hour),
          end: self.start - second,
        },
        next: {
          end: self.end + second,
          end: new Date((parseInt(new Date(self.start).toDateString().split(' ')[3]) + 2).toString()).getTime() - (3 * hour) - second,
        }
      }
    }
    function format() {
      if (array) {
        if (array[0].timeformat == '12') {
          h = parseInt(date.toTimeString().split(' ')[0].split(':')[0])
          if (h > 12) {
            h -= 12
            output = h + ':' + date.toTimeString().split(' ')[0].split(':')[1] + ' pm'
          } else
            output = date.toTimeString().split(' ')[0].split(':')[0] + ':' + date.toTimeString().split(' ')[0].split(':')[1] + ' am'
        } else
          output = date.toTimeString().split(' ')[0].split(':')[0] + ':' + date.toTimeString().split(' ')[0].split(':')[1] + ' HRS'
        if (array[0].caps)
          output = output.toUpperCase()
        else
          output = output.toLowerCase()
        return output
      } else
        output = date.toTimeString().split(' ')[0].split(':')[0] + ':' + date.toTimeString().split(' ')[0].split(':')[1] + ' HRS'
    }
    if (is.within(rawdate, Year(rawdate).last.start, Year(rawdate).last.end)) {
      output = 'Last year ' + date.toDateString().split(' ')[0]
      output += ' ' + date.toDateString().split(' ')[1]
      output += ' ' + date.toDateString().split(' ')[2]
      output += ' ' + format()
    }
    if (is.within(rawdate, Month(rawdate).last.start, Month(rawdate).last.end)) {
      output = 'Last month ' + date.toDateString().split(' ')[0]
      output += ' ' + date.toDateString().split(' ')[2]
      output += ' ' + format()
    }
    if (is.within(rawdate, Week(rawdate).last.start, Week(rawdate).last.end)) {
      output = 'Last week ' + date.toDateString().split(' ')[0]
      output += ' ' + format()
    }
    if (is.within(rawdate, Year(rawdate).next.start, Year(rawdate).next.end)) {
      output = 'Next year ' + date.toDateString().split(' ')[0]
      output += ' ' + date.toDateString().split(' ')[1]
      output += ' ' + date.toDateString().split(' ')[2]
      output += ' ' + format()
    } else if (is.within(rawdate, Month(rawdate).next.start, Month(rawdate).next.end)) {
      output = 'Next month ' + date.toDateString().split(' ')[0]
      output += ' ' + date.toDateString().split(' ')[2]
      output += ' ' + format()
    } else if (is.within(rawdate, Week(rawdate).next.start, Week(rawdate).next.end)) {
      output = 'Next week ' + date.toDateString().split(' ')[0]
      output += ' ' + format()
    }
    if (is.within(rawdate, Week(date).start, Week(date).end))
      output = date.toDateString().split(' ')[0] + ' ' + format()
    if (rawdate >= Day(rawdate).dayafter.start && rawdate <= Day(rawdate).dayafter.end) {
      output = 'Day after tomorrow ' + format()
    }
    if (rawdate> Day(rawdate).daybefore.start && rawdate < Day(rawdate).daybefore.end) {
      output = 'Day before yesterday ' + format()
    }
    if (is.tomorrow(date))
      output = 'Tomorrow ' + format()
    if (is.today(date))
      output = format()
    if (is.yesterday(new Date(date)))
      output = 'Yesterday ' + format()
    return output
  }
})
