'use strict';
module.exports = (agent) => {
    var consts = agent.consts;
    var fs = agent.fs;
    return (log,cb) => {
        var Log = q.defer();
        if (log.success) {
            fs.appendFile(consts.logFile, "\n["+Date.now()+"] [SUCCESS] "+log.log, (err) => {
                if (err) return Log.reject(err);
                Log.resolve('Logged!');
            });
        }
        if (log.info) {
            fs.appendFile(consts.logFile, "\n["+Date.now()+"] [INFO] "+log.log, (err) => {
                if (err) return Log.reject(err);
                Log.resolve('Logged!');
            });
        }
        if (log.warn) {
            fs.appendFile(consts.logFile, "\n["+Date.now()+"] [WARNING] "+log.log, (err) => {
                if (err) return Log.reject(err);
                Log.resolve('Logged!');
            });
        }
        if (log.err) {
            fs.appendFile(consts.logFile, "\n["+Date.now()+"] [ERROR] "+log.log, (err) => {
                if (err) return Log.reject(err);
                Log.resolve('Logged!');
            });
        }
        Log.promise.nodeify(cb);
        return Log.promise;
    }    
}