(https,http,q) => {
    module.exports = (request, mode, module, callback) => {
        var response = q.defer(), responseData = "";
        if (request) {
            https.get(request, (res) => {
                res.on('data', (chunk) => {responseData += chunk;})
                    .on('error', (err) => {
                        log({log: "TMDB-GET: Response stream error!", err: err});
                        response.reject(err); //same as return cb(null);
                    })
                    .on('end', () => {
                        var responseObject;
                        try  {
                            responseObject = JSON.parse(responseData);
                        } catch (err) {
                            log({log: "TMDB-GET: Response wasn't valid JSON.", err: err});
                            response.resolve(responseData); //same as return cb(null);
                        }
                        log({log: path + "TMDB-GET: Response success.", success: "Item found"});
                        response.resolve(responseObject); //same as return cb(responseObject);
                    });
            }).on('error', (err) => {
                log({log: "TMDB-GET: Web request error!", err: err});
                response.reject(err); //same as return cb(null);
            });
            
        }
        else {
            log({log: "TMDB-GET: Request path error!", err: new Error("Expected at least")});
            response.reject(err); //same as return cb(null);
        }
        response.promise.nodeify(callback);
        return response.promise;
    }
}