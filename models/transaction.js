'use strict'
// Load required packages
var mongoose = require('mongoose');

// Define a payment schema
var TransactionSchema = new mongoose.Schema({
  name: {type:String, required: true},
  for: {type: mongoose.Schema.Types.ObjectId, required: false },
  by: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
  For: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false},
  description: {type:String, required: true},
  transactionDate: {type: Date, required: true},
  code: {type:String, unique: true, required: false},
  fromDevice: {type: mongoose.Schema.Types.ObjectId, ref: 'Device', required: false},
  amount: {type: Number, required: true}
});

TransactionSchema.methods.setRef = function(ref){
  this.for.ref = ref;
}

// create the model for payments and expose it to our app
module.exports = mongoose.model('Transaction', TransactionSchema);