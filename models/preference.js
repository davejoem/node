'use strict'
// Load required packages
var mongoose = require("mongoose");
// Define a device schema
var PreferenceSchema = new mongoose.Schema({
  device: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Device"
  },
  main: {
    type: Boolean,
    required: true,
    default: false
  },
  apps: {
    display: {
      theme: {
        type: String,
        required: true,
        default: "default"
      }
    }
  },
  video: {
    display: {
      orderby: {
        type: String,
        required: true,
        default: "title"
      },
      playertype: {
        type: String,
        required: true,
        default: "default"
      },
      quickactions: {
        type: Boolean,
        required: true,
        default: false
      },
      showfilterbar: {
        type: Boolean,
        required: true,
        default: false
      },
      showinlauncher: {
        type: Boolean,
        required: true,
        default: true
      },
      startin: {
        type: String,
        required: true,
        default: "home"
      },
      starton: {
        type: String,
        required: true,
        default: "movies"
      },
      theme: {
        type: String,
        required: true,
        default: "crispyclean"
      }
    },
    play: {
      autofullscreen: {
        type: Boolean,
        required: true,
        default: false
      },
      resumeplaypos: {
        type: Boolean,
        required: true,
        default: false
      },
      subtitles: {
        type: Boolean,
        required: true,
        default: false
      },
      showvideocomments: {
        type: Boolean,
        required: true,
        default: true
      }
    }
  },
  music: {
    display: {
      showinlauncher: {
        type: Boolean,
        required: true,
        default: true
      },
      playertype: {
        type: String,
        required: true,
        default: "default"
      },
      startin: {
        type: String,
        required: true,
        default: "home"
      },
      showfilterbar: {
        type: Boolean,
        required: true,
        default: false
      },
      quickactions: {
        type: Boolean,
        required: true,
        default: false
      },
      theme: {
        type: String,
        required: true,
        default: "outdoor"
      }
    },
    play: {
      resumeplay: {
        type: Boolean,
        required: true,
        default: false
      },
      continueplayback: {
        type: Boolean,
        required: true,
        default: true
      },
      quickrestart: {
        type: Boolean,
        required: true,
        default: true
      },
      lyrics: {
        type: Boolean,
        required: true,
        default: false
      },
      showcomments: {
        type: Boolean,
        required: true,
        default: true
      }
    }
  },
  gamer: {
    display: {
      showinlauncher: {
        type: Boolean,
        required: true,
        default: true
      },
      viewertype: {
        type: String,
        required: true,
        default: "default"
      },
      startin: {
        type: String,
        required: true,
        default: "home"
      },
      shownav: {
        type: Boolean,
        required: true,
        default: true
      },
      quickactions: {
        type: Boolean,
        required: true,
        default: false
      },
      theme: {
        type: String,
        required: true,
        default: "loveray"
      }
    }
  },
  general: {
    lockscreen: {
      haspin: {
        type: Boolean,
        required: true,
        default: false
      },
      pin: {
        type: Number,
        required: true,
        default: 1234
      },
      timeout: {
        type: Number,
        required: true,
        default: 10
      },
      lock: {
        type: Boolean,
        required: true,
        default: true
      },
      key: {
        type: String,
        required: true,
        default: "password"
      }
    },
    theme: {
      type: String,
      required: true,
      default: "default"
    },
    resumetopreviousapp: {
      type: Boolean,
      required: true,
      default: true
    },
    rememberstates: {
      type: Boolean,
      required: true,
      default: true
    },
    rememberviews: {
      type: Boolean,
      required: true,
      default: true
    },
    universaltheme: {
      type: Boolean,
      required: true,
      default: false
    }
  },
  linkapp: {
    display: {
      showinlauncher: {
        type: Boolean,
        required: true,
        default: true
      },
      startin: {
        type: String,
        required: true,
        default: "conversations"
      },
      showfilterbar: {
        type: Boolean,
        required: true,
        default: false
      },
      quickactions: {
        type: Boolean,
        required: true,
        default: false
      },
      theme: {
        type: String,
        required: true,
        default: "default"
      }
    },
    calls: {},
    messaging: {
      historylength: {
        type: String,
        required: true,
        default: "100"
      },
      messagertype: {
        type: String,
        required: true,
        default: "default"
      },
      notifyonnewmessage: {
        type: Boolean,
        required: true,
        default: true
      }
    }
  },
  profile: {
    display: {
      showinlauncher: {
        type: Boolean,
        required: true,
        default: false
      },
      startin: {
        type: String,
        required: true,
        default: "general"
      },
      theme: {
        type: String,
        required: true,
        default: "darkknight"
      }
    }
  },
  settings: {
    display: {
      showinlauncher: {
        type: Boolean,
        required: true,
        default: false
      },
      startin: {
        type: String,
        required: true,
        default: "home"
      },
      theme: {
        type: String,
        required: true,
        default: "slate"
      }
    },
    misc: {
      autosync: {
        type: Boolean,
        required: true,
        default: true
      }
    }
  },
  notifications: {
    display: {
      highlightunread: {
        type: Boolean,
        required: true,
        default: false
      },
      markreadonview: {
        type: Boolean,
        required: true,
        default: false
      },
      orderby: {
        type: String,
        required: true,
        default: "date"
      },
      quickactions: {
        type: Boolean,
        required: true,
        default: true
      },
      showfilterbar: {
        type: Boolean,
        required: true,
        default: false
      },
      showinlauncher: {
        type: Boolean,
        required: true,
        default: true
      },
      theme: {
        type: String,
        required: true,
        default: "brightsky"
      }
    },
    fetch: {
      historylength: {
        type: String,
        required: true,
        default: "day"
      },
      frequency: {
        type: Number,
        required: true,
        default: 30
      },
      filter: [{
        type: String,
        required: true,
        default: ""
      }]
    }
  },
  ideas: {
    display: {
      showinlauncher: {
        type: Boolean,
        required: true,
        default: false
      },
      theme: {
        type: String,
        required: true,
        default: "monkeyparadise"
      }
    }
  },
  stats: {
    display: {
      showinlauncher: {
        type: Boolean,
        required: true,
        default: false
      },
      theme: {
        type: String,
        required: true,
        default: "skyblue"
      }
    }
  },
  logs: {
    display: {
      showinlauncher: {
        type: Boolean,
        required: true,
        default: false
      },
      showfilterbar: {
        type: Boolean,
        required: true,
        default: false
      },
      theme: {
        type: String,
        required: true,
        default: "codergreen"
      }
    },
    logging: {
      livelog: {
        type: Boolean,
        required: true,
        default: false
      }
    }
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  }
});
// methods ======================
PreferenceSchema.methods.edit = function(cb) {}
;
// create the model for devices and expose it to our app
module.exports = mongoose.model("Preference", PreferenceSchema);
