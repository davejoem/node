'use strict'
// Load required packages
var mongoose = require('mongoose');
var Episode = require('./episode');
var log = require('../utils/log')(require('fs'),require('path'),require('../utils/consts'));

// Define a series schema
var ShowSchema = new mongoose.Schema({
  actors: String,
  awards: String,
  adult: {type: Boolean, default: false, required: true},
  averagerating: {type: Number, default: 0, required: true}, 
  backdrop_path: String,
  belongs_to_collection:{
    id: Number,
    name: String,
    poster_path: String,
    backdrop_path: String
  },
  boxoffice: String,
  budget: Number,
  comments: {
    count: {type: Number, default: 0, required: true},
    comments: [{      
      author: {
        id: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
        username: {type: String, required: true},
      },
      comment: {type: String, required: true}, 
      rating: {type: Number, default: 0, required: true},
      likes: {type: Number, default: 0, required: true},
      dislikes: {type: Number, default: 0, required: true}
    }]
  },  
  country: String,
  director: String, 
  downloads: {type: Number, default: 0, required: true},
  dvd: String,
  episodes: [{type:mongoose.Schema.Types.ObjectId, ref: 'Episode', unique: true}],
  genre: String,
  genres: [{id: Number, name: String}],  
  homepage: String,
  id: Number,
  imdb_id: {type: String, required: false, unique: true},
  imdbid: {type: String, required: false, unique: true},
  imdbrating: String,
  imdbvotes: String,
  language: String,
  metascore: String,
  original_language: String,
  original_title: String,
  overview:  String,
  owners: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],
  path: {type: String, required: true, unique: true},
  plays: {type: Number, default: 0, required: true}, 
  plot: {type: String, required: false},
  popularity: Number, 
  poster: String,
  Poster: String,
  poster_path: String,
  price: {type: Number, default: 30, required: true}, 
  production: String,
  production_companies: [{name: String, id: Number}],
  production_countries: [{iso_3166_1: String, name: String}],
  rated: {type: String, required: true},
  released: {type: String, required: true},
  release_date: String,
  revenue: Number,
  rated: {type: String, required: false},
  released: {type: String, required: false},
  runtime: {type: String, required: true},
  seasons: [{type:mongoose.Schema.Types.ObjectId, ref: 'Season', unique: true}],
  spoken_languages: [{iso_639_1: String, name: String}],
  sharers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],
  sharedwiths: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true}],  
  ssns: Array,
  status: String,
  tagline: String,
  title: {type: String, required: true},
  tomatoconsensus: String,
  tomatofresh: String,
  tomatoimage: String,
  tomatometer: String,
  tomatorating: String,
  tomatoreviews: String,
  tomatorotten: String,
  tomatourl: String,
  tomatousermeter: String,
  tomatouserrating: String,
  tomatouserreviews: String,
  type: {type: String, required: true, default:"show"},
  video: Boolean,
  vote_average: Number,
  vote_count: Number,
  website: String,
  writer: String,
  year: {type: String, required: true}
});

// methods ======================
// calculate price
ShowSchema.methods.calcPrice = function(cb) {    
    var price=0; 
    Episode.find({"show": this._id}, (err,episodes)=>{
      if (err) {
        log({log: this.name + ": Error calculating price", err: err});
        return cb();
      }
      for(i=0;i<episodes.length;i++){
        price+=episodes[i].price;
      }
      return cb(price);
    });
};
// adding a comment
ShowSchema.methods.addComment = function(comment) {    
    this.comments.count++;
    this.comments.comments.push(comment);
    return this;
};
// calculate new rating
ShowSchema.methods.calcAverageRating = function() {
  var total=0, averagerating=0;
  for(i=0;i<this.comments.comments.length;i++){
    total = this.comments.comments[i].rating+total;
    averagerating = total/this.comments.comments.length;
  }
  averagerating = Math.round(averagerating);
  this.averagerating = averagerating;
  return this;
};
// create the model for series and expose it to our app
module.exports = mongoose.model('Show', ShowSchema);