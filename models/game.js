'use strict'
// Load required packages
var mongoose = require('mongoose');

// Define a movie schema
var GameSchema = new mongoose.Schema({
  name:  {type:String, default: "", required: true},
  description: {type: String, default: "", required: true}, 
  releaseyear: Date,
  genres: [{type: String}],
  studios: [{type: String}],
  price: {type: Number, default: 0, required: true}, 
  averagerating: {type: Number, default: 0, required: true}, 
  downloads: {type: Number, default: 0, required: true}, 
  reviews: {
    count: {type: Number, default: 0, required: true},
    reviews: [{      
      reviewer: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
      review: {type: String, default: "", required: true}, 
      rating: {type: Number, default: 0, required: true}, 
    }]
  },  
  owners: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  sharers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  sharedwiths: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
});

// methods ======================
// adding a review
GameSchema.methods.addReview = function(review) {    
    this.reviews.count++;
    this.reviews.push(review);
    return this;
};
// calculate new rating
GameSchema.methods.calcAverageRating = function() {    
    var total=0, averagerating=0;
    for(i=0;i<this.reviews.length;i++){
        total = this.reviews[i].rating+total;
        averagerating = total/this.reviews.length;
    }
    this.averagerating = averagerating;
    return averagerating;
};
// create the model for movies and expose it to our app
module.exports = mongoose.model('Game', GameSchema);