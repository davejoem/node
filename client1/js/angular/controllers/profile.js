var profileController = function($timeout, $state, $scope, $mdDialog, $client) {
  $scope.User = angular.merge({}, $client.user.details)
  $scope.settings = $client.user.details.settings.apps.profile
  $scope.notifications = $client.note.notifications
  $scope.Device = $client.this
  $scope.refresh = function() {
    $client.user.getUserInfo().then(function(data) {
      $client.user.details = data
    });
  }
  $scope.titles = [{
    title: 'Mister',
    abbrev: 'Mr.',
    gender: 'Male'
  }, {
    title: 'Miss',
    abbrev: 'Ms.',
    gender: 'Female'
  }, {
    title: 'Misses',
    abbrev: 'Mrs.',
    gender: 'Female'
  }, {
    title: 'Professor',
    abbrev: 'Prof.',
    gender: null
  }, {
    title: 'Doctor',
    abbrev: 'Dr.',
    gender: null
  }, {
    title: 'Honourable',
    abbrev: 'Hon.',
    gender: null
  }, ];
  if ($scope.user.settings.apps.general.rememberstates) {
    $scope.selectedIndex = $scope.user.state.states.profile.laststate;
  } else {
    switch($scope.user.settings.apps.profile.display.startin) {
    case 'general':
      $scope.selectedIndex = 0;
      break;
    case 'social':
      $scope.selectedIndex = 1;
      break;
    case 'devices':
      $scope.selectedIndex = 2;
      break;
    case 'finance':
      $scope.selectedIndex = 3;
      break;
    case 'previous':
      $scope.selectedIndex = $scope.user.state.states.profile.laststate;
      break;
    }
  }
  $scope.$watch('selectedIndex', function(index) {
    switch (index) {
    case 0:
      $scope.goTo("profile.details");
      $client.user.details.state.states.profile.laststate = 0;
      break;
    case 1:
      $scope.goTo("profile.social");
      $client.user.details.state.states.profile.laststate = 1;
      break;
    case 2:
      $scope.goTo("profile.devices");
      $client.user.details.state.states.profile.laststate = 2;
      break;
    case 3:
      $scope.goTo("profile.finance");
      $client.user.details.state.states.profile.laststate = 3;
      break;
    }
  })
  $scope.switchTo = function(index) {    
    $scope.with = false
    if (index > -1 || index < 4) {
      $scope.selectedIndex = index
    }
  }
  $scope.editing = false;
  $scope.edit = function() {
    $scope.editing = !$scope.editing;
  }
  $scope.confirm = function(obj) {
    obj.action ? $scope.action = obj.action : null ;
    obj.event ? $scope.event = obj.event : null ;
    if (obj.subscription) {
      $scope.subscription = {};
      switch (obj.subscription) {
      case 'premium':
        $scope.subscription.price = 500;
        break;
      case 'basic':
        $scope.subscription.price = 250;
        break;
      case 'free':
        $scope.subscription.price = 0;
        break;
      }
      $scope.subscription.type = obj.subscription
    }
    $scope.popup({
      scope: $scope,
      event: event,
      templateUrl: '/templates/profile/confirm.html'
    });
  }
  $scope.execute = function() {
    $scope.closeDialog(true);
    ($scope.action == 'deleteAccount') ? $scope.deleteAccount($scope.event, true) : ($scope.action == 'subscribe') ? $scope.subscribe($scope.subscription.type) : null ;
  }
  $scope.upgrade = function(event) {
    $scope.popup({
      event: event,
      scope: $scope,
      templateUrl: '/templates/misc/subscriptions.html'
    });
  }
  $scope.subscribe = function(subscription) {
    $client.user.subscribe(subscription).then(function(data) {
      $client.user.details.finance = data.finance;
      $rootScope.$broadcast('user', $client.user.details);
      $client.toast.toast(data.message.message);
    }, function(error) {
      $client.toast.toast(data.message.message);
    });
  }
  $scope.use = function(event, scheme) {
    $scope.event = event;
    !scheme ? $scope.popup({
      scope: $scope,
      event: event,
      templateUrl: '/templates/profile/choosemeans.html'
    }) : twitch();
    function twitch() {
      console.log(event, scheme);
      switch (scheme) {
      case 'mpesa':
        $scope.popup({
          scope: $scope,
          event: event,
          templateUrl: '/templates/profile/mpesa.html'
        });
        break;
      case 'airtelmoney':
        $scope.popup({
          scope: $scope,
          event: event,
          templateUrl: '/templates/profile/airtelmoney.html'
        });
        break;
      case 'equitel':
        $scope.popup({
          scope: $scope,
          event: event,
          templateUrl: '/templates/profile/equitel.html'
        });
        break;
      case 'mobikash':
        $scope.popup({
          scope: $scope,
          event: event,
          templateUrl: '/templates/profile/mobikash.html'
        });
        break;
      case 'paypal':
        $scope.popup({
          scope: $scope,
          event: event,
          templateUrl: '/templates/profile/paypal.html'
        });
        break;
      }
    }
  }
  $scope.sendTransCode = function(scheme) {
    $scope.User.finance.code.scheme = scheme;
    $client.user.sendTransCode($scope.User.finance.code).then(function(data) {
      $mdDialog.cancel();
      console.log(data);
      $scope.user.finance = $client.user.details.finance = data.finance;
      $scope.User.finance.code = null ;
      data.message ? $client.toast.toast(data.message.message) : $client.toast.toast("Transaction completed successfully.");
    }, function(error) {
      $mdDialog.cancel();
      console.log(error.data);
      $scope.User.finance.code = null ;
      error.data.message ? $client.toast.toast(error.data.message.message) : $client.toast.toast("An error occured. Please try again.");
    })
  }
  $scope.exec = function(event, username) {
    $scope.shareCredit(event, username);
  }
  $scope.shareCredit = function(event, username, amount) {
    $scope.action = "share credit";
    (!username && !amount) ? $scope.popup({
      scope: $scope,
      event: event,
      templateUrl: '/templates/misc/chooseuser.html'
    }) : (username && !amount) ? findUser() : (!username && amount) ? share() : null ;
    function findUser() {
      $client.user.findUser(username).then(function(data) {
        $scope.sharewith = data.username;
        $scope.popup({
          scope: $scope,
          event: event,
          templateUrl: '/templates/profile/shareamount.html'
        });
      }, function(error) {
        $mdDialog.cancel();
        error.data.message ? $client.toast.toast(error.data.message.message).then(function() {
          $scope.popup({
            scope: $scope,
            event: event,
            templateUrl: '/templates/misc/chooseuser.html'
          })
        }) : $client.toast.toast("An error occured. Please try again.").then(function() {
          $scope.popup({
            scope: $scope,
            event: event,
            templateUrl: '/templates/misc/chooseuser.html'
          })
        });
      });
    }
    function share() {
      $client.user.shareCredit($scope.sharewith, amount).then(function(data) {
        $mdDialog.cancel();
        $client.toast.toast(data.message.message);
        $scope.user = $client.user.details = data.user;
      }, function(error) {
        $mdDialog.cancel();
        console.log(error.data.err);
        error.data.message ? $client.toast.toast(error.data.message.message) : $client.toast.toast("An error occured. Please try again.");
      });
    }
  }
  $scope.closeDialog = function() {
    $mdDialog.cancel();
    $scope.event = null ;
  }
  $scope.signOutOf = function(index) {
    $scope.signIndex = index;
    var id = $client.user.details.devices.signedInOn[index]._id;
    $client.signOut(id).then(function(data) {
      $client.user.details.devices.signedOutOf.push($client.user.details.devices.signedInOn[index])
      $client.user.details.devices.signedInOn[index].message = data.message;
      $scope.signIndex = null ;
      $scope.clearSignedOutDevice(index, 3);
    }, function(error) {
      $client.toast.toast('An Error occured');
    });
  }
  $scope.clearSignedOutDevice = function(index, wait) {
    $timeout(function() {
      $client.user.details.devices.signedInOn.splice(index, 1);
      $scope.refresh();
    }, wait * 1000);
  }
  $scope.deleteAccount = function() {
    $client.user.deleteAccount().finally(function() {
      $client.toast.toast('Account successfully deleted.')
    });
  }
}
profileController.$inject = ['$timeout', '$state', '$scope', '$mdDialog', '$client'];
app.controller('profileController', profileController);
