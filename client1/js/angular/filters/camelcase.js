app.filter('camelcase', function() {
  return function(input, array) {
    var input = input || '', out = "", a = 0, b = 0, capitalize
    while (a < input.length) {
      while (b < array.length) {
        if (a == array[b]) {
          capitalize = true
          break
        } else
          capitalize = false
        b++
      }
      if (capitalize)
        out = out + input.charAt(a).toUpperCase()
      else
        out = out + input.charAt(a)
      b = 0
      a++
    }
    return out
  }
})
