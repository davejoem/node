module.exports = function(grunt) {
  grunt.initConfig({
    uglify: {
      build: {
        src: ['/client/web/javascripts/angular/floatr.js'],
        dest: '/client/web/javascripts/angular/floatr.min.js'
      }
    }
  })
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.registerTask('default', ['uglify'])
}
