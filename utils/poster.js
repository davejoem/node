'use strict';
module.exports = (agent) => {
	var https	= agent.https;
	var http	= agent.http;
	var url		= agent.url;
	var fs		= agent.fs;
	var path  	= agent.path;
	var consts	= agent.consts;
	var log		= agent.log;
	var Q		= agent.Q;

	function get(req, callback) {
		response = Q.Promise((resolve,reject,notify) => {
        	if (req) {
        		if (typeof req !== "object") throw new TypeError("The first argument must be an object.");        
	            if (callback && (typeof callback !== "function")) throw new TypeError("The callback must be a function.");        		
        		http.get(req, (res) => {
	            	res.on('data', (chunk) => {req.poster.write(chunk);})
	            		.on('error', (err) => {
	                        Q.nfcall(fs.unlink, path.resolve(consts.postersFolder,req.file)).done(
	                        	() => {
	                        		log({log: "POSTER-GET: Response stream error!", err: err});
	                        		reject(err); //same as return callback(err,null);},
	                        	},
	                        	(err) => {reject(err);} //same as return callback(err,null);},
	                        );
                    	})
                    	.on('end', () => {
                       		req.poster.end();
                       		log({log: "POSTER-GET: " + req.item + "'s poster was successfully saved.", success: "Saved!"});
                    	   	resolve(req.file); //same as return callback(null,req.file);
                	    });
            	}).on('error', (err) => {
        	        log({log: "POSTER-GET: Web request error!", err: err});
    	            reject(err); //same as return callback(err,null);
	            });
        	}
        	else {
            	err = new Error("Expects first argument to be an object or a string.");
            	log({log: "POSTER-GET: Request object/path error!", err: err});
            	reject(err); //same as return callback(err,null);
        	}
		});
        response.nodeify(callback);
        return response;
    }
    return (query, cb) => {
        var asked = Q.Promise((resolve,reject,notify) => {
        	if (query) {
            	if (cb && (typeof cb !== "function")) reject(new TypeError("The callback must be a function."));
            	if ((!query.poster || query.poster == "N/A") && query.poster_path) {
			    	query.poster = query.poster_path;
    		    }
    		    var parts = query.poster.split('.');
				var ext = parts[parts.length-1];
	    	    if (!query.imdbid) query.imdbid = query.imdb_id;
	    	    var options = {
						file: query.imdbid +'.'+ ext,
				    	host: url.parse(query.poster).host,
						item: query.title,
						mode: http,
						path: url.parse(query.poster).pathname,
						port: 80
				}          				
        		Q.nfcall(fs.readdir, consts.postersFolder).done(
        			(files) => {
        				console.log("looking for ",options.file);
        	        	Q.nfcall(fs.stat, path.resolve(consts.postersFolder,options.file)).done(
        	        		(stats) => {
        	        			if (stats.size > 0) {
        	        				console.log("Skipped "+options.item+"'s poster. Already exists. Size: ",stats.size);
                	    			log({log: options.item + "'s poster already exists.", info: options.item + "'s poster already exists."});
                    				resolve(options.file); //same as return cb(null,options.file);
	                			} else {
	                				console.log("Redownloading "+options.item+"'s poster. Size too small. Size: ",stats.size);
                	    			options.poster = fs.createWriteStream(path.resolve(consts.postersFolder,options.file));
        	        				get(options).done(
    	        	    				(poster) => resolve(options.file), //same as return cb(null,options.file);
        	        					(err) => reject(err) //same as return cb(err,null);
                					);
        	        			}
        	        		},
        	        		(err) => {
    							options.poster = fs.createWriteStream(path.resolve(consts.postersFolder,options.file));
        	        			get(options).done(
    	        	    			(poster) => resolve(options.file), //same as return cb(null,options.file);
        	        				(err) => reject(err) //same as return cb(err,null);
                				);
        	        		}
        	        	);
        			},
        			(err) => {
        				log({log: "[POSTER] Couldn't read posters folder!", err: err});
        		        reject(err);
        			}
        		);
    		} else {
            	err = new Error("Expects first argument to be an object or a string.");
            	log({log: "[POSTER] Request object/path error!", err: err});
            	reject(err); //same as return cb(err,null);
        	}
    	});
    	asked.nodeify(cb);
       	return asked;
    }
}