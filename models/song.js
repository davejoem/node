'use strict'
// Load required packages
var mongoose = require('mongoose')
//models
var Playlist = require('./playlist')
var Album = require('./album')
// Define a call schema
var SongSchema = new mongoose.Schema({
  album: {
    type: mongoose.Schem.Type.ObjectId,
    ref: 'Album'
  },
  albumart: String,
  artist: {
    type: mongoose.Schem.Type.ObjectId,
    ref: 'Artist'
  },
  awards: [{
    type: String
  }],
  availableformats: [{
    type: String
  }],
  averagerating: {
    type: Number,
    default: 0,
    required: true
  },
  bitrate: String,
  contibutingartists: [{
    type: mongoose.Schem.Type.ObjectId,
    ref: 'Artist'
  }],
  comments: [{
    author: {
      id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
      },
      username: {
        type: String,
        required: true
      },
    },
    comment: {
      type: String,
      required: true
    },
    rating: {
      type: Number,
      default: 0,
      required: true
    },
    likes: {
      type: Number,
      default: 0,
      required: true
    },
    dislikes: {
      type: Number,
      default: 0,
      required: true
    },
    date: {
      type: Date,
      required: true,
      default: Date.now()
    }
  }],
  id: Number,
  country: String,
  producer: String,
  genre: String,
  subgenres: [{
    id: Number,
    name: String
  }],
  homepage: String,
  language: String,
  length: {
    type: String,
    required: true
  },
  metascore: String,
  mood: String,
  overview: String,
  owners: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    unique: true
  }],
  path: {
    type: String,
    required: true,
    unique: true
  },
  plays: {
    type: Number,
    default: 0,
    required: true
  },
  rated: {
    type: String,
    required: true
  },
  release_date: String,
  revenue: Number,
  sharers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    unique: true
  }],
  sharedwiths: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    unique: true
  }],
  skips: {
    type: Number,
    default: 0,
    required: true
  },
  songart: String,
  studios: [{
    type: String
  }],
  title: {
    type: String,
    required: true
  },
  track_no: Number,
  type: {
    type: String,
    required: false
  },
  video: {
    has: Boolean,
    path: String
  },
  writer: String,
  year: String
})
// methods ======================
SongSchema.methods.addToPlaylist = function(playlistid, cb) {
  Playlist.findById(playlistid, (err,playlist)=>{
    if (err)
      return cb(err)
    playlist.songs.push(this._id)
    playlist.save()
    cb(null , this)
  }
  )
}
SongSchema.methods.calcAverageRating = function(cb) {
  var total = 0
    , averagerating = 0;
  for (i = 0; i < this.comments.length; i++) {
    total = this.comments[i].rating + total;
    averagerating = total / this.comments.length;
  }
  Math.round(averagerating);
  this.averagerating = averagerating;
  if (cb)
    cb(null , this);
  else
    return this;
}
// create the model for songs and expose it to our app
module.exports = mongoose.model('Song', SongSchema);
