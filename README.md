this is a node that can be deployed by boss and also be hosted online.

It implements the work flow that is described below. 

Note: This work flow tries to define event in sequencial order. Events that are client side are denoted by C and events 
that are server side are denoted by S. The trailing number will take the form of runlevel:stack:recurrent:index - desc. e.g.
 C0:2:1 indicates that event happens on run level 0, is run under stack 0 and 1 indicates it runs multiple times
        while 0 indicates it runs only once. - desc is a description of what is happening.

#start 
//client

C0:base:0:0 load index page
C0:base:0:1 angular is loaded
C0:base:0:2 socket.io is loaded
C0:base:0:3 angular is configured and run
C0:angular:0:0 display "loading client"
C0:angular:0:1 check for previous "config"
C0:angular:0:2 display "configuring client" if "config" exists
C0:angular:0:2 display "settting up client" if "config" doesn't exist
C0:socket:0:0 connect socket.io using previous config if exists or request config, report on failure
C0:angular:0:3 display "client configured" if "config" exists and successful connection
C0:angular:0:3 display "saving configuration" if "config" didn't exist
C0:angular:0:4 save "config" if recieved else report fail and clear and reload page
C0:angular:0:5 display "saved configuration" if "config" saved
C0:angular:0:5 display "configuration error" if "config" didn't save
C0:socket:0:1 configure socket environment if everything is ok
C0:socket:0:2 connect socket.io dependancies if successfully connected
C0:socket:1:3 start socket.io event listening
C0:angular:0:6 display start page or requested page
C0:angular:2:7 respond to socket events functions