module.exports = (agent) => {
    var consts  = agent.consts;
    var fn      = agent.fn;
	var https	= agent.https;
	var log		= agent.log;
	var Q		= agent.Q;

    function get(req, callback) {
        var response = Q.Promise((resolve,reject,notify) => {
            var responseData = "";
            if (callback && (typeof callback !== "function")) reject(new TypeError("The callback must be a function."));
            if (req) {
                https.get(req, (res) => {
                    res.on('data', (chunk) => {responseData += chunk;})
                        .on('error', (err) => {
                            log({log: "OMDB-GET: Response stream error!", err: err});
                            reject(err); //same as return callback(err,null);
                        })
                        .on('end', () => {
                            Q(JSON.parse(responseData)).done(
                                (obj) => {
                                    if (obj.Response == "True") {
                                        log({log: "OMDB-GET: Response success.", success: "Item found"});
                                        return resolve(fn.objectKeysToLowerCase(obj)); //same as return callback(null,keysToLowerCase(responseObject));                                        
                                    }
                                    if (obj.Response == "False") {
                                        err = new Error("Item not found");
                                        log({log: "OMDB-GET: " + path + " returned zero results!", err: err});
                                        return reject(err); //same as return callback(err,null);
                                    }
                                },
                                (err) => {
                                    log({log: "OMDB-GET: Response wasn't valid JSON.", err: err});
                                    return reject(err); //same as return callback(err,null);
                                }
                            );
                        });
                }).on('error', (err) => {
                    log({log: "OMDB-GET: Web request error!", err: err});
                    reject(err); //same as return callback(err,null);
                });
            }   
            else {
                err = new Error("Expects first argument to be an object or a string.");
                log({log: "OMDB-GET: Request object/path error!", err: err});
                reject(err); //same as return callback(err,null);
            }
        });
        response.nodeify(callback);
        return response;
    }
    return (query, cb) => {
        var asked = Q.Promise((resolve,reject,notify) => {
            if (query) {
                if (cb && (typeof cb !== "function")) reject(new TypeError("The callback must be a function."));
                if (typeof query == "object") {
                    /*expect an query to an object in the form of:
                    * query = {
                    *    scheme: 't=' || 'i=' || 's=', -- required
                    *    search: 'String' -- required
                    *    year: 'String' --optional
                    *    type: 'String' --optional
                    *    year: 'String' --optional
                    *    r: 'String'    --optional
                    *    plot: 'String' --optional   //only when query.scheme = "t=" or "i="
                    *    page: 'string' --optional  //only when query.scheme = "s="
                    *    tomatoes: String --optional      //only when query.scheme = "t=" or "i="
                    * } 
                    */
                    if (!query.scheme || !query.search) {
                        err = new Error("Invalid search parameters.");
                        log({log: "Invalid search parameters.", err: err});
                        reject(err); //same as return cb(err,null);
                    }
                    var path = consts.omdbAPI + query.scheme + query.search;
                    if (query.type == "movie") path += "&type=" + query.type;
                    if (query.year) path += "&y=" + query.year;
                    if (query.plot) {
                        if (query.scheme == "t=" || query.scheme == "i=") path += "&plot=" + query.plot;
                    }
                    if ((query.scheme == "t=" || query.scheme == "i=") && !query.plot) path += "&plot=full";
                    if (query.scheme == "s=" && query.page) path += "&page" + query.page;
                    if (query.r) path += "&r" + query.r; 
                    if (!query.r) path += "&r=json";
                    if (query.tomatoes) path += "&tomatoes=true";
                    get(path).then(
                        (res) => resolve(res), //same as return cb(null,res);
                        (err) => reject(err) //same as return cb(err,null);                        
                    );
                }
                else if (typeof query == "string") {
                    get(path).then(
                        (res) => resolve(res), //same as return cb(null,res);
                        (err) => reject(err) //same as return cb(err,null);
                    );  
                }
                else {
                    err = new TypeError('OMDB: Expects an object or a string.');
                    log({log: ' [OMDB] Query type error.', err: err});
                    reject(err); //same as return cb(err,null);
                }   
            } else {
                err = new Error("Expects first argument to be an object or a string.");
                log({log: "[OMDB] Request object/path error!", err: err});
                reject(err); //same as return cb(err,null);
            }            
        });
        asked.nodeify(cb);
        return asked;
    }
}