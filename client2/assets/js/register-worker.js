(function(){
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('assets/js/service-worker.js')
      .catch(err => console.log('Error', err));
  }
})()
