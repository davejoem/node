module.exports = (agent) => {
    return {
        certificate: './certs/cert.pem',
        key: './certs/key.pem',
        appFolder: './fs/apps',
        linkFolder: './fs/links',
        moviesFolder: './fs/movies/files',
        showsFolder: './fs/shows/files',
        gamesFolder: './fs/games/files',
        postersFolder: '../client/web/images/posters',
        logFile: './fs/logs/alllogs.log',
        tmdbAPI: "https://api.themoviedb.org/3",
        omdbAPI: "https://www.omdbapi.com/?"
    }
}