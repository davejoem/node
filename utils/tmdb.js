module.exports = (agent) => {
    var auth    = agent.auth;
	var consts  = agent.consts;
	var extend  = agent.extend;
    var fn      = agent.fn;
	var fs		= agent.fs;
	var https	= agent.https;
	var log		= agent.log;
	var omdb	= agent.omdb;
	var Q		= agent.Q;
	
    function get(req, callback) {
        var response = Q.Promise((resolve,reject,notify) => {
            var responseData = "";
            if (callback && (typeof callback !== "function")) reject(new TypeError("The callback must be a function."));
            if (req) {
                https.get(req, (res) => {
                    res.on('data', (chunk) => {responseData += chunk;})
                        .on('error', (err) => {
                            log({log: "TMDB-GET: Response stream error!", err: err});
                            reject(err); //same as return callback(err,null);
                        })
                        .on('end', () => {
                            Q(JSON.parse(responseData)).then(
                                (obj) => {
                                    log({log: "TMDB-GET: Response success.", success: "Item found"});
                                    resolve(obj); //same as return callback(null,responseObject);
                                },
                                (err) => {
                                    log({log: "TMDB-GET: Response wasn't valid JSON.", err: err});
                                    reject(err); //same as return callback(err,null);
                                }
                            );
                        });
                }).on('error', (err) => {
                    log({log: "TMDB-GET: Web request error!", err: err});
                    reject(err); //same as return callback(err,null);
                });
            }
            else {
                err = new Error("Expects first argument to be an object or a string.")
                log({log: "TMDB-GET: Request object/path error!", err: err});
                reject(err); //same as return callback(err,null);
            }            
        });
        response.nodeify(callback);
        return response;
    }
    return (query, cb) => {
        var asked = Q.Promise((resolve,reject,notify) => {
            if (query) {
                if (cb && (typeof cb !== "function")) reject(new TypeError("The callback must be a function."));
                var key = auth.theMovieDB;
                if (!query.scheme || !query.search) {
                    err = new Error("Invalid search parameters.")
                    log({log: "Invalid search parameters.", err: err});
                    reject(err); //same as return cb(err,null);
                }
                var path = consts.tmdbAPI +'/search/'+ query.scheme;
                path += "?api_key=" + key;
                path += "&query=" + query.search;            
                if (query.scheme == "movie" && query.year) path += "&year=" + query.year;
                if (query.scheme == "tv" && query.year) path = path + "&first_air_date_year=" + query.year;
                if (query.scheme == "movie") path += "&include_adult=" + "true";
                get(path).then((res1) => {
                    path = consts.tmdbAPI +'/'+ query.scheme + '/' + res1.results[0].id + "?api_key=" + key;
                    get(path).then(
                        (res2) => {
                            if (res2.imdb_id) {
                                var query2 = {};
                                query2.scheme = 'i=';
                                query2.search = res2.imdb_id;
                                query2.tomatoes = true;
                                if (res2.year) query2.year = res2.year;
                                omdb(query2).then(
                                    (rs) => resolve(extend(res2,rs)),
                                    (err) => resolve(res2)
                                );
                            }
                            if (!res2.imdb_id) resolve(res2); //same as return cb(null,res2);
                        }
                    );                
                }).fail((err) => {
                    reject(err); //same as return cb(err,null);
                });
            } else {
                err = new Error("Expects first argument to be an object or a string.");
                log({log: "TMDB-GET: Request object/path error!", err: err});
                reject(err); //same as return cb(err,null);
            }
        });
        asked.nodeify(cb);
        return asked;
    }
}