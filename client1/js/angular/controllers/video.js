var videoController = function($window, $location, $timeout, $state, $scope, $mdDialog, $mdMedia, $client) {
  $scope.settings = $client.user.details.settings.apps.video
  $scope.view = $client.user.details.settings.apps.video.display.starton
  $scope.getMovies = $client.video.getMovies
  $scope.getMyMovies = $client.video.getMyMovies
  $scope.getShows = $client.video.getShows
  $scope.getMyShows = $client.video.getMyShows
  $scope.getVideos = function() {
    $client.video.getShows()
    $client.video.getMovies()
  }
  $scope.filterbar = $scope.settings.display.showfilterbar
  $scope.filter = {
    scheme: "all",
    searchText: ''
  }
  $scope.orderBy = ''
  function watch() {
    $scope.$watch('filter.searchText', function(text) {
      var expression
      switch ($scope.filter.scheme) {
      case 'all':
        expression = text
        break
      case 'title':
        expression = {
          title: text
        }
        break
      case 'actors':
        expression = {
          actors: text
        }
        break
      case 'genre':
        expression = {
          genre: text
        }
        break
      case 'year':
        expression = {
          year: text
        }
        break
      case 'studio':
        expression = {
          studio: text
        }
        break
      }
      ($state.is('video.library') && $scope.view == 'movies') ? (obj = {
        array: $scope.allmovies,
        expression: expression
      },
      $scope.movies = $client.search.filter(obj)) : ($state.is('video.mylibrary') && $scope.view == 'movies') ? (obj = {
        array: $scope.allmymovies,
        expression: expression
      },
      $scope.mymovies = $client.search.filter(obj)) : ($state.is('video.library') && $scope.view == 'shows') ? (obj = {
        array: $scope.allshows,
        expression: expression
      },
      $scope.movies = $client.search.filter(obj)) : ($state.is('video.mylibrary') && $scope.view == 'shows') ? (obj = {
        array: $scope.allmyshows,
        expression: expression
      },
      $scope.myshows = $client.search.filter(obj)) : null
    })
    $scope.$watch('orderBy', function(text) {
      ($scope.filter == 'all') ? expression = ["title", "year", "released"] : expression = [$scope.filter, "year", "released"];
      (text == 'asc') ? reverse = false : reverse = true;
      ($state.is("video.library") && $scope.view == "movies") ? (obj = {
        array: $scope.allmovies,
        expression: expression,
        reverse: reverse
      },
      $scope.movies = $client.search.orderBy(obj)) : ($state.is('video.mylibrary') && $scope.view == "movies") ? (obj = {
        array: $scope.allmymovies,
        expression: expression,
        reverse: reverse
      },
      $scope.mymovies = $client.search.orderBy(obj)) : ($state.is('video.library') && $scope.view == "shows") ? (obj = {
        array: $scope.allshows,
        expression: expression,
        reverse: reverse
      },
      $scope.movies = $client.search.orderBy(obj)) : ($state.is('video.mylibrary') && $scope.view == "shows") ? (obj = {
        array: $scope.allmyshows,
        expression: expression,
        reverse: reverse
      },
      $scope.myshows = $client.search.orderBy(obj)) : null
    })
  }
  $scope.myDate = new Date()
  $scope.minDate = new Date($scope.myDate.getFullYear() - 100,$scope.myDate.getMonth(),$scope.myDate.getDate())
  $scope.maxDate = $scope.myDate
  $scope.maxYear = $scope.myDate.getFullYear()
  $scope.movie = {}
  $scope.switchTo = function(index) {
    $scope.selectedIndex = index
  }
  $scope.switchView = function(view, action) {
    if (action == 'button') {
      $scope.view = view
    }
    if (action == 'right') {
      if ($scope.view != view) {
        $scope.view = view
      } else {
        if ($scope.selectedIndex > 0)
          $scope.selectedIndex -= 1
      }
    }
    if (action == 'left') {
      if ($scope.view != view) {
        $scope.view = view
      } else {
        if ($scope.selectedIndex < 4)
          $scope.selectedIndex += 1
      }
    }
  }
  if ($scope.user.settings.apps.general.rememberstates) {
    $scope.selectedIndex = $scope.user.state.states.video.laststate
  } else {
    switch ($scope.settings.display.startin) {
    case 'home':
      $scope.selectedIndex = 0
      break
    case 'library':
      $scope.selectedIndex = 1
      break
    case 'collection':
      $scope.selectedIndex = 2
      break
    case 'previous':
      $scope.selectedIndex = $scope.user.state.states.video.laststate
      break
    }
  }
  $scope.$watch('selectedIndex', function(index) {
    switch (index) {
    case 0:
      $scope.goTo("video.home")
      $client.user.details.state.states.video.laststate = 0
      break
    case 1:
      $scope.goTo("video.library")
      $client.user.details.state.states.video.laststate = 1
      break
    case 2:
      $scope.goTo("video.collection")
      $client.user.details.state.states.video.laststate = 2
      break
    case 3:
      if ($scope.linked()) {
        $scope.goTo("video.play")
      } else {
        if ($client.user.details.details.accounttype == "admin")
          $scope.goTo("video.add")
      }
      $client.user.details.state.states.video.laststate = 3
      break
    case 4:
      $scope.goTo("video.add")
      $client.user.details.state.states.video.laststate = 4
      break
    }
  })
  $scope.confirm = function(action, obj) {
    $scope.action = action
    obj.format ? $scope.item.format = obj.format : null
    $scope.popup({
      scope: $scope,
      templateUrl: '/templates/video/confirm.html'
    })
  }
  $scope.inform = function(item, event, index) {
    $scope.item = item
    $scope.event = event
    $scope.index = index
    $scope.popup({
      scope: $scope,
      templateUrl: '/templates/video/info.html'
    })
  }
  $scope.chooseFormat = function() {
    $scope.popup({
      scope: $scope,
      templateUrl: '/templates/video/format.html'
    })
  }
  $scope.execute = function() {
    $scope.closeDialog(true)($scope.action == 'buy') ? $scope.purchase($scope.item, $scope.event, $scope.index) : ($scope.action == 'download') ? $scope.download($scope.item, $scope.event, $scope.index) : ($scope.action == 'play') ? $scope.play($scope.item, $scope.event, $scope.index) : null
  }
  $scope.purchase = function(item, event, index) {
    (item.type == 'movie') ? ($scope.movies[index].buying = true,
    $client.video.buyMovie(item._id, index).then(function(data) {
      $client.user.details.Collection.movies = $client.video.mymovies = data.movies
      $client.video.ownMovies()
      $scope.movies[index].buying = item.buying = false
      $scope.movies[index].owned = item.owned = true
      $client.toast.toast(data.message.message).then(function() {
        $scope.inform($scope.item, $scope.event, $scope.index)
      })
    }, function(error) {
      $scope.movies[index].buying = false,
      console.log(error)
      error.message ? $client.toast.toast(error.message.message, null , null , $timeout($scope.inform($scope.item, $scope.event, $scope.index), 3000)) : $client.toast.toast('An error occured')
    })) : (item.type == 'show') ? ($scope.shows[index].buying = true,
    $client.video.buyShow(item._id, index).then(function(data) {
      $scope.shows[index].buying = false
      $client.user.details.Collection.shows = $client.video.myshows = data.shows
      $client.user.details.Collection.shows = data.shows
      $client.video.ownShows()
      $rootScope.$broadcast('BoughtShow', $client.video.buyindex)
      $client.video.buyindex = null
      $rootScope.$broadcast('Shows', $client.video.shows)
      $rootScope.$broadcast('MyShows', $client.video.myshows)
      $client.toast.toast(data.message.message)
    }, function(error) {
      $client.toast.toast(error.message.message)
      $scope.shows[index].buying = false
    })) : (item.type == 'season') ? ($scope.seasons[index].buying = true,
    $client.video.buySeason(item._id, index).then(function(data) {
      $scope.seasons[index].buying = false
    }, function(error) {
      $scope.seasons[index].buying = false
    })) : (item.type == 'episode') ? ($scope.episodes[index].buying = true,
    $client.video.buyEpisode(item._id, index).then(function(data) {
      $scope.episodes[index].buying = false
    }, function(error) {
      $scope.episodes[index].buying = false
    })) : null
  }
  $scope.download = function(item, event, index) {
    ($scope.item.type == 'movie') ? ($scope.movies[index].getting = true,
    $client.video.downloadMovie($scope.item).then(function() {
      $scope.movies[index].getting = false
      $scope.inform(item, event, index)
    })) : (item.type == 'show') ? ($scope.shows[index].buying = true,
    $client.video.downloadShow($scope.item._id).then(function() {
      $scope.shows[index].getting = false
      $scope.inform($scope.item, $scope.event, $scope.index)
    })) : (item.type == 'season') ? ($scope.shows[index].buying = true,
    $client.video.downloadSeason($scope.item._id).then(function() {
      $scope.shows[index].getting = false
      $scope.inform($scope.item, $scope.event, $scope.index)
    })) : (item.type == 'episode') ? ($scope.shows[index].buying = true,
    $client.video.downloadEpisode($scope.item._id).then(function() {
      $scope.shows[index].getting = false
      $scope.inform($scope.item, $scope.event, $scope.index)
    })) : null
    $scope.inform($scope.item, $scope.event, $scope.index)
  }
  $scope.play = function(item, event, index) {
    ($scope.item.type == 'movie') ? ($scope.movies[index].gettinglink = true,
    $client.video.playMovie(item._id).then(function(data) {
      $scope.movies[index].gettinglink = false
      item.link = "/movies/" + item._id + "/play?sid=" + data.sid
      $scope.reformLink(item.link)
      $mdDialog.cancel()
      $scope.switchTo(3)
    }, function(error) {
      $scope.movies[index].gettinglink = false
      $client.toast.toast(error.message.message)
    })) : (item.type == 'show') ? ($scope.shows[index].gettinglink = true,
    $client.video.playShow(item._id).then(function(data) {
      $scope.currentLink = item.link = "/shows/" + item._id + "/play?sid=" + data.sid
      $scope.switchTo(3)
      $scope.shows[index].gettinglink = false
    }, function(error) {
      $scope.shows[index].gettinglink = false
      $client.toast.toast(error.message.message)
    })) : (item.type == 'season') ? ($scope.shows[index].buying = true,
    $client.video.playSeason(item._id).then(function(data) {
      $scope.currentLink = item.link = "/seasons/" + item._id + "/play?sid=" + data.sid
      $scope.switchTo(3)
      $scope.seasons[index].gettinglink = false
    }, function(error) {
      $scope.seasons[index].gettinglink = false
      $client.toast.toast(error.message.message)
    })) : (item.type == 'episode') ? ($scope.shows[index].buying = true,
    $client.video.playEpisode($scope.item._id).then(function(data) {
      $scope.currentLink = item.link = "/episodes/" + item._id + "/play?sid=" + data.sid
      $scope.switchTo(3)
      $scope.episodes[index].gettinglink = false
    }, function(error) {
      $scope.episodes[index].gettinglink = false
      $client.toast.toast(error.message.message)
    })) : null
  }
  $scope.reformLink = function(link) {
    $scope.currentLink = []
    for (var i = 0; i < $scope.item.availableextensions.length; i++) {
      var format = $scope.item.availableextensions[i]
      var text = link + '&format=' + format
      $scope.currentLink.push({
        text: text,
        format: format
      })
    }
  }
  $scope.comment = function(item) {
    var id = item._id
      , comment = {
      comment: item.comment,
      rating: item.rating
    }
    if (item.type == 'movie') {
      $client.video.commentMovie(id, comment).then(function(data) {
        $scope.item = data
      }, function(error) {})
    } else if (item.type == 'show') {
      $client.video.commentShow(id, comment).then(function(data) {
        show.comments = data.comments
        show.averagerating = data.averagerating
      }, function(error) {})
    } else if (item.type == 'season') {
      $client.video.commentSeason(id, comment).then(function(data) {
        season.comments = data.comments
        season.averagerating = data.averagerating
      }, function(error) {})
    } else if (item.type == 'episode') {
      $client.video.commentEpisode(id, comment).then(function(data) {
        episode.comments = data.comments
        episode.averagerating = data.averagerating
      }, function(error) {})
    }
  }
  $scope.like = function(item, index) {
    if (item.type == 'movie') {
      $client.video.likeMovieComment(item._id, index).then(function(data) {
        $scope.item = data
      }, function(error) {})
    } else {
      $client.video.like(item.type, item._id, index).then(function(data) {
        $scope.item = data
      }, function(error) {})
    }
  }
  $scope.dislike = function(item, index) {
    $client.video.dislikeMovieComment(item._id, index).then(function(data) {
      $scope.item = data
    }, function(error) {})
  }
  $scope.linked = function() {
    return $scope.currentLink ? true : false
  }
  $scope.reload = function() {
    $scope.reloading = true
    if ($scope.view == 'movies') {
      $client.video.reloadMovies().then(function(data) {
        $scope.reloading = false
        $client.toast.toast(data)
        $scope.getMovies()
      }, function(error) {
        $scope.reloading = true
        $client.toast.toast(error)
      })
    } else if ($scope.view == 'shows') {
      $client.video.reloadShows().then(function(data) {
        $scope.reloading = false
        $client.toast.toast(data)
        $scope.getShows()
      }, function(error) {
        $scope.reloading = false
        $client.toast.toast(error)
      })
    }
  }
  $scope.add = function(item) {
    (item == "movie") ? $client.video.postMovie(scope.movie).then(function(data) {
      $client.toast.toast(data.message.message)
    }, function(error) {
      $client.toast.toast(data.message.message)
    }) : (item == "show") ? $client.video.postShow($scope.show).then(function(data) {
      $client.toast.toast(data.message.message)
    }, function(error) {
      $client.toast.toast(data.message.message)
    }) : null
  }
  $scope.$on('BoughtShow', function(event, index) {
    $scope.shows[index].owned = true
  })
  $scope.$on('BoughtSeason', function(event, index) {
    $scope.show.seasons[index].owned = true
  })
  $scope.$on('BoughtEpisode', function(event, index) {
    $scope.season.episodes[index].owned = true
  })
  $scope.$on('Movies', function(event, data) {
    $scope.movies = data
    $scope.allmovies = angular.copy(data)
    watch()
  })
  $scope.$on('MyMovies', function(event, data) {
    $scope.mymovies = data
    $scope.allmymovies = angular.copy(data)
  })
  $scope.$on('Shows', function(event, data) {
    $scope.shows = data
    $scope.allshows = angular.copy(data)
    watch()
  })
  $scope.$on('MyShows', function(event, data) {
    $scope.myshows = data
    $scope.allmyshows = angular.copy(data)
  })
  $scope.$on('MoviesError', function(event, data) {
    $scope.moviesError = data.message
  })
  $scope.$on('ShowsError', function(event, data) {
    $scope.showError = data.message
  })
}
videoController.$inject = ['$window', '$location', '$timeout', '$state', '$scope', '$mdDialog', '$mdMedia', '$client']
app.controller('videoController', videoController)
