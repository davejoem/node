var gulp = require('gulp')
  , webserver = require('gulp-webserver')
  , sourcemaps = require('gulp-sourcemaps')
  , git = require('gulp-git')
  , browserify = require('browserify')
  , sourcestream = require('vinyl-source-stream')
  , watchify = require('watchify')
  , tsify = require('tsify')
  , gutil = require('gulp-util')
  , gutil = require('gulp-util')
  , tsconfig = require('./tsconfig.json')
  , path = require('path')
  , dist = 'dist/'
  , src = 'src/'

gulp.task('html', function () {
  gulp.src(src+'**/*.html')
})

gulp.task('css', function () {
  gulp.src() 
})

gulp.task('copylibs', function () {
  return gulp.src('node_modules/es6-shim/es6-shim.min.js')
    .pipe(gulp.dest(dist+'/client/js/misc/r2/'))
})

gulp.task('watch', function () {
    gulp.watch(src+'css/*.css',['css'])
    gulp.watch(src+'**/*.html',['html'])
})
gulp.task('webserver', function () {
    return gulp.src(dist)
    .pipe(webserver({
      livereload: true,
      open: true
    }))
})
gulp.task('push', function(){
  git.push('origin', 'master', function (err) {
    if (err) throw err
  })
})

// Run git push with multiple branches and tags 
gulp.task('push', function(){
  git.push('origin', ['master', 'srcelop'], {args: ' --tags'}, function (err) {
    if (err) throw err
  })
})

gulp.task('default', ['copylibs','html','typescipt','watch','webserver'])
