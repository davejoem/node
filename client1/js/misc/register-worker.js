(function(){
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('js/misc/service-worker.js')
      .catch(err => console.log('Error', err));
  }
})()
