var $clientFactory = function($window, $rootScope, $state, $timeout, $mdToast, $interval, $q, $filter, $log, FileSaver, Blob) {
  $client = {
    //characteristics
    authenticated: false,
    //methods
    connect: function() {
      $rootScope.$broadcast('activity', 'Connecting')
      var defered = $q.defer()
      defered.notify('Connecting')
      $client.socket = io().on('connect', function() {
        defered.notify("Connected.")
        $rootScope.$broadcast('status', 'connected.')
        $client.authenticate().then(function(success) {
          defered.resolve("Authenticated.")
        }, function(error) {
          defered.reject(error)
        }, function(info) {
          defered.notify(info)
        })
      }).on('error', function(error) {
        defered.reject(error)
      })
      return defered.promise
    },
    authenticate: function() {
      $rootScope.$broadcast('activity', 'Authenticating')
      var defered = $q.defer()
        , obj = {}
      if ($window.localStorage['user'])
        obj.user = $window.localStorage['user']
      if ($window.localStorage['device']) {
        obj.device = $window.localStorage['device']
        $client.say({
          event: 'authentication',
          data: obj
        }).then(function(success) {
          $rootScope.$broadcast('status', 'authenticated')
          defered.resolve(success)
        }, function(error) {
          console.log(error)
          defered.reject(error)
        }, function(info) {
          defered.notify(info)
        })
      } else {
        $client.register().then(function(data) {
          $client.saveDeviceToken(data.token)
          obj.device = data.token
          $client.say({
            event: 'authentication',
            data: obj
          }).then(function(success) {
            $rootScope.$broadcast('status', 'authenticated')
            defered.resolve(success)
          }, function(error) {
            defered.reject(error)
          }, function(info) {
            defered.notify(info)
          })
        }, function(error) {
          console.log(error)
          defered.reject(error)
        }, function(info) {
          defered.notify(info)
        })
      }
      return defered.promise
    },
    register: function() {
      $rootScope.$broadcast('activity', 'Registering')
      var defered = $q.defer()
      $client.identify().then(function(data) {
        $client.say({
          event: 'register device',
          data: data
        }).then(function(data) {
          $rootScope.$broadcast('status', 'registered')
          console.log('registered',data)
          defered.resolve(data)
        }, function(error) {
          defered.reject(error)
        }, function(info) {
          defered.notify(info)
        })
      }, function(data) {
        $client.say({
          event: 'register device',
          data: data
        }).then(function(data) {
          $rootScope.$broadcast('status', 'registered')
          defered.resolve(data)
        }, function(error) {
          defered.reject(error)
        }, function(info) {
          defered.notify(info)
        })
      })
      return defered.promise
    },
    identify: function() {
      $rootScope.$broadcast('activity', 'Identifying')
      var id, defered = $q.defer()
      if ($client.tokenExists()) {
        id = JSON.parse($window.atob($window.localStorage['device'].split('.')[1]))._id
      }
      var define = function(pos) {
        var This = {
          id: id,
          location: pos || "Unknown location."
        }
        if (is.chrome()) {
            This.name = "Google Chrome"
            This.type = "Browser"
        }
        if (is.edge()) {
          This.name = "Edge"
          This.type = "Browser"
        }        
        if (is.ie()) {
          This.name = "Internet Explorer"
          This.type = "Browser"
        }
        if (is.firefox()) {
          This.name = "Mozilla Firefox"
          This.type = "Browser"
        }
        if (is.opera()) {
          This.name = "Opera"
          This.type = "Browser"
        }
        if (is.safari()) {
          This.name = "Safari"
          This.type = "Browser"
        }
        if (is.tablet()) 
          This.machine = "Tablet"
        if (is.desktop())
          This.machine = "Desktop"
        if (is.mobile())
          This.machine = "Phone"
        if (is.blackberry())
          This.os = "BlackBerry OS"
        if (is.linux())
          This.os = "Linux"
        if (is.mac())
          This.os = "Mac OS"
        if (is.ios())
          This.os = "iOS"
        if (is.windows())
          This.os = "Windows"
        if (is.android())
          This.os = "Android"
        if (is.windowsPhone())
          This.os = "Windows Phone"
        if (!This.name && is.windowsTablet())
          This.name = "Edge"
        if (!This.name && (is.iphone() || is.ipad() || is.ipod()))
          This.name = "Safari"
        if (!This.name && is.androidTablet())
          This.name = "Android Browser"
        $client.identified = true
        $client.this = This
        return This
      }
      var dev = define()
      defered.resolve(dev)
      /*$client.locate().then(function(pos) {
        var dev = define(pos)
        console.log('resolved location')
        defered.resolve(dev)
      }, function(error) {
        var dev = define()
        console.log('rejected location')
        defered.reject(dev)
      })*/
      return defered.promise
    },
    listen: function() {
      $rootScope.$broadcast('activity', 'Listening')
      $rootScope.$broadcast('status', 'active')
      $client.socket.on('device authenticated', function() {
        $client.authenticated = true
      }).on('user signed out', function() {
        $window.localStorage.removeItem('user')
        $state.go('signin')
        $timeout(function() {
          $client.toast.toast('You signed out of this device. Please sign in again.')
        }, 3000)
      }).on('user', function(details) {
        $rootScope.$broadcast('status', 'loaded')
        $client.authenticated = true
        //$client.user.details = details
        //$rootScope.$broadcast('user', user)
      }).on('reconnect', function(data) {
        $client.authenticate().then(function(success) {
          $client.toast.update("Reconnected.", 2000)
        }, function(error) {}, function(info) {})
        $rootScope.$broadcast('status', 'connected')
      }).on('new device activated', function(devices) {
        //$client.user.details.devices.signedInOn = devices
        $client.toast.toast({
          textContent: "New device active.",
          hideDelay: 5000,
          action: 'View',
          theme: "success"
        }).then(function(resolve) {
          if (resolve == 'ok') {
            $state.go('profile.devices')
          }
        })
      }).on('device is now offline', function(device) {
        $client.toast.toast({
          textContent: "A device went offline.",
          hideDelay: 2000
        })
      }).on('device locked', function() {
        console.log("device locked")
        $rootScope.$broadcast('locked')
      }).on('new notification', function(data) {
        if (data.notification.devices.indexOf($client.this.id)) {
          if (!$client.note.muted) {
            $rootScope.$broadcast('new notification', data)
          }
        }
      }).on('chat message', function(data) {
        $rootScope.$broadcast('chat message', data)
        if ($client.user.details.settings.linkapp.messaging.notifyonnewmessage) {
          var promise = $client.toast.toast({
            textContent: data.username + ' sent you a new message.',
            hideDelay: 2000,
            action: 'View',
            theme: "info"
          })
          promise.then(function(res) {
            if (res == 'ok') {
              $client.navigator.goTo('linkapp.conversations')
            }
          })
        }
      }).on("disconnect", function() {
        $client.authenticated = false
        $rootScope.$broadcast('status', 'disconnected.')
        var offline = function() {
          $client.toast.toast({
            messages: ['You are offline. Reconnecting.', 'You are offline. Reconnecting..', 'You are offline. Reconnecting...'],
            interval: 500,
            loop: true,
            theme: "warn",
            cancelAfter: function() {
              return $client.socket.connected
            }
          })
        }
        if ($state.is('signin.activateaccount') || $state.is('signin.signoutdevices') || $state.is('signin.recoverpassword'))
          $state.go('signin').then(offline())
        else
          offline()
      })
    },
    getActiveDevices: function(token) {
      return $client.say({
        event: 'active devices',
        data: {
          token: token
        }
      })
    },
    getToken: function() {
      return $window.localStorage['device']
    },
    saveDeviceToken: function(token) {
      $rootScope.$broadcast('activity', 'Storing identity')
      $window.localStorage['device'] = token
      $client.token = token
    },
    disconnect: function(socket) {
      $rootScope.$broadcast('activity', 'Disconnecting')
      var defered = $q.defer()
      $client.socket.disconnect()
      $rootScope.$broadcast('status', 'disconnected.')
      defered.resolve()
      return defered.promise
    },
    lockSession: function() {
      $rootScope.$broadcast('activity', 'Locking')
      return $client.say({
        event: 'lock device',
      }).then(function(data) {
        $state.go('locked')
        $client.locked = true
      }, function(error) {
        console.log(error)
      })
    },
    signOut: function(deviceid, token) {
      $rootScope.$broadcast('activity', 'Signing out device')
      if (deviceid) {
        return $client.say({
          event: 'signout device',
          data: {
            deviceid: deviceid,
            token: token
          }
        })
      } else {
        return $client.say({
          event: 'signout device',
          data: {
            deviceid: $client.this.id,
            userid: $client.user.details._id
          }
        })
      }
    },
    startTimeout: function() {
      $timeout.cancel($client.countdown)
      if ($client.user.details) {
        if ($client.user.details.settings.apps.general.lockscreen.lock) {
          var delay = $client.user.details.settings.apps.general.lockscreen.timeout * 60 * 1000
          var Countdown = function(period) {
            return $timeout(function() {
              $client.lockSession()
            }, period)
          }
          $client.countdown = new Countdown(delay)
        }
      }
    },
    cancelTimeout: function() {
      $timeout.cancel($client.countdown)
    },
    tokenExists: function() {
      if ($window.localStorage['device']) {
        $client.token = $window.localStorage['device']
        return true
      } else
        return false
    },
    unlockSession: function(key) {
      $rootScope.$broadcast('activity', 'Unlocking')
      return $client.say({
        event: 'unlock device',
        data: {
          key: key
        }
      })
    },
    say: function(config) {
      var defered = $q.defer()
      var e = config.event
      var d = config.data || undefined
      var c = function(error, success, notification) {
        if (notification)
          defered.notify(notification)
        if (error)
          return defered.reject(error)
        if (success)
          return defered.resolve(success)
      }
      $client.socket.emit(e, d, c)
      return defered.promise
    },
    //components
    launcher: {
      Shown: true,
      show: function() {
        $client.launcher.Shown = true
      },
      hide: function() {
        $client.launcher.Shown = false
      },
      toggle: function() {
        $client.launcher.Shown = !$client.launcher.Shown
      },
      views: {
        allapps: {
          id: "allapps",
          button: "allappsbutton",
          name: 'All apps',
          filter: '',
          icon: "tab",
          apps: [{
            id: "video",
            name: 'Video Store',
            icon: "movie",
            url: 'video',
            color: '#E2306C',
            settings: {}
          }, {
            id: "music",
            name: 'Music',
            icon: "headset",
            url: 'music',
            color: '#47B95A',
            settings: {}
          }, {
            id: "gamer",
            name: 'Gamer',
            icon: "gamepad",
            url: 'gamer',
            color: '#AD0CD6',
            settings: {}
          }, {
            id: "linkapp",
            name: 'LinkApp',
            icon: "chat",
            url: 'linkapp',
            color: '',
            settings: {}
          }, {
            id: "profile",
            name: 'Profile',
            icon: "face",
            url: 'profile',
            color: 'blue',
            settings: {}
          }, {
            id: "settings",
            name: 'Settings',
            icon: "settings",
            url: 'settings',
            color: '#D3D3D3',
            settings: {}
          }, {
            id: "notifications",
            name: 'Notifications',
            icon: "notifications",
            url: 'notifications',
            color: '#337aE7',
            settings: {}
          }, {
            id: "ideas",
            name: 'Ideas',
            icon: "lightbulb_outline",
            url: 'ideas',
            color: 'greenyellow',
            settings: {}
          }, {
            id: "stats",
            name: 'Stats',
            icon: "insert_chart",
            url: 'stats',
            color: 'skyblue',
            settings: {}
          }, {
            id: "logs",
            name: 'Logs',
            icon: "view_list",
            url: 'logs',
            color: 'cyan',
            settings: {}
          }]
        },
        recentapps: {
          id: "recentapps",
          button: "recentappsbutton",
          name: 'Recent apps',
          filter: 'recent',
          icon: "movie",
          apps: []
        },
        /*runningapps: {
          id: "runningapps",
          button: "runningappsbutton",
          name: 'Running apps',
          filter: 'running',
          icon: "play-arrow",
          apps: []
        },
        favorite: {
          id: "favoriteapps",
          button: "favoriteappsbutton",
          name: 'Favorite apps',
          filter: 'favorite',
          icon: "stars",
          apps: []
        }*/
      },
      selectView: function(view) {
        views = $client.launcher.views
        tabs = $client.launcher.views
        for (var i = 0; i < views.length; i++) {
          if ((tabs[i].title == view) && (views[i].view == tabs[i].view)) {
            views[i].display = 'ng-show'
          } else if ((tabs[i].title != view) || (views[i].view != tabs[i].view)) {
            views[i].display = 'ng-hide'
          }
        }
      },
      currentIndex: 0,
      setCurrentViewIndex: function(index) {
        $client.launcher.currentIndex = index
      },
      isCurrentViewIndex: function(index) {
        return $client.launcher.currentIndex === index
      },
      prevView: function(index) {
        if (index <= 0) {
          $client.launcher.currentIndex = $client.launcher.views.length - 1
        } else {
          $client.launcher.currentIndex = (index -= 1)
        }
      },
      nextView: function(index) {
        if (index >= this.views.length - 1) {
          $client.launcher.currentIndex = 0
        } else {
          $client.launcher.currentIndex = (index += 1)
        }
      }
    },
    user: {
      tokenExists: function() {
        if ($window.localStorage['user']) {
          $client.user.token = $window.localStorage['user']
          return true
        } else
          return false
      },
      signUp: function(user) {
        $rootScope.$broadcast('activity', 'Signing up')
        return $client.say({
          event: 'signup',
          data: {
            user: user,
            socket: $client.socket.id
          }
        })
      },
      signIn: function(user) {
        $rootScope.$broadcast('activity', 'Signing in')
        return $q(function(resolve, reject) {
          if ($client.socket.connected) {
            resolve($client.say({
              event: 'signin',
              data: {
                user: user,
                socket: $client.socket.id
              }
            }))
          } else {
            $client.connect().then(function(success) {
              resolve($client.say({
                event: 'signin',
                data: {
                  user: user,
                  socket: $client.socket.id
                }
              }))
            }, function(error) {
              reject(error)
            })
          }
        })
      },
      activateAccount: function(code, token, cb) {
        $rootScope.$broadcast('activity', 'Activating account')
        return $client.say({
          event: 'activate user',
          data: {
            code: code,
            token: token
          }
        })
      },
      enablePin: function(pin) {
        $rootScope.$broadcast('activity', 'Enabling pin')
        return $client.say({
          event: 'enable pin',
          data: {
            pin: pin
          }
        })
      },
      changePin: function(pin) {
        $rootScope.$broadcast('activity', 'Changing PIN')
        return $client.say({
          event: 'change pin',
          data: {
            pin: pin
          }
        })
      },
      subscribe: function(type) {
        $rootScope.$broadcast('activity', 'Subscribing')
        return $client.say({
          event: 'subscribe',
          data: {
            subscription: type
          }
        })
      },
      signOut: function() {
        $rootScope.$broadcast('activity', 'Signing you out')
        return $client.user.syncUser(function(success) {
          return $client.say({
            event: 'signout user'
          })
        }, function(error) {
          console.log(error.data)
        })
      },
      getToken: function() {
        return $window.localStorage['user']
      },
      saveToken: function(token) {
        $rootScope.$broadcast('activity', 'Saving your identity')
        $window.localStorage['user'] = token
      },
      getSettings: function() {
        $rootScope.$broadcast('activity', 'Updating your settings')
        var promise = $client.say({
          event: 'user settings'
        })
        promise.then(function(data) {
          $client.user.details.settings = data
        }, function(error) {}, function(info) {
          $client.toast.toast(info)
        })
        return promise
      },
      syncSettings: function() {
        $rootScope.$broadcast('activity', 'Syncronizing settings')
        if ($client.user.details) {
          return $client.say({
            event: 'sync user settings',
            data: {
              settings: $client.user.details.settings
            }
          })
        } else
          return Promise.resolve()
      },
      settings: {},
      syncState: function() {
        $rootScope.$broadcast('activity', 'Syncronizing state')
        if ($client.user.details) {
          return $client.say({
            event: 'sync user state',
            data: {
              state: $client.user.details.state
            }
          })
        } else
          return Promise.resolve()
      },
      syncUser: function() {
        $rootScope.$broadcast('activity', 'Saving details')
        return $q(function(resolve, reject) {
          $client.user.syncSettings().then(function(success) {
            $client.user.syncState().then(function(success) {
              resolve(success)
            }, function(error) {
              reject(error)
            })
          }, function(error) {
            reject(error)
          })
        })
      },
      sendCode: function(method, repeat, token) {
        $rootScope.$broadcast('activity', 'Requesting code to be sent')
        if (!repeat) {
          return $client.say({
            event: 'send code',
            data: {
              method: method,
              token: token
            }
          })
        } else {
          return $client.say({
            event: 'resend code',
            data: {
              method: method,
              token: token
            }
          })
        }
      },
      isLoggedIn: function() {
        if ($client.user.tokenExists()) {
          return JSON.parse($window.atob($client.user.token.split('.')[1])).exp > Date.now() / 1000
        } else
          return false
      },
      getUserId: function() {
        if ($client.user.tokenExists()) {
          $client.user.id = JSON.parse($window.atob($window.localStorage['user'].split('.')[1]))._id
        } else
          $client.user.id = null
      },
      getUserInfo: function(username) {
        $rootScope.$broadcast('activity', 'Finding user')
        if (!username) {
          $client.user.getUserId()
          if ($client.user.id) {
            return $client.say({
              event: 'details'
            })
          }
        } else {
          return $client.say({
            event: 'user info',
            data: {
              username: username
            }
          })
        }
      },
      sendTransCode: function(code) {
        $rootScope.$broadcast('activity', 'Finding transaction')
        return $client.say({
          event: 'send transaction code',
          data: {
            code: code
          }
        })
      },
      findUser: function(username) {
        $rootScope.$broadcast('activity', 'Finding user')
        return $client.say({
          event: 'user info',
          data: {
            username: username
          }
        })
      },
      shareCredit: function(username, amount) {
        $rootScope.$broadcast('activity', 'Sharing credits')
        return $client.say({
          event: 'share credit',
          data: {
            username: username,
            amount: amount
          }
        })
      },
      request: function(username) {
        $rootScope.$broadcast('activity', 'Sending request')
        return $client.say({
          event: 'request',
          data: {
            username: username
          }
        })
      },
      unrequest: function(username, cb) {
        $rootScope.$broadcast('activity', 'Canceling request')
        return $client.say({
          event: 'unrequest',
          data: {
            username: username
          }
        })
      },
      befriend: function(username) {
        $rootScope.$broadcast('activity', 'Adding user to friends list')
        return $client.say({
          event: 'befriend',
          data: {
            username: username
          }
        })
      },
      unfriend: function(username) {
        $rootScope.$broadcast('activity', 'Removing user from friends list')
        return $client.say({
          event: 'unfriend',
          data: {
            username: username
          }
        })
      },
      follow: function(username) {
        return $client.say({
          event: 'follow',
          data: {
            username: username
          }
        })
      },
      unfollow: function(username) {
        return $client.say({
          event: 'unfollow',
          data: {
            username: username
          }
        })
      },
      block: function(username) {
        return $client.say({
          event: 'block',
          data: {
            username: username
          }
        })
      },
      unblock: function(username) {
        return $client.say({
          event: 'unblock',
          data: {
            username: username
          }
        })
      }
    },
    link: {},
    video: {
      postMovie: function(movie) {
        $rootScope.$broadcast('activity', 'Posting movie')
        movie.releaseyear = movie.releasedate.getFullYear()
        return $client.say({
          event: 'new movie',
          data: {
            movie: movie
          }
        })
      },
      postShow: function(show) {
        $rootScope.$broadcast('activity', 'Posting show')
        shows.releaseyear = shows.releasedate.getFullYear()
        return $client.say({
          event: 'new show',
          data: {
            show: show
          }
        })
      },
      getMovies: function() {
        $rootScope.$broadcast('activity', 'Collecting movies')
        var self = this
        return $client.say({
          event: 'all movies',
          data: null
        }).then(function(data) {
          $client.video.movies = data
          self.ownMovies()
        }, function(error) {
          $rootScope.$broadcast('getMoviesError', {
            movieserror: error
          })
        })
      },
      getShows: function() {
        $rootScope.$broadcast('activity', 'Collecting shows')
        return $client.say({
          event: 'all shows',
          data: null
        }).then(function(data) {
          $client.video.shows = data.shows
          $client.video.seasons = data.seasons
          $client.video.episodes = data.episodes
          $client.video.ownShows()
        }, function(error) {
          $rootScope.$broadcast('getShowsError', {
            showserror: error
          })
        })
      },
      getMyMovies: function() {
        $rootScope.$broadcast('activity', 'Populating your movies')
        var self = this
        return $client.say({
          event: 'user movies'
        }).then(function(data) {
          $client.video.mymovies = data.movies
          $client.user.details.Collection.movies = data.movies
          self.ownMovies()
        }, function(error) {
          $rootScope.$broadcast('getMyMoviesError', {
            mymovieserror: error
          })
        })
      },
      getMyShows: function() {
        $rootScope.$broadcast('activity', 'Populating your shows')
        return $client.say({
          event: 'user shows'
        }).then(function(data) {
          $client.video.myshows = data.shows
          $client.user.details.Collection.shows = data.shows
          $client.video.ownShows()
        }, function(error) {
          $rootScope.$broadcast('getMyshowsError', {
            myshowserror: error
          })
        })
      },
      ownMovies: function() {
        $client.video.mymovies ? null : $client.video.mymovies = $client.user.details.Collection.movies
        moviesids = []
        $client.video.movies.forEach(function(movie) {
          moviesids.push(movie._id)
        })
        $client.video.mymovies.forEach(function(movie, index) {
          movie.owned = true
          if (moviesids.indexOf(movie._id) >= 0) {
            $client.video.movies[moviesids.indexOf(movie._id)].owned = true
          }
        })
        $rootScope.$broadcast('Movies', $client.video.movies)
        $rootScope.$broadcast('MyMovies', $client.video.mymovies)
      },
      ownShows: function() {
        $client.video.myshows ? null : $client.video.myshows = $client.user.details.Collection.shows
        $client.video.myseasons ? null : $client.video.myseasons = $client.user.details.Collection.seasons
        $client.video.myepisodes ? null : $client.video.myepisodes = $client.user.details.Collection.episodes
        var showsids = []
          , seasonsids = []
          , episodesids = []
        $client.video.shows.forEach(function(show) {
          showsids.push(show._id)
        })
        $client.video.seasons.forEach(function(season) {
          seasonsids.push(season._id)
        })
        $client.video.episodes.forEach(function(episode) {
          episodesids.push(episode._id)
        })
        $client.video.myshows.forEach(function(show, index) {
          show.owned = true
          if (showsids.indexOf(show._id) >= 0) {
            $client.video.shows[showsids.indexOf(show._id)].owned = true
          }
        })
        $client.video.myseasons.forEach(function(season, index) {
          season.owned = true
          if (seasonsids.indexOf(show._id) >= 0) {
            $client.video.seasons[seasonsids.indexOf(season._id)].owned = true
          }
        })
        $client.video.myepisodes.forEach(function(episode, index) {
          episode.owned = true
          if (episodesids.indexOf(episode._id) >= 0) {
            $client.video.episodes[episodesids.indexOf(episode._id)].owned = true
          }
        })
        $rootScope.$broadcast('Shows', $client.video.shows)
        $rootScope.$broadcast('MyShows', $client.video.myshows)
        $rootScope.$broadcast('MySeasons', $client.video.myseasons)
        $rootScope.$broadcast('MyEpisodes', $client.video.myepisodes)
      },
      disownMovie: function(movie) {
        if (movie) {
          movie.owned = false
        } else {
          $client.video.movies.forEach(function(movie) {
            movie.owned = false
          })
        }
      },
      disownShow: function(showseasonepisode) {
        if (showseasonepisode) {
          showseasonepisode.owned = false
        } else {
          $client.video.shows.forEach(function(show) {
            show.owned = false
          })
          $client.video.seasons.forEach(function(season) {
            season.owned = false
          })
          $client.video.episodes.forEach(function(episode) {
            episode.owned = false
          })
        }
      },
      buyMovie: function(movieid) {
        $rootScope.$broadcast('activity', 'Buying movie')
        return $client.say({
          event: 'buy movie',
          data: {
            movieid: movieid
          }
        })
      },
      downloadMovie: function(movie) {
        return $client.say({
          event: 'movie downloadlink',
          data: {
            movieid: movie._id,
            format: movie.format
          }
        }).then(function(data) {
          $client.say({
            event: 'download movie',
            data: {
              movieid: movie._id,
              sid: data.sid,
              format: movie.format
            }
          }).then(function(data) {
            var blob = new Blob(data,{
              responseType: 'arraybuffer'
            })
            FileSaver.saveAs(blob, movie.title)
          }, function(error) {
            $client.toast.toast(error)
          })
        }, function(error) {
          $client.toast.toast(error)
        })
      },
      playMovie: function(movieid) {
        return $client.say({
          event: 'movies playlink',
          data: {
            movieid: movieid
          }
        })
      },
      commentMovie: function(movieid, comment) {
        return $client.say({
          event: '/movie comment',
          data: {
            movieid: movieid,
            comment: comment
          }
        })
      },
      likeMovieComment: function(movieid, index) {
        return $client.say({
          event: 'like movie comment',
          data: {
            movieid: movieid,
            index: index
          }
        })
      },
      dislikeMovieComment: function(movieid, index) {
        return $client.say({
          event: 'dislike movie comment',
          data: {
            movieid: movieid,
            index: index
          }
        })
      },
      reloadMovies: function() {
        return $client.say({
          event: 'reload movies'
        }).then(function(data) {
          $client.toast.toast(data)
        }, function(error) {
          console.log(error)
          $client.toast.toast(error)
        })
      },
      shareMovie: function(movieid, user) {
        return $client.say({
          event: 'share movie',
          data: {
            movieid: movieid,
            user: user
          }
        })
      },
      movieToDevice: function(deviceid, movie) {
        return $client.say({
          event: 'play movie on device',
          data: {
            deviceid: deviceid,
            movieid: movieid
          }
        })
      },
      buyshow: function(showsid, index) {
        $client.video.buyindex = index
        return $client.say({
          event: 'buy show'
        })
      },
      buySeason: function(showsid, seasonid, index) {
        $rootScope.$broadcast('activity', 'Buying show')
        $client.video.buyindex = index
        return $client.say('/season/' + seasonid + '/buy').then(function(data) {
          $client.video.myshows = data.shows
          $client.user.details.Collection.shows = data.shows
          $client.video.own()
          $rootScope.$broadcast('BoughtSeason', $client.video.buyindex)
          $client.video.buyindex = null
          $rootScope.$broadcast('Shows')
          $rootScope.$broadcast('MyShows')
          $client.toast.toast(data)
        }, function(error) {
          $client.toast.toast(error)
        })
      },
      buyEpisode: function(episodeid, index) {
        $client.video.buyindex = index
        return $client.say('/episode/' + episodeid + '/buy').then(function(data) {
          $client.video.myshows = data.shows
          $client.user.details.Collection.shows = data.shows
          $client.video.own()
          $rootScope.$broadcast('BoughtEpisode', $client.video.buyindex)
          $client.video.buyindex = null
          $rootScope.$broadcast('Shows')
          $rootScope.$broadcast('MyShows')
          $client.toast.toast(data)
        }, function(error) {
          $client.toast.toast(error)
        })
      },
      downloadShow: function(showsid) {
        return $client.say('/shows/' + showsid + '/downloadlink')
      },
      playShow: function(showsid) {
        return $client.say('/shows/' + showsid + '/playlink')
      },
      get: function(showsid, link) {
        return $client.say('/shows/' + showsid + '/download?sid=' + link, {
          responseType: 'arraybuffer'
        })
      },
      commentShow: function(id, comment) {
        return $client.say('/shows/' + id + '/comments', comment)
      },
      commentSeason: function(id, comment) {
        return $client.say('/shows/season/' + id + '/comments', comment)
      },
      commentEpisode: function(id, comment) {
        return $client.say('/shows/episode/' + id + '/comments', comment)
      },
      likeShowComment: function(showsid, index) {
        return $client.say({
          event: 'like show comment',
          data: {
            showsid: showsid,
            index: index
          }
        })
      },
      dislikeShowComment: function(showid, index) {
        return $client.say({
          event: 'dislike show comment',
          data: {
            showsid: showsid,
            index: index
          }
        })
      },
      likeSeasonComment: function(seasonid, index) {
        return $client.say({
          event: 'like season comment',
          data: {
            seasonid: seasonid,
            index: index
          }
        })
      },
      dislikeSeasonComment: function(seasonid, index) {
        return $client.say({
          event: 'dislike season comment',
          data: {
            seasonid: seasonid,
            index: index
          }
        })
      },
      likeEpisodeComment: function(episodeid, index) {
        return $client.say({
          event: 'like episode comment',
          data: {
            episodeid: episodeid,
            index: index
          }
        })
      },
      dislikeEpisodeComment: function(episodeid, index) {
        return $client.say({
          event: 'dislike episode comment',
          data: {
            episodeid: episodeid,
            index: index
          }
        })
      },
      getInfo: function(showsid) {
        $client.say('/shows/' + showsid + '/info').then(function(data) {
          $client.video.currentshows = data
        }, function(error) {
          $client.video.currentshowsError = error
        })
      },
      reloadShows: function() {
        return $client.say({
          event: 'reload shows'
        }).then(function(data) {
          $client.toast.toast(data)
        }, function(error) {
          console.log(error)
          $client.toast.toast(error)
        })
      },
      shareShow: function(showsid, username) {
        return $client.say('share show')
      },
      toDevice: function(deviceid, shows) {
        return $client.say({
          event: 'shows play device',
          data: {
            deviceid: deviceid,
            shows: shows
          }
        })
      },
      pause: function() {},
      isPlaying: function() {
        return $state.is('shows.play')
      },
      isPaused: function() {
        return ( $state.is('shows.play') && this.shows.paused)
      }
    },
    music: {
      playing: false,
      timestamp: "2:32",
      volume: 50,
      getMusic: function() {},
      shuffle: function() {},
      repeat: function() {},
      pause: function() {},
      play: function() {},
    },
    gamer: {},
    toast: {
      toast: function(config) {
        var element = config.element || angular.element("#toastBounds")
        if (typeof config == 'string') {
          $client.toast.display = $mdToast.simple({
            capsule: true,
            parent: element,
            position: 'bottom right',
            hideDelay: 3000,
            autoWrap: true,
            preserveScope: false
          }).textContent(config).theme("info")
          return $mdToast.show($client.toast.display)
        } else if (typeof config == 'object' && config.message) {
          $client.toast.display = $mdToast.simple({
            capsule: config.capsule || true,
            parent: element,
            position: config.position || 'bottom right',
            hideDelay: config.hideDelay || 3000,
            autoWrap: config.autoWrap || true,
            preserveScope: config.preserveScope || false
          }).textContent(config.message.message || config.message)
          var theme = config.theme || config.class || config.message.class || undefined
          $client.toast.display.theme(theme)
          return $mdToast.show($client.toast.display)
        } else if (typeof config == 'object' && config.messages) {
          $client.toast.display = $mdToast.simple({
            capsule: config.capsule || true,
            parent: element,
            position: 'bottom right',
            hideDelay: config.hideDelay || 0,
            autoWrap: config.autoWrap || true,
            preserveScope: config.preserveScope || false
          }).textContent(config.messages[0])
          var theme = config.theme || config.class || undefined
          $client.toast.display.theme(theme)
          $mdToast.show($client.toast.display)
          var i = 1
          var interval = $interval(function() {
            config.cancelAfter ? !config.cancelAfter() ? ($mdToast.updateTextContent(config.messages[i]),
            i == config.messages.length - 1 ? config.loop ? i = 0 : $interval.cancel(interval) : i++) : $interval.cancel(interval) : (function() {
              return interval
            }
            )
          }, config.interval || 1000)
        } else {
          $client.toast.display = $mdToast.simple(config)
          return $mdToast.show($client.toast.display)
        }
      },
      update: function(update, delay) {
        if (typeof update == "string")
          $mdToast.updateTextContent(update)
        if (typeof update == "object") {
          if (update.text)
            $client.toast.display.textContent(update.text)
          if (update.theme)
            $client.toast.display.theme(update.theme)
          return $mdToast.show($client.toast.display)
        }
        if (delay || update.delay) {
          var delay = delay || update.delay
          $timeout(function() {
            $mdToast.hide()
          }, delay)
        }
      },
      action: function(text) {
        $client.toast.display.action(text)
        $mdToast.show($client.toast.display)
      },
      hide: function(response, delay) {
        delay ? $timeout(function() {
          $mdToast.hide(response)
        }, delay) : $mdToast.hide(response)
      }
    },
    theme: {
      themes: {
        crispyclean: {
          name: 'Crispy Clean',
          shortname: 'crispyclean',
          colour: 'white'
        },
        codergreen: {
          name: 'Coder Green',
          shortname: 'codergreen',
          colour: 'green'
        },
        darkknight: {
          name: 'Dark Knight',
          shortname: 'darkknight',
          colour: 'black'
        },
        default: {
          name: 'Default',
          shortname: 'default',
          colour: 'black'
        },
        girly: {
          name: 'Pretty Girly',
          shortname: 'girly',
          colour: 'pink'
        },
        loveray: {
          name: 'Love Ray',
          shortname: 'loveray',
          colour: 'purple'
        },
        monkeyparadise: {
          name: 'Monkey Paradise',
          shortname: 'monkeyparadise',
          colour: 'yellow'
        },
        orangecounty: {
          name: 'Orange County',
          shortname: 'orangecounty',
          colour: 'orange'
        },
        outdoor: {
          name: 'Brisk Outdoor',
          shortname: 'outdoor',
          colour: 'lightgreen'
        },
        romance: {
          name: 'Firy Romance',
          shortname: 'romance',
          colour: 'red'
        },
        skyblue: {
          name: 'Smily Blue',
          shortname: 'skyblue',
          colour: 'blue'
        },
        slate: {
          name: 'Salmon Slate',
          shortname: 'slate',
          colour: 'grey'
        },
      },
      choose: function(state) {
        $rootScope.$broadcast('activity', 'Changing theme')
        var siteTheme, appTheme
        if (state)
          $client.theme.state = state
        if ($client.user.details.settings.apps.general.universaltheme) {
          siteTheme = appTheme = $client.user.details.settings.apps.general.theme
        } else {
          siteTheme = $client.user.details.settings.apps.general.theme
          $client.user.details.settings.apps[$client.theme.state.name.split('.')[0]] ? $client.user.details.settings.apps[$client.theme.state.name.split('.')[0]].display ? $client.user.details.settings.apps[$client.theme.state.name.split('.')[0]].display.theme ? appTheme = $client.user.details.settings.apps[$client.theme.state.name.split('.')[0]].display.theme : appTheme = siteTheme : appTheme = siteTheme : appTheme = siteTheme
        }
        $rootScope.$broadcast('theme', {
          siteTheme: siteTheme,
          appTheme: appTheme
        })
      }
    },
    note: {
      notifications: {
        all: [],
        unread: []
      },
      get: function() {
        $rootScope.$broadcast('activity', 'Fetching notifications')
        return $client.say({
          event: 'notifications'
        })
      },
      sort: function(method) {
        $rootScope.$broadcast('activity', 'Sorting notifications')
      },
      muted: false,
      mute: function() {
        $rootScope.$broadcast('status', 'muted')
        $client.note.muted = true
        return $client.note
      },
      unmute: function() {
        $rootScope.$broadcast('status', 'unmuted')
        $client.note.muted = false
        return $client.note
      },
      clear: function(index) {
        return $client.say({
          event: 'clear notification',
          data: {
            notification: index
          }
        })
      },
      clearAll: function() {
        return $client.say({
          event: 'clear all notifications'
        })
      }
    },
    navigator: {
      goTo: function(dest, directly) {
        if ($state.current.name == '')
          this.from = 'apps'
        else if ($state.current.name != dest.split('.')[0])
          this.from = $state.current.name
        $client.navigator.dest = dest
        $client.navigator.directly = directly
        $state.go(dest)
        $client.user.details.state.general.lastapp = dest.split('.')[0]
        //$client.user.syncState()
      },
      goBack: function() {
        $state.go(this.from)
      },
      resume: function() {
        if ($state.previous.name) {
          $state.go($state.previous.name)
        } else
          $state.go('apps')
      },
      from: 'apps'
    },
    search: {
      searchtext: null ,
      results: null ,
      find: function(where, searchtext, filters) {
        results = {}
        $socket.emit({
          socket: 'search',
          event: 'search',
          data: {
            where: where,
            searchtext: search.searchtext
          },
          callback: function(errordata) {
            if (data) {
              results = data.data;
            }
            if (error) {}
          }
        });
        return results;
      },
      search: function(where) {
        if (!search.searchtext || search.searchtext == '') {
          return new Error("Invalid search parameters.");
        }
        search.results = search.find(where, search.searchtext, search.filters);
        $state.go(where + '.search');
        console.log('Went to ' + where + '.search')
        return search.results;
      },
      filter: function(obj) {
        return $filter('filter')(obj.array, obj.expression, obj.comparator);
      },
      orderBy: function(obj) {
        return $filter('orderBy')(obj.array, obj.expression, obj.comparator);
      },
      sort: function(method) {}
    },
    context: {
      show: function(event) {
        event.preventDefault()
        console.log(event)
      }
    }
  }
  return $client
}
$clientFactory.$inject = ['$window', '$rootScope', '$state', '$timeout', '$mdToast', '$interval', '$q', '$filter', '$log', 'FileSaver', 'Blob']
app.factory('$client', $clientFactory)
