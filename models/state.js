'use strict'
// Load required packages
var mongoose = require("mongoose");

// Define our state schema
var StateSchema = new mongoose.Schema({
    device: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Device"
    },
    general: {
        lastapp: {
            type: String,
            required: true,
            default: "apps"
        }
    },
    main: {
        type: Boolean,
        required: true,
        default: false
    },
    states: {
        video: {
            laststate: {
                type: Number,
                required: true,
                default: 0
            },
            activeitem: String,
        },
        music: {
            laststate: {
                type: Number,
                required: true,
                default: 0
            },
            activeitem: String,
        },
        settings: {
            laststate: {
                type: Number,
                required: true,
                default: 5
            }
        },
        gamer: {
            laststate: {
                type: Number,
                required: true,
                default: 0
            }
        },
        profile: {
            laststate: {
                type: Number,
                required: true,
                default: 0
            }
        },
        logs: {
            laststate: {
                type: Number,
                required: true,
                default: 0
            }
        },
        linkapp: {
            laststate: {
                type: Number,
                required: true,
                default: 1
            }
        }
    },
    views: {
        settings: {
            video: {
                type: Number,
                required: true,
                default: 0
            },
            music: {
                type: Number,
                required: true,
                default: 0
            },
            gamer: {
                type: Number,
                required: true,
                default: 0
            },
            linkapp: {
                type: Number,
                required: true,
                default: 0
            },
            profile: {
                type: Number,
                required: true,
                default: 0
            },
            settings: {
                type: Number,
                required: true,
                default: 0
            },
            notifications: {
                type: Number,
                required: true,
                default: 0
            },
            ideas: {
                type: Number,
                required: true,
                default: 0
            },
            stats: {
                type: Number,
                required: true,
                default: 0
            },
            logs: {
                type: Number,
                required: true,
                default: 0
            }
        },
        profile: {
            social: {
                type: Number,
                required: true,
                default: 0
            },
            devices: {
                type: Number,
                required: true,
                default: 0
            },
            finance: {
                type: Number,
                required: true,
                default: 0
           }
        },
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    }
});
module.exports = mongoose.model('State',StateSchema);