function gamerController($scope, $mdSidenav, $client) {
  $scope.settings = $client.user.details.settings.apps.gamer
  $scope.gaming = false
  $scope.sidenav = false
  $scope.toggleSideNav = function() {
    $mdSidenav('nav').toggle().then(function() {
      $scope.sidenav = !$scope.sidenav
    })
  }
  $scope.getMusic = $client.gamer.getMusic
  $scope.searchbar = $scope.settings.display.showsearchbar
  $scope.filter = {
    scheme: "all",
    searchText: ''
  }
  $scope.orderBy = ''
  function watch() {
    $scope.$watch('filter.searchText', function(text) {
      var expression;
      switch ($scope.filter.scheme) {
      case 'all':
        expression = text;
        break;
      case 'developer':
        expression = {
          title: text
        };
        break;
      case 'genre':
        expression = {
          genre: text
        };
        break;
      case 'year':
        expression = {
          year: text
        };
        break;
      case 'studio':
        expression = {
          studio: text
        };
        break;
      }
      $state.is('gamer.library') ? (obj = {
        array: $scope.allgames,
        expression: expression
      },
      $scope.mygames = $client.search.filter(obj)) : $state.is('gamer.collection') ? (obj = {
        array: $scope.allmygames,
        expression: expression
      },
      $scope.games = $client.search.filter(obj)) : $state.is('gamer.library') ? (obj = {
        array: $scope.allgames,
        expression: expression
      },
      $scope.mygames = $client.search.filter(obj)) : $state.is('gamer.collection') ? (obj = {
        array: $scope.allmygames,
        expression: expression
      },
      $scope.mygames = $client.search.filter(obj)) : null ;
    });
    $scope.$watch('orderBy', function(text) {
      ($scope.filter == 'all') ? expression = ["title", "year", "released"] : expression = [$scope.filter, "year", "released"];
      (text == 'asc') ? reverse = false : reverse = true;
      $state.is("gamer.library") ? (obj = {
        array: $scope.allgames,
        expression: expression,
        reverse: reverse
      },
      $scope.albums = $client.search.orderBy(obj)) : $state.is('gamer.collection') ? (obj = {
        array: $scope.allmygames,
        expression: expression,
        reverse: reverse
      },
      $scope.myalbums = $client.search.orderBy(obj)) : $state.is('gamer.library') ? (obj = {
        array: $scope.allgames,
        expression: expression,
        reverse: reverse
      },
      $scope.songs = $client.search.orderBy(obj)) : $state.is('gamer.collection') ? (obj = {
        array: $scope.allmygames,
        expression: expression,
        reverse: reverse
      },
      $scope.mysongs = $client.search.orderBy(obj)) : null ;
    });
  }
  $scope.switchTo = function(index) {
    $scope.selectedIndex = index
  }
  if ($scope.user.settings.apps.general.rememberstates) {
    $scope.selectedIndex = $scope.user.state.states.gamer.laststate;
  } else {
    switch ($scope.settings.display.startin) {
    case 'home':
      $scope.selectedIndex = 0;
      break;
    case 'library':
      $scope.selectedIndex = 1;
      break;
    case 'collection':
      $scope.selectedIndex = 2;
      break;
    case 'previous':
      $scope.selectedIndex = $scope.user.state.states.gamer.laststate;
      break;
    }
  }
  $scope.$watch('selectedIndex', function(index) {
    switch (index) {
    case 0:
      $scope.goTo("gamer.home")
      $client.user.details.state.states.gamer.laststate = 0
      break;
    case 1:
      $scope.goTo("gamer.library")
      $client.user.details.state.states.gamer.laststate = 1
      break;
    case 2:
      $scope.goTo("gamer.collection")
      $client.user.details.state.states.gamer.laststate = 2
      break;
    case 3:
      $scope.goTo("gamer.play")
      $client.user.details.state.states.video.laststate = 3
      break;
    }
  })
  $scope.inform = function(item, event, index) {
    $scope.item = item;
    $scope.event = event;
    $scope.index = index;
    $scope.popup({
      scope: $scope,
      templateUrl: '/templates/gamer/info.html'
    });
  }
}
gamerController.$inject = ['$scope', '$mdSidenav', '$client'];
app.controller('gamerController', gamerController);
