"use strict"
let agent = {}
  , args = agent.args = require('argparse')
  , bcrypt = agent.bcrypt = require('bcrypt-nodejs')
  , compression = agent.compression = require('compression')
  , crypto = agent.crypto = require('crypto')
  , cp = agent.cp = require('child_process')
  , express = agent.express = require('express')
  , extend = agent.extend = require('extend')
  , favicon = agent.favicon = require('serve-favicon')
  , fs = agent.fs = require('fs-extra')
  , http = agent.http = require('http')
  , https = agent.https = require('https')
  , socketio = agent.socketio = require('socket.io')
  , jwt = agent.jwt = require("jsonwebtoken")
  , lodash = agent.lodash = require('lodash')
  , mongoose = agent.mongoose = require('mongoose')
  , morgan = agent.morgan = require('morgan')
  , pass = agent.pass = require('passport')
  , path = agent.path = require('path')
  , url = agent.url = require('url')
  , Q = require('q')
  , shortId = require('shortid')
  , ipaddress = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || `127.0.0.1`
  , port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3000
  , db_path = ``  
  , status = agent.status = {
      online: false,
      version: 2
  }
  , chooseVersion = agent.chooseVersion = ()=>{
    return new Promise((resolve,reject)=>{
      var ArgumentParser = args.ArgumentParser;
      var parser = new ArgumentParser({
        version: '1.0.0',
        addHelp:true,
        description: 'Floatr'
      });
      parser.addArgument(
        [ '-V', '--Version' ],
        {
          action:"store",
          type:"int",      
          choices: [1,2],
          help: 'Version of web client to run. Choose between 1 & 2'
        }
      );
      var arg = parser.parseArgs();
      switch(arg.Version){
        case 1: resolve('client1'); break;
        case 2: resolve('client2'); break;
        default: resolve('client1'); break;
      }
    })
  }
  , checkOnlineStatus = agent.checkOnlineStatus = ()=>{
    return new Promise((resolve,reject)=>{
      let com = cp.exec('ping www.google.com')
      let out = com.stdout
      let all = ""
      out.on('data', data=>{
        all += data
      }).on('end', (err)=>{
        if (all.toLowerCase().includes(`could not find host www.google.com`))
          resolve(false)
        else
          resolve(true)
      }).on('error', err=>{
        reject(err)
      })
    })
  }
  , checkDb  = agent.checkDb = (status)=>{
    return new Promise((resolve,reject)=>{
      db_path = `mongodb://`
      if (process.env.OPENSHIFT_MONGODB_DB_USERNAME && process.env.OPENSHIFT_MONGODB_DB_PASSWORD) {
        db_path += process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" + process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@";
      }
      if (process.env.OPENSHIFT_MONGODB_DB_HOST) {
        db_path += process.env.OPENSHIFT_MONGODB_DB_HOST + ":";
      } else {
        db_path += "127.0.0.1:"
      }
      if (process.env.OPENSHIFT_MONGODB_DB_PORT) {
        db_path += process.env.OPENSHIFT_MONGODB_DB_PORT;
      } else {
        db_path += "27017"
      }
      db_path += "/floatr"
      if (status == true) resolve("mongodb://dave:floatr@ds139985.mlab.com:39985/floatr")
      else resolve(db_path)
    })
  }
chooseVersion().then(source=>{
  checkOnlineStatus().then(stat=>{
    status.online = agent.status.online = stat
    checkDb(stat).then(pat=>{
      process.stdout.write(`Using database: ${pat}\n`);
      agent.db_path = db_path = pat
      let models = agent.models = {
        Call: require(path.resolve(__dirname, 'models/call')),
        Chat: require(path.resolve(__dirname, 'models/chat')),
        Device: require(path.resolve(__dirname, 'models/device')),
        Episode: require(path.resolve(__dirname, 'models/episode')),
        Game: require(path.resolve(__dirname, 'models/game')),
        Message: require(path.resolve(__dirname, 'models/message')),
        Movie: require(path.resolve(__dirname, 'models/movie')),
        Notification: require(path.resolve(__dirname, 'models/notification')),
        Preference: require(path.resolve(__dirname, 'models/preference')),
        Season: require(path.resolve(__dirname, 'models/season')),
        Show: require(path.resolve(__dirname, 'models/show')),
        State: require(path.resolve(__dirname, 'models/state')),
        Transaction: require(path.resolve(__dirname, 'models/transaction')),
        User: require(path.resolve(__dirname, 'models/user'))
      }
      , keys = agent.keys = require(path.resolve(__dirname, 'config/keys'))
      , consts = agent.consts = require(path.resolve(__dirname, 'utils/consts'))(agent)
      , log = agent.log = require(path.resolve(__dirname, 'utils/log'))(agent)
      , auth = agent.auth = require(path.resolve(__dirname, 'utils/socket_auth'))
      , fn = agent.fn = require(path.resolve(__dirname, 'utils/fn'))(agent)
      , poster = agent.poster = require(path.resolve(__dirname, 'utils/poster'))(agent)
      , tgdb = agent.tgdb = require(path.resolve(__dirname, 'utils/tmdb'))(agent)
      , omdb = agent.omdb = require(path.resolve(__dirname, 'utils/omdb'))(agent)
      , tmdb = agent.tmdb = require(path.resolve(__dirname, 'utils/tmdb'))(agent)
      , dev = agent.dev = fn.dev
      , rquser = agent.rquser = fn.rquser
      , admin = agent.admin = fn.admin
      , db = agent.db = mongoose.connect(pat)
      , app = agent.app = express()
      , server = agent.server = http.createServer(app)
      , io = agent.io = socketio.listen(server)
      , passport = agent.passport = require(path.resolve(__dirname, 'config/passport'))(agent)
      , events = agent.events = require(path.resolve(__dirname, 'events/main'))(agent)
      //app.use(morgan('dev'))
      app.use(favicon(path.resolve(__dirname,source,'assets','icon','favicon.png')))
      app.use(express.static(path.resolve(__dirname, source), {
        maxAge: 86400000
      }))
      app.use(compression({
        threshold: 1024
      }))
      app.get('/auth/facebook', (req,res,next)=>{
        console.log(req.query)
      })
      app.get('/auth/twitter', (req,res,next)=>{
        console.log(req.query)
      })
      app.get('/auth/instagram', (req,res,next)=>{
        console.log(req.query)
      })
      app.get('/activate', (req,res,next)=>{
        console.log(req.query)
        models.User.find(req.query.username, (err,user)=>{
          if (err) {}
          user.activate(req.query.code, (err,user)=>{
            if (err) {}
            res.redirect('https://floatr.herokuapp.com/#/signin')
          })
        })
      })
      app.post('/activate', (req,res,next)=>{
        console.log(req.query)
        models.User.findById(req.query.id, (err,user)=>{
          if (err) {}
          user.activate(req.query.code, (err,user)=>{
            if (err) {}
            res.redirect('https://floatr.herokuapp.com/#/signin')
          })
        })
      })
      app.get('/', (req,res,next)=>{
        res.render(path.resolve(__dirname, source, 'index.html'))
      })    
      app.get('/*', (req,res,next)=>{
        res.redirect('//' + req.headers.host)
      })
      fn.sock(agent)
      function onerr(err) {
        console.log(err)
        function inc(p) {
          p++
          server = http.Server(app).listen(p)
        }
        if (err.code == 'EADDRINUSE') {
          inc(port)
        }
      }
      server.listen(port, function() {
        var addr = server.address()
        console.log("floatr running on %s:%s", addr.address, addr.port)
      }).on('error', onerr)
    })
  }).catch(err=>{process.stdout.write(`Err: ${err}`)})
}).catch(err=>{process.stdout.write(`Err: ${err}`)})